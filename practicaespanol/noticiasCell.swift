//
//  noticiasCell.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 05/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class noticiasCell: UITableViewCell {
    var lblTitulo : UILabel!
    var lblSeccion : UILabel!
    var viewDificultad : UIView!
    var lblDificultad : UILabel!
    var imgVisto : UIImageView!
    var btnPlay : UIImageView!
    var imgFondo : UIImageView!
    var viewOpacoImg : UIView!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0
    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.frame.size.height = 150
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        
        self.buildInterface()
        
        self.selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        
        switch tipo {
        case "titulo":
            font = UIFont.boldSystemFont(ofSize: 13)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightBold)
            }
            break
        case "seccion":
            font = UIFont.systemFont(ofSize: 13)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightLight)
            }
            break
        case "dificultad":
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightLight)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 13)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightLight)
            }
            break
        }
        
        return font
    }
    
    func buildInterface(){
        
        //img fondo
        self.imgFondo = UIImageView()
        self.imgFondo.frame = self.contentView.frame
        
        self.imgFondo.contentMode = .scaleToFill
        //self.imgFondo.clipsToBounds = true
        self.contentView.addSubview(self.imgFondo)
        //self.imgFondo.center = self.contentView.center
        self.imgFondo.center.x = self.contentView.frame.size.width / 2
        self.imgFondo.center.y = self.contentView.frame.size.height / 2
        
        //view encima para opacar imagen y resaltar texto
        self.viewOpacoImg = UIImageView(frame: self.imgFondo.frame)
        self.viewOpacoImg.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        self.contentView.addSubview(self.viewOpacoImg)
        self.viewOpacoImg.center = self.imgFondo.center
        
        //lbl seccion de noticia
        self.lblSeccion = UILabel(frame: CGRect(x: 10, y: 10, width: self.anchoCelda * 0.9, height: 10))
        self.lblSeccion.textColor = UIColor.white
        self.lblSeccion.numberOfLines = 1
        self.lblSeccion.font = self.getFont(tipo: "seccion")
        
        self.contentView.addSubview(self.lblSeccion)
        
        //circulo y label con la dificultad de la prueba
        self.viewDificultad = UIView(frame: CGRect(x: 10, y: 10, width: 25, height: 25))
        self.viewDificultad.backgroundColor = UIColor(red: 141/255, green: 174/255, blue: 71/255, alpha: 1)
        self.lblDificultad = UILabel(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.lblDificultad.textColor = UIColor.white
        self.lblDificultad.numberOfLines = 1
        self.lblDificultad.text = "XX"
        self.lblDificultad.font = self.getFont(tipo: "dificultad")
        
        self.viewDificultad.addSubview(self.lblDificultad)
        self.contentView.addSubview(self.viewDificultad)
        
        //imagen visto
        self.imgVisto = UIImageView(frame: CGRect( x: 10, y: 10, width: 25, height: 25))
        self.imgVisto.contentMode = .scaleAspectFill
        self.imgVisto.image = UIImage(named: "eyeblanco")
        self.contentView.addSubview(self.imgVisto)
        
        //btn play
        self.btnPlay = UIImageView(frame: CGRect(x: 10, y: 10, width: 40, height: 40))
        self.btnPlay.contentMode = .scaleAspectFit
        self.btnPlay.image = UIImage(named: "Circle blanco play")
        self.contentView.addSubview(self.btnPlay)
        
        //titulo de la noticia
        self.lblTitulo = UILabel(frame: CGRect(x: 10, y: 10, width: self.anchoCelda * 0.9, height: 10))
        self.lblTitulo.textColor = UIColor.white
        self.lblTitulo.numberOfLines = 2
        self.lblTitulo.adjustsFontSizeToFitWidth = true
        self.lblTitulo.font = self.getFont(tipo: "titulo")
        
        self.contentView.addSubview(self.lblTitulo)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
