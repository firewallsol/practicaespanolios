//
//  BoletinVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 04/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Jukebox
import SideMenu
import PKHUD

class BoletinVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var boletinData = [Boletin]()
    var boletinDataRespaldo = [Boletin]()
    var tableBoletin : UITableView!
    
    var funciones = Functions()
    
    var dataLoaded = false
    
    var offsetAlto : CGFloat = 0
    
    var isDataLoading = false
    var pageNo : Int = 1
    
    var callFromMenu = false
    
    var celdaSeleccionada : IndexPath!
    
    let notifFiltroBoletin = Notification.Name("aplicaFiltroBoletin")
    
    var vistoOnOff = false
    
    var primeraCarga = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.view.tag = 3
        
        self.buildInterface()
        
        self.getData()
        
        if self.callFromMenu {
            HUD.show(.labeledProgress(title: "", subtitle: "Cargando..."))
            self.setNavBar()
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDatosFiltro), name: self.notifFiltroBoletin, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.callFromMenu {
            self.title = "BOLETÍN"
        }
        
        if self.celdaSeleccionada != nil {
            self.tableBoletin.reloadRows(at: [self.celdaSeleccionada], with: .automatic)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.callFromMenu {
            self.title = ""
        }
    }
    
    func buildInterface(){
        
        self.tableBoletin = UITableView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        self.view.addSubview(self.tableBoletin)
        self.tableBoletin.dataSource = self
        self.tableBoletin.delegate = self
        self.tableBoletin.register(boletinCell.self, forCellReuseIdentifier: "boletinCell")
        self.tableBoletin.rowHeight = 70
        self.tableBoletin.separatorStyle = .none
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
        
        //filtro
        let imgFiltro = UIImage(named: "filter2")
        let filterButton = UIBarButtonItem(image: imgFiltro, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.filterButtonTap))
        
        self.navigationItem.rightBarButtonItem = filterButton
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func filterButtonTap(){
        let vcFiltro = filtroVistoNoVistoVC()
        vcFiltro.vistoOnOff = self.vistoOnOff
        vcFiltro.sender = "boletin"
        vcFiltro.buildInterface()
        self.navigationController?.pushViewController(vcFiltro, animated: true)
    }
    
    func getDatosFiltro(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        self.vistoOnOff = paramFiltro["visto"] as! Bool
        self.aplicaFiltro()
    }
    
    func aplicaFiltro(){
        self.boletinData = self.boletinDataRespaldo
        //self.pageNo = 1
        var newArray = [Boletin]()
        
        if self.vistoOnOff {
            for item in self.boletinData {
                if Functions().existeIdEnVisto(id_nota: item.id){
                    newArray.append(item)
                }
            }
            self.boletinData = newArray
        }
        
        self.dataLoaded = true
        self.tableBoletin.reloadData()
    }
    
    func getData(){
        let urlrequest = Constantes.WSData.urlRestBase + "?filter[category_name]=boletines-informativos&page=\(self.pageNo)"
        Alamofire.request("\(urlrequest)").responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonData = JSON(value)
                if ((jsonData.array?.count)! > 0){
                    self.boletinData = self.boletinDataRespaldo
                    if (self.pageNo == 1){
                        self.boletinData = self.funciones.JSONtoArrayBoletin(elJson: jsonData)
                    } else {
                        self.boletinData.append(contentsOf: self.funciones.JSONtoArrayBoletin(elJson: jsonData))
                    }
                    
                    self.boletinDataRespaldo = self.boletinData
                    
                    if (!self.dataLoaded){
                        var insets = self.tableBoletin.contentInset
                        insets.bottom += self.offsetAlto
                        self.tableBoletin.contentInset = insets
                    }
                    
                    self.aplicaFiltro()
                    
                }
                
                if self.primeraCarga {
                    HUD.hide()
                }
                
            case .failure(let error):
                print(error)
                if self.callFromMenu {
                    HUD.hide()
                    let alerta = Functions().alertaSimple(titulo: "Error", mensaje: "Ha ocurrido un error al intentar conectarse al servidor", estilo: .alert, showButton: true)
                    self.present(alerta, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataLoaded {
            return self.boletinData.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableBoletin.dequeueReusableCell(withIdentifier: "boletinCell")! as! boletinCell
        
        cell.frame.size.width = self.anchoPantalla
        
        if self.dataLoaded {
            //btn play
            var frameVar = cell.btnPlay.frame
            frameVar.size = CGSize(width: cell.frame.size.height * 0.7, height: cell.frame.size.height * 0.7)
            cell.btnPlay.frame = frameVar
            cell.btnPlay.center.y = cell.frame.size.height / 2
            let tapPlay = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            cell.btnPlay.isUserInteractionEnabled = true
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.addGestureRecognizer(tapPlay)
            
            //fecha
            cell.lblFecha.text = self.boletinData[indexPath.row].fecha
            cell.lblFecha.sizeToFit()
            frameVar = cell.lblFecha.frame
            let yFecha = cell.frame.size.height - (cell.lblFecha.frame.size.height + 10)
            frameVar.origin = CGPoint(x: cell.btnPlay.frame.maxX + 10, y: yFecha)
            cell.lblFecha.frame = frameVar
            
            //titulo
            cell.lblTitulo.text = self.boletinData[indexPath.row].titulo
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: cell.btnPlay.frame.maxX + 10, y: 5)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.55, height: cell.lblTitulo.frame.size.height)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            let lblTituloPosY = cell.lblFecha.frame.minY - (cell.lblTitulo.frame.size.height + 5)
            cell.lblTitulo.frame.origin.y = lblTituloPosY
            
            //estrella
            //cell.imgEstrella.frame.size  = CGSize(width: cell.frame.size.height * 0.4, height: cell.frame.size.height * 0.4)
            cell.imgEstrella.frame.size = CGSize.zero
            frameVar = cell.imgEstrella.frame
            let xposEstrella = cell.frame.size.width - (cell.imgEstrella.frame.size.width + 5)
            frameVar.origin = CGPoint(x: xposEstrella, y: 30)
            cell.imgEstrella.frame = frameVar
            cell.imgEstrella.center.y = cell.frame.size.height / 2
            cell.imgEstrella.isHidden = true
            
            //visto
            cell.imgVisto.frame.size = CGSize(width: cell.frame.size.height * 0.4, height: cell.frame.size.height * 0.4)
            frameVar = cell.imgVisto.frame
            let posVisto = cell.imgEstrella.frame.minX - (cell.imgVisto.frame.size.width + 8)
            frameVar.origin = CGPoint(x: posVisto, y: 30)
            cell.imgVisto.frame = frameVar
            cell.imgVisto.center.y = cell.frame.size.height / 2
            
            if (Functions().existeIdEnVisto(id_nota: self.boletinData[indexPath.row].id)){
                cell.imgVisto.isHidden = false
            } else {
                cell.imgVisto.isHidden = true
            }
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.celdaSeleccionada = indexPath
        let vcEstatico = staticTextVC()
        vcEstatico.sender = "boletin"
        vcEstatico.dataBoletin = self.boletinData[indexPath.row]
        self.navigationController?.pushViewController(vcEstatico, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //print("scrollViewWillBeginDragging")
        self.isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndDecelerating")
    }
    //paginacion
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //print("scrollViewDidEndDragging")
        if ((self.tableBoletin.contentOffset.y + self.tableBoletin.frame.size.height) > self.tableBoletin.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo += 1
                self.primeraCarga = true
                self.getData()
                
            }
        }
        
        
    }
    
    func playItem(sender: UITapGestureRecognizer){
        let navController = self.navigationController as! navControllerPrincipal
        if (navController.toolBarPlayer.isHidden){
            navController.toolBarPlayer.isHidden = false
            navController.view.bringSubview(toFront: navController.toolBarPlayer)
        } else {
            if (navController.jukeBoxPlayer?.currentItem != nil){
                navController.jukeBoxPlayer?.remove(item: (navController.jukeBoxPlayer?.currentItem)!)
            }
            
        }
        
        if navController.jukeBoxPlayer?.state == .playing {
            navController.jukeBoxPlayer?.stop()
        }
        
        let index = sender.view?.tag
        
        if (self.boletinData[index!].url_audio != ""){
            
            navController.tituloAudioActual = self.boletinData[index!].titulo
            navController.jukeBoxPlayer?.append(item: JukeboxItem(URL: URL(string: self.boletinData[index!].url_audio!)!), loadingAssets: true)
            
            navController.jukeBoxPlayer?.play()
            
        
        }
        
        let rowToSelect = IndexPath(row: index!, section: 0)
        self.tableBoletin.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none);
        self.tableBoletin.delegate!.tableView!(self.tableBoletin, didSelectRowAt: rowToSelect)
    }
    
} //fin de clase
