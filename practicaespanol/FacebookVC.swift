//
//  FacebookVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 26/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import WebKit
import SideMenu

class FacebookVC: UIViewController {
    
    var webV : WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        let url = URL (string: Constantes.facebookAddr)
        let requestObj = URLRequest(url: url!)
        
        self.webV = WKWebView(frame: self.view.frame)
        
        self.webV.load(requestObj)
        self.view.addSubview(self.webV)
        self.title = "Facebook"
        
        self.setNavBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }


}
