//
//  staticTextVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 13/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SideMenu
import SwiftyJSON
import Jukebox
import youtube_ios_player_helper

class staticTextVC: UIViewController {
    var sender = ""
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var scrollView : UIScrollView!
    
    var dataBoletin : Boletin?
    
    var tituloVC = ""
    
    let xfunciones = Functions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        if (self.sender != "boletin"){
            self.setNavBar()
            self.buildInterface()
        } else {
            self.buildInterfaceBoletin()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func buildInterface(){
        var strTitle = ""
        var staticText = ""
        
        var posY : CGFloat = 10
        
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        if (self.sender == "quienessomos"){
            let youtubePlayer = YTPlayerView(frame: CGRect(x: 0, y: posY, width: self.anchoPantalla, height: self.anchoPantalla * 0.7))
            youtubePlayer.load(withVideoId: Constantes.quienesSomosYTVideoID)
            self.scrollView.addSubview(youtubePlayer)
            posY = youtubePlayer.frame.maxY + 10
            youtubePlayer.center.x = self.anchoPantalla / 2
        }
        
        let indexIdioma = Functions().getIdiomaIndex()
        
        switch self.sender {
        case "quienessomos":
            strTitle = idiomasClass().arrayIdiomas[indexIdioma].menu_quienes_somos
            staticText = "Practica Español es una web de Agencia EFE SAU (primera agencia informativa mundial en español del grupo SEPI con cobertura en 120 países de los cinco continentes) para practicar el idioma español a través de la noticias.\n\nLa APP de Practica Español contiene noticias de la actualidad internacional clasificadas por temática (España, mundo, deportes, entretenimiento, viajes, salud, economía, cultura, ciencia, gastronomía... ) y niveles de competencia lingüistica: básico ( A1/A2), intermedio (B1/B2) y avanzado (C1/C2).\n\nTextos, audios, vídeos, boletines informativos, vocabulario español y ejercicios para leer, escuchar, practicar y mejorar la comprensión de un idioma que hablan en el mundo más de 500 millones de personas."
            break
        case "privacidad":
            strTitle = idiomasClass().arrayIdiomas[indexIdioma].menu_privacidad
            staticText = "La APP de Practica Español es propiedad de Agencia EFE SAU, del grupo SEPI, y esta sujeta a lo dispuesto en política de privacidad a la Ley Orgánica 15/1999, de 13 de Diciembre, de Protección de Datos de Carácter Personal (LOPD), así como en el Real Decreto 1720/2007 de 21 de diciembre por el que se aprueba el Reglamento que la desarrolla.\n\nAGENCIA EFE S.A.U. ha creado un registro ante la Agencia Española de Protección de Datos y requiere el consentimiento del afectado en los casos en los que se recoge información de carácter personal.\n\nEl Usuario puede ejercer sus derechos de acceso, rectificación, cancelación y oposición al tratamiento de sus datos de carácter personal mediante el envío de una notificación por escrito, adjuntando fotocopia de un documento que acredite su identidad, a la siguiente dirección:\n\n\n\nAv. de Burgos, 8b\n28036 Madrid (España)."
            break
        default: break
        }
        
        self.title = strTitle.uppercased()
        
        let label = UILabel(frame: CGRect(x: 10, y: posY, width: self.view.frame.size.width * 0.92, height: 10))
        label.text = staticText
        label.numberOfLines = 0
        label.textColor = UIColor.lightGray
        label.sizeToFit()
        
        self.scrollView.addSubview(label)
        
        self.scrollView.contentSize.height = posY + (self.altoPantalla * 0.25)
        
        self.view.addSubview(self.scrollView)
    }
    
    func buildInterfaceBoletin(){
        var posy : CGFloat = 0
        
        
        
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        
        //nav bar
        
        //visto
        let vistoImg   = UIImage(named: "eyeblanco")!
        let vistoButton   = UIButton.init(type: .custom)
        vistoButton.setImage(vistoImg, for: .normal)
        vistoButton.addTarget(self, action:#selector(self.marcarDesmarcarVisto), for: UIControlEvents.touchUpInside)
        vistoButton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        let vistoBarButton = UIBarButtonItem.init(customView: vistoButton)
        
        self.navigationItem.rightBarButtonItems = [vistoBarButton]
        
        //titulo
        var xframe = CGRect(x: 10, y: 10, width: self.anchoPantalla * 0.8, height: 10)
        let tituloBoletin = UILabel(frame: xframe)
        
        self.tituloVC = (self.dataBoletin?.titulo)!
        self.title = self.tituloVC  
        
        var font = UIFont.boldSystemFont(ofSize: 20)
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightBold)
        }
        
        tituloBoletin.text = self.tituloVC
        tituloBoletin.numberOfLines = 0
        tituloBoletin.font = font
        tituloBoletin.sizeToFit()
        posy = tituloBoletin.frame.maxY
        
        self.scrollView.addSubview(tituloBoletin)
        
        //fecha
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: 300, height: 10)
        
        font = UIFont.systemFont(ofSize: 13)
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightRegular)
        }
        
        let fechaBoletin = UILabel(frame: xframe)
        fechaBoletin.text = self.dataBoletin?.fecha
        fechaBoletin.font = font
        fechaBoletin.textColor = UIColor(red: 178/255, green: 29/255, blue: 33/255, alpha: 1)
        fechaBoletin.sizeToFit()
        
        posy = fechaBoletin.frame.maxY
        
        self.scrollView.addSubview(fechaBoletin)
        
        
        //contenido
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: self.anchoPantalla * 0.9, height: 10)
        let contenidoBoletin = UITextView(frame: xframe)
        
        let htmlContenido = Functions().stringFromHtml(string: (self.dataBoletin?.contenido)!, tipo: "texto")

        contenidoBoletin.attributedText = htmlContenido
        font = UIFont.systemFont(ofSize: 16)
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium)
        }
        contenidoBoletin.font = font
        contenidoBoletin.isScrollEnabled = false
        contenidoBoletin.isEditable = false
        contenidoBoletin.sizeToFit()
        
        self.scrollView.addSubview(contenidoBoletin)
        
        posy = contenidoBoletin.frame.maxY
        
        contenidoBoletin.center.x = self.anchoPantalla / 2
        
        //si tiene audio, boton de play
        if (self.dataBoletin?.url_audio != ""){
            xframe = CGRect(x: 10, y: posy + 15, width: self.anchoPantalla * 0.15, height: self.anchoPantalla * 0.15)
            let btnPlay = UIImageView(frame: xframe)
            
            btnPlay.contentMode = .scaleAspectFit
            btnPlay.image = UIImage(named: "play_negro")
            self.scrollView.addSubview(btnPlay)
            btnPlay.center.x = self.anchoPantalla / 2
            let tapPlay = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            btnPlay.isUserInteractionEnabled = true
            btnPlay.addGestureRecognizer(tapPlay)
            posy = btnPlay.frame.maxY
        }
        
        self.scrollView.contentSize.height = posy + (self.altoPantalla * 0.25)
        
        self.view.addSubview(self.scrollView)
        
        
        
    }
    
    func marcarDesmarcarVisto(){
        let resultado = Functions().agregarQuitarVisto(id_nota: (self.dataBoletin?.id)!)
        let alerta = Functions().alertaSimple(titulo: "", mensaje: resultado, estilo: .actionSheet, showButton: false)
        self.present(alerta, animated: true, completion: nil)
        Functions().dismissAlertDelay(alerta: alerta)
        
    }
    
    func playItem(sender: UITapGestureRecognizer){
        let navController = self.navigationController as! navControllerPrincipal
        if (navController.toolBarPlayer.isHidden){
            navController.toolBarPlayer.isHidden = false
            navController.view.bringSubview(toFront: navController.toolBarPlayer)
        } else {
            if (navController.jukeBoxPlayer?.currentItem != nil){
                navController.jukeBoxPlayer?.remove(item: (navController.jukeBoxPlayer?.currentItem)!)
            }
            
        }
        
        if navController.jukeBoxPlayer?.state == .playing {
            navController.jukeBoxPlayer?.stop()
        }
        
        if (self.dataBoletin?.url_audio != ""){
            navController.tituloAudioActual = self.tituloVC
            navController.jukeBoxPlayer?.append(item: JukeboxItem(URL: URL(string: (self.dataBoletin?.url_audio!)!)!), loadingAssets: true)
            navController.jukeBoxPlayer?.play()
        
        }
    }
    
} //fin clase
