//
//  PerfilNotificacionesVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 04/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SideMenu
import Firebase
import Kingfisher
import FacebookCore
import FacebookLogin

class PerfilNotificacionesVC: UIViewController {
    
    var imgUsuario : UIImageView!
    var lblNombreUsuario : UILabel!
    var btnFacebook : UIView!
    var btnLoginCorreo : UIView!
    var btnRegistro : UIView!
    var btnCerrarSesion : UIView!
    var switchA1 : UISwitch!
    var switchA2 : UISwitch!
    var switchB1 : UISwitch!
    var switchB2 : UISwitch!
    var switchC1 : UISwitch!
    var switchC2 : UISwitch!
    var scrollView : UIScrollView!
    
    var usrLogged = false
    
    var primeraVez = true

    var altoPantalla : CGFloat = 0
    var anchoPantalla : CGFloat = 0
    
    var nombreUsuario = ""
    var urlImagen : URL!
    
    let notifSwitchToPrev = Notification.Name("gotoPrevious")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.altoPantalla = self.view.frame.size.height
        self.anchoPantalla = self.view.frame.size.width
        self.setNavBar()
        
        self.primeraVez = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        
        switch tipo {
        case "titulo":
            font = UIFont.boldSystemFont(ofSize: 17)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)
            }
            break
        case "nombreUsr":
            font = UIFont.systemFont(ofSize: 19)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 19, weight: UIFontWeightBold)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 17)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightLight)
            }
            break
        }
        
        return font
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let index = Functions().getIdiomaIndex()
        
        self.title = idiomasClass().arrayIdiomas[index].menu_perfil_notif.uppercased()
        
        if let user = FIRAuth.auth()?.currentUser {
            self.usrLogged = true
            self.nombreUsuario = user.displayName!
            if let url = user.photoURL {
                self.urlImagen = url
            }
            
        } else {
            self.usrLogged = false
        }
        
        if (!self.primeraVez){
            self.scrollView.removeFromSuperview()
        }
        
        self.buildInterface()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    
    func buildInterface(){
        self.view.backgroundColor = UIColor.white
        
        self.view.frame.origin = CGPoint.zero
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        self.primeraVez = false
        
        var xframe = CGRect.zero
        var posy : CGFloat = 0
        
        let indexIdioma = Functions().getIdiomaIndex()
        
        //imagen
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: self.anchoPantalla * 0.4, height: self.anchoPantalla * 0.4)
        self.imgUsuario = UIImageView(frame: xframe)
        
        if (self.usrLogged && self.urlImagen != nil) {
            self.imgUsuario.kf.setImage(with: self.urlImagen)
            self.imgUsuario.kf.indicatorType = .activity
        } else {
            self.imgUsuario.image = UIImage(named: "profile01")
        }
        
        self.imgUsuario.layer.cornerRadius = self.imgUsuario.frame.size.width / 2
        self.imgUsuario.clipsToBounds = true
        self.scrollView.addSubview(self.imgUsuario)
        self.imgUsuario.center.x = self.anchoPantalla / 2
        
        posy = self.imgUsuario.frame.maxY
        
        //label nombre
        xframe.origin = CGPoint(x: 10, y: posy + 5)
        xframe.size = CGSize(width: self.anchoPantalla, height: 30)
        self.lblNombreUsuario = UILabel(frame: xframe)
        if (self.usrLogged){
            self.lblNombreUsuario.text = self.nombreUsuario
        } else {
            self.lblNombreUsuario.text = ""
        }
        self.lblNombreUsuario.font = self.getFont(tipo: "nombreUsr")
        self.lblNombreUsuario.sizeToFit()
        self.scrollView.addSubview(self.lblNombreUsuario)
        self.lblNombreUsuario.center.x = self.anchoPantalla / 2
        //ajustar fuente de texto
        
        posy = self.lblNombreUsuario.frame.maxY
        
        
        let altoBoton = self.anchoPantalla * 0.7
        let anchoBoton = self.anchoPantalla * 0.15
        
        if (self.usrLogged) { //logueado
            //btn cerrar sesion
            
            xframe.origin = CGPoint(x: 10, y: posy + 10)
            xframe.size = CGSize(width: altoBoton, height: anchoBoton)
            self.btnCerrarSesion = UIView(frame: xframe)
            self.btnCerrarSesion.backgroundColor = UIColor.white
            self.btnCerrarSesion.layer.cornerRadius = 5
            self.btnCerrarSesion.layer.borderWidth = 1
            self.btnCerrarSesion.layer.borderColor = Constantes.paletaColores.bordeRojoBotonesLogin.cgColor
            self.scrollView.addSubview(self.btnCerrarSesion)
            self.btnCerrarSesion.center.x = self.anchoPantalla / 2
            self.btnCerrarSesion.isUserInteractionEnabled = true
            let tapCerrarSesion = UITapGestureRecognizer(target: self, action: #selector(self.confirmaCerrarSesion))
            self.btnCerrarSesion.addGestureRecognizer(tapCerrarSesion)
            
            //label cerrar
            let lblBtnCerrar = UILabel(frame: xframe)
            lblBtnCerrar.text = "Cerrar sesión"
            lblBtnCerrar.textColor = Constantes.paletaColores.rojoBtnLoginEmail
            lblBtnCerrar.sizeToFit()
            self.btnCerrarSesion.addSubview(lblBtnCerrar)
            lblBtnCerrar.center.x = self.btnCerrarSesion.frame.size.width / 2
            lblBtnCerrar.center.y = self.btnCerrarSesion.frame.size.height / 2
            
            posy = self.btnCerrarSesion.frame.maxY
            
            
        } else { //no logueado
            //boton facebook
            
            xframe.origin = CGPoint(x: 10, y: posy + 15)
            xframe.size = CGSize(width: altoBoton, height: anchoBoton)
            self.btnFacebook = UIView(frame: xframe)
            self.btnFacebook.backgroundColor = Constantes.paletaColores.azulBtnFacebook
            self.btnFacebook.layer.cornerRadius = 5
            self.btnFacebook.layer.borderWidth = 0.5
            self.btnFacebook.layer.borderColor = Constantes.paletaColores.bordeAzulBotonesLogin.cgColor
            self.scrollView.addSubview(self.btnFacebook)
            self.btnFacebook.center.x = self.anchoPantalla / 2
            let tapBtnFaceb = UITapGestureRecognizer(target: self, action: #selector(self.gotoLoginFacebook))
            self.btnFacebook.isUserInteractionEnabled = true
            self.btnFacebook.addGestureRecognizer(tapBtnFaceb)
            
            //label facebook
            let lblBtnFacebook = UILabel(frame: xframe)
            lblBtnFacebook.text = idiomasClass().arrayIdiomas[indexIdioma].btn_facebook
            lblBtnFacebook.textColor = UIColor.white
            lblBtnFacebook.sizeToFit()
            self.btnFacebook.addSubview(lblBtnFacebook)
            lblBtnFacebook.center.x = self.btnFacebook.frame.size.width / 2
            lblBtnFacebook.center.y = self.btnFacebook.frame.size.height / 2
            
            posy = self.btnFacebook.frame.maxY
            
            //boton login con correo
            xframe.origin = CGPoint(x: 10, y: posy + 10)
            xframe.size = CGSize(width: altoBoton, height: anchoBoton)
            self.btnLoginCorreo = UIView(frame: xframe)
            self.btnLoginCorreo.backgroundColor = Constantes.paletaColores.rojoBtnLoginEmail
            self.btnLoginCorreo.layer.cornerRadius = 5
            self.btnLoginCorreo.layer.borderWidth = 0.5
            self.btnLoginCorreo.layer.borderColor = Constantes.paletaColores.bordeAzulBotonesLogin.cgColor
            self.btnLoginCorreo.isUserInteractionEnabled = true
            let tapLCorreo = UITapGestureRecognizer(target: self, action: #selector(self.gotoLoginEmail))
            self.btnLoginCorreo.addGestureRecognizer(tapLCorreo)
            self.scrollView.addSubview(self.btnLoginCorreo)
            self.btnLoginCorreo.center.x = self.anchoPantalla / 2
            
            //label login correo
            let lblBtnLoginCorreo = UILabel(frame: xframe)
            lblBtnLoginCorreo.text = idiomasClass().arrayIdiomas[indexIdioma].btn_email
            lblBtnLoginCorreo.textColor = UIColor.white
            lblBtnLoginCorreo.sizeToFit()
            self.btnLoginCorreo.addSubview(lblBtnLoginCorreo)
            lblBtnLoginCorreo.center.x = self.btnLoginCorreo.frame.size.width / 2
            lblBtnLoginCorreo.center.y = self.btnLoginCorreo.frame.size.height / 2
            
            posy = self.btnLoginCorreo.frame.maxY
            
            //boton registro
            xframe.origin = CGPoint(x: 10, y: posy + 10)
            xframe.size = CGSize(width: altoBoton, height: anchoBoton)
            self.btnRegistro = UIView(frame: xframe)
            self.btnRegistro.backgroundColor = UIColor.white
            self.btnRegistro.layer.cornerRadius = 5
            self.btnRegistro.layer.borderWidth = 1
            self.btnRegistro.layer.borderColor = Constantes.paletaColores.bordeRojoBotonesLogin.cgColor
            self.scrollView.addSubview(self.btnRegistro)
            self.btnRegistro.center.x = self.anchoPantalla / 2
            self.btnRegistro.isUserInteractionEnabled = true
            let tapRegistro = UITapGestureRecognizer(target: self, action: #selector(self.gotoRegistro))
            self.btnRegistro.addGestureRecognizer(tapRegistro)
            
            //label registro
            let lblBtnRegistro = UILabel(frame: xframe)
            lblBtnRegistro.text = idiomasClass().arrayIdiomas[indexIdioma].btn_registrarse
            lblBtnRegistro.textColor = Constantes.paletaColores.rojoBtnLoginEmail
            lblBtnRegistro.sizeToFit()
            self.btnRegistro.addSubview(lblBtnRegistro)
            lblBtnRegistro.center.x = self.btnRegistro.frame.size.width / 2
            lblBtnRegistro.center.y = self.btnRegistro.frame.size.height / 2
            
            posy = self.btnRegistro.frame.maxY
        }
        
        //label notificaciones
        xframe.origin = CGPoint(x: 10, y: posy + 20)
        xframe.size = CGSize(width: 10, height: 10)
        let lblTituloNotificacion = UILabel(frame: xframe)
        lblTituloNotificacion.text = "Notificaciones"
        lblTituloNotificacion.font = self.getFont(tipo: "titulo")
        lblTituloNotificacion.sizeToFit()
        self.scrollView.addSubview(lblTituloNotificacion)
        
        posy = lblTituloNotificacion.frame.maxY
        
        //texto notificaciones
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblTextoNotif = UILabel(frame: xframe)
        lblTextoNotif.text = "Señala cual es el nivel de Español que estas preparando para notificarte cada vez que subamos contenido de tu nivel"
        lblTextoNotif.numberOfLines = 0
        lblTextoNotif.frame.size.width = self.anchoPantalla * 0.9
        self.scrollView.addSubview(lblTextoNotif)
        lblTextoNotif.sizeToFit()
        
        posy = lblTextoNotif.frame.maxY
        
        //switches
        //B1
        xframe.origin = CGPoint(x: 10, y: posy + 15)
        xframe.size = CGSize(width: 90, height: 10)
        self.switchB1 = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.switchB1)
        self.switchB1.addTarget(self, action: #selector(self.subscribeUnsubscribeTopic(sender:)), for: .valueChanged)
        
        self.switchB1.center.x = self.anchoPantalla / 2
        
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblSWB1 = UILabel(frame: xframe)
        lblSWB1.text = "B1"
        self.scrollView.addSubview(lblSWB1)
        lblSWB1.sizeToFit()
        
        lblSWB1.center.y = self.switchB1.frame.minY + (self.switchB1.frame.size.height / 2)
        lblSWB1.frame.origin.x = self.switchB1.frame.minX - (5 + lblSWB1.frame.size.width)
        
        var posx = lblSWB1.frame.origin.x
        
        //A1
        xframe.origin = CGPoint(x: posx - 80, y: posy + 15)
        xframe.size = CGSize(width: 90, height: 10)
        self.switchA1 = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.switchA1)
        
        self.switchA1.addTarget(self, action: #selector(self.subscribeUnsubscribeTopic(sender:)), for: .valueChanged)
        
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblSWA1 = UILabel(frame: xframe)
        lblSWA1.text = "A1"
        self.scrollView.addSubview(lblSWA1)
        lblSWA1.sizeToFit()
        
        lblSWA1.center.y = self.switchA1.frame.minY + (self.switchA1.frame.size.height / 2)
        lblSWA1.frame.origin.x = self.switchA1.frame.minX - (5 + lblSWA1.frame.size.width)
        
        
        posx = self.switchB1.frame.maxX
        
        //C1
        xframe.origin = CGPoint(x: posx + 30, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblSWC1 = UILabel(frame: xframe)
        lblSWC1.text = "C1"
        self.scrollView.addSubview(lblSWC1)
        lblSWC1.sizeToFit()
        
        posx = lblSWC1.frame.maxX
        
        xframe.origin = CGPoint(x: posx + 5, y: posy + 15)
        xframe.size = CGSize(width: 90, height: 10)
        self.switchC1 = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.switchC1)
        
        self.switchC1.addTarget(self, action: #selector(self.subscribeUnsubscribeTopic(sender:)), for: .valueChanged)
        
        lblSWC1.center.y = self.switchC1.frame.minY + (self.switchC1.frame.size.height / 2)
        
        posy = self.switchB1.frame.maxY
        
        //B2
        xframe.origin = CGPoint(x: 10, y: posy + 20)
        xframe.size = CGSize(width: 90, height: 10)
        self.switchB2 = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.switchB2)
        
        self.switchB2.addTarget(self, action: #selector(self.subscribeUnsubscribeTopic(sender:)), for: .valueChanged)
        
        self.switchB2.center.x = self.anchoPantalla / 2
        
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblSWB2 = UILabel(frame: xframe)
        lblSWB2.text = "B2"
        self.scrollView.addSubview(lblSWB2)
        lblSWB2.sizeToFit()
        
        lblSWB2.center.y = self.switchB2.frame.minY + (self.switchB2.frame.size.height / 2)
        lblSWB2.frame.origin.x = self.switchB2.frame.minX - (5 + lblSWB2.frame.size.width)
        
        posx = lblSWB2.frame.origin.x
        
        //A2
        xframe.origin = CGPoint(x: posx - 80, y: posy + 20)
        xframe.size = CGSize(width: 90, height: 10)
        self.switchA2 = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.switchA2)
        
        self.switchA2.addTarget(self, action: #selector(self.subscribeUnsubscribeTopic(sender:)), for: .valueChanged)
        
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblSWA2 = UILabel(frame: xframe)
        lblSWA2.text = "A2"
        self.scrollView.addSubview(lblSWA2)
        lblSWA2.sizeToFit()
        
        lblSWA2.center.y = self.switchA2.frame.minY + (self.switchA2.frame.size.height / 2)
        lblSWA2.frame.origin.x = self.switchA2.frame.minX - (5 + lblSWA2.frame.size.width)
        
        
        posx = self.switchB2.frame.maxX
        
        //C2
        xframe.origin = CGPoint(x: posx + 30, y: posy + 10)
        xframe.size = CGSize(width: 10, height: 10)
        let lblSWC2 = UILabel(frame: xframe)
        lblSWC2.text = "C2"
        self.scrollView.addSubview(lblSWC2)
        lblSWC2.sizeToFit()
        
        posx = lblSWC2.frame.maxX
        
        xframe.origin = CGPoint(x: posx + 5, y: posy + 20)
        xframe.size = CGSize(width: 90, height: 10)
        self.switchC2 = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.switchC2)
        
        self.switchC2.addTarget(self, action: #selector(self.subscribeUnsubscribeTopic(sender:)), for: .valueChanged)
        
        lblSWC2.center.y = self.switchC2.frame.minY + (self.switchC2.frame.size.height / 2)
        
        posy = self.switchB2.frame.maxY
        
        self.scrollView.contentSize.height = posy + self.altoPantalla * 0.25
        
        self.view.addSubview(self.scrollView)
        
        self.marcaSwitches()
    }
    
    func gotoRegistro(){
        let registroVC = RegistroUsuarioVC()
        self.navigationController?.pushViewController(registroVC, animated: true)
    }
    
    func gotoLoginEmail(){
        let VCloginEmail = LoginEmailVC()
        self.navigationController?.pushViewController(VCloginEmail, animated: true)
    }
    
    func gotoLoginFacebook(){
        let lmanager = LoginManager()
        
        lmanager.logIn([.publicProfile, .email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print("Logged in!")
                if declinedPermissions.contains("email") {}
                if grantedPermissions.contains("email"){
                    self.singInToFirebaseFromFacebook(xaccessToken: accessToken)
                } else {
                    
                }
                
            }
        }
    }
    
    func singInToFirebaseFromFacebook(xaccessToken : AccessToken){
        let credential = FIRFacebookAuthProvider.credential(withAccessToken: xaccessToken.authenticationToken)
        
        FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            if error != nil {
                print(" JSON Error: \(error!) ")
                
                let mensaje = Functions().getFirAuthErrorMessage(elError: error!)
                
                let alerta = Functions().alertaSimple(titulo: "Error", mensaje: mensaje, estilo: .alert, showButton: false)
                self.present(alerta, animated: true, completion: nil)
                Functions().dismissAlertDelay(alerta: alerta)
                return
            }else{
                print("Login Successful: \(user)")
                NotificationCenter.default.post(name: self.notifSwitchToPrev, object: nil)
                _ = self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func confirmaCerrarSesion(){
        let alert = UIAlertController(title: "Cerrar sesión", message: "¿Esta seguro que desea cerrar su sesión?", preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: "Si", style: UIAlertActionStyle.default, handler: cerrarSesion))
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func cerrarSesion(action: UIAlertAction){
        if (action.title == "Si") {
            try! FIRAuth.auth()?.signOut()
            LoginManager().logOut()
            Functions().removeTodosFavoritos()
            NotificationCenter.default.post(name: self.notifSwitchToPrev, object: nil)
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func subscribeUnsubscribeTopic(sender: UISwitch){
        //FIRMessaging.messaging().subscribeToTopic("/topics/notificaciones_general")
        sender.isEnabled = false
        var topicURL = ""
        var nivel = ""
        switch sender {
        case self.switchA1:
            topicURL = Constantes.topics.topicURLA1
            nivel = "A1"
        case self.switchA2:
            topicURL = Constantes.topics.topicURLA2
            nivel = "A2"
        case self.switchB1:
            topicURL = Constantes.topics.topicURLB1
            nivel = "B1"
        case self.switchB2:
            topicURL = Constantes.topics.topicURLB2
            nivel = "B2"
        case self.switchC1:
            topicURL = Constantes.topics.topicURLC1
            nivel = "C1"
        case self.switchC2:
            topicURL = Constantes.topics.topicURLC2
            nivel = "C2"
        default:
            break
        }
        
        if topicURL != "" {
            var mensaje = ""
            Functions().agregarQuitarAlerta(nivel: topicURL, estado: sender.isOn)
            if sender.isOn {
                FIRMessaging.messaging().subscribe(toTopic: "/topics/\(topicURL)")
                mensaje = "Se ha suscrito al canal de notificaciones \(nivel)"
            } else {
                FIRMessaging.messaging().unsubscribe(fromTopic: "/topics/\(topicURL)")
                mensaje = "Ha cancelado su suscripción al canal de notificaciones \(nivel)"
            }
            
            let alerta = Functions().alertaSimple(titulo: "Éxito", mensaje: mensaje, estilo: .alert, showButton: true)
            self.present(alerta, animated: true, completion: nil)
            sender.isEnabled = true
            
        } else {
            print("No deberia llegar desde otro lado que no sea un switch")
        }
        
    } //fin de subscribe
    
    func marcaSwitches(){
        let dicAlertas = Functions().getDicAlertas()
        
        for (nivel, esActivo) in dicAlertas {
            switch nivel {
            case Constantes.topics.topicURLA1:
                self.switchA1.isOn = esActivo
            case Constantes.topics.topicURLA2:
                self.switchA2.isOn = esActivo
            case Constantes.topics.topicURLB1:
                self.switchB1.isOn = esActivo
            case Constantes.topics.topicURLB2:
                self.switchB2.isOn = esActivo
            case Constantes.topics.topicURLC1:
                self.switchC1.isOn = esActivo
            case Constantes.topics.topicURLC2:
                self.switchC2.isOn = esActivo
            default:
                break
            }
        }
    }
}
