//
//  navControllerPrincipal.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 11/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import MediaPlayer
import Foundation
import Jukebox
import SideMenu
import Firebase
import PKHUD

class navControllerPrincipal: UINavigationController, JukeboxDelegate {
    
    //para player
    var toolBarPlayer : UIToolbar!
    var playerContainerView: UIView!
    var btnPlay: UIImageView!
    var lblTitulo: UILabel!
    var lblTiempo: UILabel!
    var sliderTiempo: UISlider!
    var jukeBoxPlayer = Jukebox()
    var toolBarHeight : CGFloat = 0
    
    var tituloAudioActual = ""
    
    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    var actualVC = ""
    var previoVC = ""
    
    let notifPortada = Notification.Name("gotoPortada")
    let notifQuienesSomos = Notification.Name("gotoQuienesSomos")
    let notifPrivacidad = Notification.Name("gotoPrivacidad")
    let notifVozNoticia = Notification.Name("gotoVozNoticia")
    let notifNoConfundas = Notification.Name("gotoNoConfudas")
    let notifFacebook = Notification.Name("gotoFacebook")
    let notifPerfil = Notification.Name("gotoPerfil")
    let notifSwitchToPrev = Notification.Name("gotoPrevious")
    let notifFavoritos = Notification.Name("gotoFavoritos")
    let notifBoletin = Notification.Name("gotoBoletin")
    let notifIdiomas = Notification.Name("gotoIdiomas")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.actualVC = "portada"
        
        //para recibir eventos del lock screen
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        //observers
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoPortada), name: self.notifPortada, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoQuienesSomos), name: self.notifQuienesSomos, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoPrivacidad), name: self.notifPrivacidad, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoVozNoticia), name: self.notifVozNoticia, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoNoConfundas), name: self.notifNoConfundas, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoFacebook), name: self.notifFacebook, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoPerfil), name: self.notifPerfil, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.switchToPrevious), name: self.notifSwitchToPrev, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoFavoritos), name: self.notifFavoritos, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoBoletin), name: self.notifBoletin, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoIdiomas), name: self.notifIdiomas, object: nil)
        
        //fin observers
        
        let menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "sideMenuNC") as! UISideMenuNavigationController
        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
        
        SideMenuManager.menuPushStyle = .replace
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuWidth = max(round(min((self.view.frame.size.width), (self.view.frame.size.height)) * 0.7), 250)
        menuLeftNavigationController.leftSide = true
        
        
        
        //inicializa el player
        self.jukeBoxPlayer = Jukebox(delegate: self, items: [])!
        self.buildToolBar()
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        switch tipo {
        case "tiempo":
            font = UIFont.systemFont(ofSize: 12)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightThin)
            }
            break
        case "titulo":
            font = UIFont.systemFont(ofSize: 12)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightRegular)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 12)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightThin)
            }
            break
        }
        
        return font
    }
    
    func buildToolBar(){
        //toolbar
        self.toolBarHeight = self.view.frame.size.height * 0.1
        self.toolBarPlayer = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height - self.toolBarHeight, width: self.view.frame.size.width, height: toolBarHeight))
        self.view.addSubview(self.toolBarPlayer)
        
        //view container player
        self.playerContainerView = UIView(frame: CGRect(x: 0, y: 0, width: Int(self.toolBarPlayer.frame.size.width), height: Int(self.toolBarPlayer.frame.size.height)))
        self.playerContainerView.backgroundColor = UIColor.black
        
        //boton play
        let tamanioBntPlay = self.playerContainerView.frame.size.height * 0.7
        self.btnPlay = UIImageView(frame: CGRect(x: 10, y: 0, width: tamanioBntPlay, height: tamanioBntPlay))
        self.btnPlay.image = UIImage(named: "Circle blanco play")
        self.btnPlay.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.pausePlayAction(sender:)))
        self.btnPlay.isUserInteractionEnabled = true
        self.btnPlay.addGestureRecognizer(tap)
        self.btnPlay.center.y = self.playerContainerView.frame.size.height / 2
        self.playerContainerView.addSubview(self.btnPlay)
        
        //tiempo
        self.lblTiempo = UILabel(frame: CGRect(x: self.btnPlay.frame.maxX + 5, y: 10, width: 10, height: 15))
        self.lblTiempo.textColor = UIColor.white
        self.lblTiempo.text = "0:00 / 0:00"
        self.lblTiempo.font = self.getFont(tipo: "tiempo")
        self.lblTiempo.sizeToFit()
        self.playerContainerView.addSubview(self.lblTiempo)
        
        //titulo
        let anchotitulo = self.playerContainerView.frame.size.width - (self.btnPlay.frame.maxX + 10)
        self.lblTitulo = UILabel(frame: CGRect(x: self.btnPlay.frame.maxX + 5, y: self.lblTiempo.frame.maxY + 5, width: anchotitulo, height: 15))
        self.lblTitulo.textColor = UIColor.white
        self.lblTitulo.text = ""
        self.lblTitulo.lineBreakMode = .byTruncatingTail
        self.lblTitulo.font = self.getFont(tipo: "titulo")
        self.playerContainerView.addSubview(self.lblTitulo)
        
        //slider
        let widthSlider = self.playerContainerView.frame.size.width - (self.lblTiempo.frame.maxX + 30)
        self.sliderTiempo = UISlider(frame: CGRect(x: self.lblTiempo.frame.maxX + 20, y: 5, width: widthSlider, height: 15))
        self.sliderTiempo.isContinuous = true
        self.sliderTiempo.minimumTrackTintColor = UIColor.red
        self.sliderTiempo.setThumbImage(UIImage(named: "circulo_slider"), for: .normal)
        self.sliderTiempo.addTarget(self, action: #selector(self.progressSliderValueChanged), for: UIControlEvents.valueChanged)
        self.playerContainerView.addSubview(self.sliderTiempo)
        
        self.toolBarPlayer.addSubview(self.playerContainerView)
        
        self.toolBarPlayer.isHidden = true //cambiar a true
        //self.view.bringSubview(toFront: self.toolBarPlayer) // quitar
        
        
    }
    
    // MARK:- JukeboxDelegate -
    
    //esto no es parte del delegate, pero es para el boton de play
    func pausePlayAction(sender: UITapGestureRecognizer){
        
        if self.jukeBoxPlayer?.state == .playing {
            self.jukeBoxPlayer?.pause()
        }
        else if self.jukeBoxPlayer?.state == .paused {
            self.jukeBoxPlayer?.play()
        } else if self.jukeBoxPlayer?.state == .ready {
            self.jukeBoxPlayer?.play()
        }
        
    }
    
    func progressSliderValueChanged() {
        if let duration = self.jukeBoxPlayer?.currentItem?.meta.duration {
            self.jukeBoxPlayer?.seek(toSecond: Int(Double(self.sliderTiempo.value) * duration))
        }
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        
        DispatchQueue.main.async {
            print("Jukebox did load: \(item.URL.lastPathComponent)")
            print("Jukebox titulo nota: \(self.tituloAudioActual)")
            if (self.tituloAudioActual != ""){
                self.lblTitulo.text = self.tituloAudioActual
            } else {
                self.lblTitulo.text = item.URL.lastPathComponent
            }
        }
        
        
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            let value = Float(currentTime / duration)
            
            self.sliderTiempo.value = value
            
            self.populateLabelWithTime(self.lblTiempo, time: currentTime, duration: duration)
        } else {
            print("No puedo traer el tiempo actual o la duracion del audio")
        }
    }
    
    func populateLabelWithTime(_ label : UILabel, time: Double, duration: Double) {
        //tiempo actual
        var minutes = Int(time / 60)
        var seconds = Int(time) - minutes * 60
        var stringTiempo = String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
        
        minutes = Int(duration / 60)
        seconds = Int(duration) - minutes * 60
        stringTiempo += " / "
        stringTiempo += String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
        DispatchQueue.main.async {
            label.text = stringTiempo
            label.sizeToFit()
        }
        
    }
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        
        self.btnPlay.isUserInteractionEnabled = jukebox.state == .loading ? false : true
        
        if jukebox.state == .ready {
            self.btnPlay.image = UIImage(named: "Circle blanco play")
        } else if jukebox.state == .loading  {
            self.btnPlay.image = UIImage(named: "btn_pausa")
            self.lblTitulo.text = "Cargando..."
        } else {
            let imageName: String
            switch jukebox.state {
            case .playing, .loading:
                imageName = "btn_pausa"
            case .paused, .failed, .ready:
                imageName = "Circle blanco play"
            }
            self.btnPlay.image = UIImage(named: imageName)
        }
        
        print("Jukebox state changed to \(jukebox.state)")
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        //print("Item updated:\n\(forItem)")
    }
    
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                self.jukeBoxPlayer?.play()
            case .remoteControlPause :
                self.jukeBoxPlayer?.pause()
            case .remoteControlNextTrack :
                self.jukeBoxPlayer?.playNext()
            case .remoteControlPreviousTrack:
                self.jukeBoxPlayer?.playPrevious()
            case .remoteControlTogglePlayPause:
                if self.jukeBoxPlayer?.state == .playing {
                    self.jukeBoxPlayer?.pause()
                } else {
                    self.jukeBoxPlayer?.play()
                }
            default:
                break
            }
        }
    }
    //-- fin jukebox delegate
    
    func switchToPrevious(){
        self.actualVC = self.previoVC
        self.previoVC = ""
    }
    
    func gotoQuienesSomos(){
        if (self.actualVC != "quienessomos"){
            let vcStaticTextVC = staticTextVC()
            vcStaticTextVC.sender = "quienessomos"
            self.pushViewController(vcStaticTextVC, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "quienessomos"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoPrivacidad(){
        if (self.actualVC != "privacidad"){
            let vcStaticTextVC = staticTextVC()
            vcStaticTextVC.sender = "privacidad"
            self.pushViewController(vcStaticTextVC, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "privacidad"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoPortada(){
        if (self.actualVC != "portada"){
            if (self.viewControllers.count > 1){
                for index in stride(from: self.viewControllers.count - 1, to: 0, by: -1) {
                    self.viewControllers.remove(at: index)
                }
            }
            self.previoVC = self.actualVC
            self.actualVC = "portada"
        }
    }
    
    func gotoVozNoticia(){
        if (self.actualVC != "vozNoticia"){
            let vcVozNoticia = self.mainStoryboard.instantiateViewController(withIdentifier: "vozNoticiaVC") as! vozNoticiaVC
            vcVozNoticia.callFromMenu = true
            self.pushViewController(vcVozNoticia, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "vozNoticia"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoNoConfundas(){
        if (self.actualVC != "noConfundas"){
            let vcNoConfundas = self.mainStoryboard.instantiateViewController(withIdentifier: "noConfundasVC") as! noConfundasVC
            self.pushViewController(vcNoConfundas, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "noConfundas"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoFacebook(){
        if (self.actualVC != "facebook"){
            let vcfacebook = FacebookVC()
            self.pushViewController(vcfacebook, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "facebook"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoPerfil(){
        if (self.actualVC != "perfil"){
            let vcPerfil = PerfilNotificacionesVC()
            self.pushViewController(vcPerfil, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "perfil"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoFavoritos(){
        
        //solo puede ir a favoritos si esta logueado
        if let _ = FIRAuth.auth()?.currentUser {
            if (self.actualVC != "favoritos"){
                let vcFav = FavoritosVC()
                self.pushViewController(vcFav, animated: true)
                self.previoVC = self.actualVC
                self.actualVC = "favoritos"
                if ((self.viewControllers.count) > 2){
                    self.viewControllers.remove(at: 1)
                }
            }
        } else { //sino indicalo con una amable alerta
            let index = Functions().getIdiomaIndex()
            let msg = idiomasClass().arrayIdiomas[index].msg_inicio_sesion_favoritos
            let alerta = Functions().alertaSimple(titulo: "", mensaje: msg, estilo: .alert, showButton: false)
            present(alerta, animated: true, completion: nil)
            Functions().dismissAlertDelay(alerta: alerta)
            
        }
        
    }
    
    func gotoBoletin(){
        if (self.actualVC != "boletin"){
            //BoletinVC
            let vcBoletin = self.mainStoryboard.instantiateViewController(withIdentifier: "BoletinVC") as! BoletinVC
            vcBoletin.callFromMenu = true
            self.pushViewController(vcBoletin, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "boletin"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
    
    func gotoIdiomas(){
        if (self.actualVC != "idiomas"){
            let vcIdiomas = IdiomasVC()
            self.pushViewController(vcIdiomas, animated: true)
            self.previoVC = self.actualVC
            self.actualVC = "idiomas"
            if ((self.viewControllers.count) > 2){
                self.viewControllers.remove(at: 1)
            }
        }
    }
}
