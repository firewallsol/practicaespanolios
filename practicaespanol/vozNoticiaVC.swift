//
//  vozNoticiaVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 18/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import Jukebox
import SideMenu
import PKHUD

class vozNoticiaVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    var altoNavBar : CGFloat = 0
    var altoStatusBar : CGFloat = 0
    
    var noticiasData = [Noticia]()
    var noticiasRespaldo = [Noticia]()
    var tableNoticias : UITableView!
    
    var dataLoaded = false
    
    var callFromMenu = false
    
    var xfunciones = Functions()
    
    var offsetAlto : CGFloat = 0
    
    var isDataLoading = false
    var pageNo : Int = 1
    
    var vistoOnOff = false
    
    let notifFiltroVozN = Notification.Name("aplicaFiltroVozNoticia")
    
    var celdaSeleccionada : IndexPath!
    
    var primeraCarga = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        //self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)!
        self.altoStatusBar = self.xfunciones.statusBarHeight()
        
        self.offsetAlto = self.view.frame.size.height * 0.33
        
        self.view.tag = 2
        
        if callFromMenu {
            self.setNavBar()
            
            HUD.show(.labeledProgress(title: "", subtitle: "Cargando..."))
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDatosFiltro), name: self.notifFiltroVozN, object: nil)
        
        self.buildInterface()
        //self.xfunciones.startActivityIndicator(sender: self)
        
        self.getData()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        
        switch tipo {
        case "lblDif":
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
            }
            break
        }
        
        return font
    }

    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
        
        //filtro
        let imgFiltro = UIImage(named: "filter2")
        let filterButton = UIBarButtonItem(image: imgFiltro, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.filterButtonTap))
        
        self.navigationItem.rightBarButtonItem = filterButton
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func filterButtonTap(){
        let vcFiltro = filtroVistoNoVistoVC()
        vcFiltro.vistoOnOff = self.vistoOnOff
        vcFiltro.sender = "vozNoticia"
        vcFiltro.buildInterface()
        self.navigationController?.pushViewController(vcFiltro, animated: true)
    }
    
    func getDatosFiltro(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        self.vistoOnOff = paramFiltro["visto"] as! Bool
        self.aplicafiltro()
    }
    
    func aplicafiltro(){
        self.noticiasData = self.noticiasRespaldo
        //self.pageNo = 1
        var newArray = [Noticia]()
        
        if self.vistoOnOff {
            for item in self.noticiasData {
                if xfunciones.existeIdEnVisto(id_nota: item.id){
                    newArray.append(item)
                }
            }
            self.noticiasData = newArray
        }
        self.dataLoaded = true
        self.tableNoticias.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.callFromMenu {
            self.title = "LA VOZ EN ESPAÑOL"
        }
        
        if self.celdaSeleccionada != nil {
            self.tableNoticias.reloadRows(at: [self.celdaSeleccionada], with: .automatic)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.callFromMenu {
            self.title = ""
        }
    }
    
    func buildInterface(){
        
        self.tableNoticias = UITableView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        self.view.addSubview(self.tableNoticias)
        self.tableNoticias.dataSource = self
        self.tableNoticias.delegate = self
        self.tableNoticias.register(noticiasCell.self, forCellReuseIdentifier: "noticiasCell")
        self.tableNoticias.rowHeight = 150
        self.tableNoticias.separatorStyle = .none
    }
    
    func getData(){
        HUD.show(.labeledProgress(title: "", subtitle: "Cargando..."))
        let urlrequest = Constantes.WSData.urlRestBase + "?filter[category_name]=la-voz-de-la-noticia&page=\(self.pageNo)"
        Alamofire.request("\(urlrequest)").responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let jsonData = JSON(value)
                if ((jsonData.array?.count)! > 0){
                    self.noticiasData = self.noticiasRespaldo
                    if self.pageNo == 1 {
                        self.noticiasData = self.xfunciones.JSONtoArrayNoticia(elJson: jsonData)
                    } else {
                        self.noticiasData.append(contentsOf: self.xfunciones.JSONtoArrayNoticia(elJson: jsonData))
                    }
                    
                    self.noticiasRespaldo = self.noticiasData
                    
                    if (!self.dataLoaded){
                        var insets = self.tableNoticias.contentInset
                        insets.bottom += self.offsetAlto
                        self.tableNoticias.contentInset = insets
                    }
                    
                    self.aplicafiltro()
                }
                if self.primeraCarga {
                    HUD.hide()
                }
                
            case .failure(let error):
                print(error)
                if self.callFromMenu {
                    HUD.hide()
                    let alerta = Functions().alertaSimple(titulo: "Error", mensaje: "Ha ocurrido un error al intentar conectarse al servidor", estilo: .alert, showButton: true)
                    self.present(alerta, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    func playItem(sender: UITapGestureRecognizer){
        let navController = self.navigationController as! navControllerPrincipal
        if (navController.toolBarPlayer.isHidden){
            navController.toolBarPlayer.isHidden = false
            navController.view.bringSubview(toFront: navController.toolBarPlayer)
        } else {
            if (navController.jukeBoxPlayer?.currentItem != nil){
                navController.jukeBoxPlayer?.remove(item: (navController.jukeBoxPlayer?.currentItem)!)
            }
            
        }
        
        if navController.jukeBoxPlayer?.state == .playing {
            navController.jukeBoxPlayer?.stop()
        }
        
        let index = sender.view?.tag
        
        if (self.noticiasData[index!].url_audio != ""){
            navController.tituloAudioActual = self.noticiasData[index!].titulo
            navController.jukeBoxPlayer?.append(item: JukeboxItem(URL: URL(string: self.noticiasData[index!].url_audio!)!), loadingAssets: true)
            navController.jukeBoxPlayer?.play()
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataLoaded {
            return self.noticiasData.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableNoticias.dequeueReusableCell(withIdentifier: "noticiasCell")! as! noticiasCell
        
        cell.frame.size.width = self.anchoPantalla
        
        if self.dataLoaded {
            //img
            
            var frameVar = cell.imgFondo.frame
            frameVar.origin = CGPoint(x: 0, y: 0)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.98, height: cell.frame.size.height * 0.97)
            cell.imgFondo.frame = frameVar
            cell.viewOpacoImg.frame = frameVar
            if (self.noticiasData[indexPath.row].url_imagen != ""){
                let imgurl = URL(string: self.noticiasData[indexPath.row].url_imagen!)
                cell.imgFondo.kf.setImage(with: imgurl, placeholder: UIImage(named: "placeHolderImagenGris"))
                cell.imgFondo.kf.indicatorType = .activity
            } else {
                cell.imgFondo.image = UIImage(named: "placeHolderImagenGris")
            }
            cell.imgFondo.clipsToBounds = true
            cell.imgFondo.contentMode = .scaleAspectFill
            cell.imgFondo.center.x = cell.frame.size.width / 2
            cell.imgFondo.center.y = cell.frame.size.height / 2
            cell.viewOpacoImg.center = cell.imgFondo.center
            
            //fin imagen
            
            //titulo
            cell.lblTitulo.text = self.noticiasData[indexPath.row].titulo
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: 10, y: 10)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.9, height: 30)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            var posy = cell.frame.size.height - (cell.lblTitulo.frame.size.height + 10)
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: 10, y: posy)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.9, height: frameVar.size.height)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            // fin titulo
            
            /* quitar - la voz es una seccion
            //seccion
            let strSeccion = self.funciones.getSeccionNoticia(arr_ids: self.noticiasData[indexPath.row]["categories"].arrayObject as! [Int])
            cell.lblSeccion.text = strSeccion
            cell.lblSeccion.sizeToFit()
            frameVar = cell.lblSeccion.frame
            posy = cell.lblTitulo.frame.minY - (cell.lblSeccion.frame.size.height + 5)
            frameVar.origin = CGPoint(x:10, y:posy)
            cell.lblSeccion.frame = frameVar
            //fin seccion
            */
            
            //dificultad
            frameVar = cell.viewDificultad.frame
            posy = cell.lblTitulo.frame.minY - (cell.frame.size.width * 0.08) - 10
            frameVar.origin = CGPoint(x: cell.lblTitulo.frame.minX, y: posy)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.08, height: cell.frame.size.width * 0.08)
            cell.viewDificultad.frame = frameVar
            cell.viewDificultad.layer.cornerRadius = cell.viewDificultad.frame.size.width / 2
            cell.viewDificultad.clipsToBounds = true
            
            // label dificultad
            let strDif = self.xfunciones.getNivelDificultad(arr_ids: self.noticiasData[indexPath.row].categorias)
            if (strDif != ""){
                let elColor = self.xfunciones.getColorDificultad(tipo: strDif)
                cell.viewDificultad.backgroundColor = elColor
                cell.lblDificultad.text = strDif
                cell.lblDificultad.font = self.getFont(tipo: "lblDif")
                cell.lblDificultad.sizeToFit()
                cell.lblDificultad.center.x = cell.viewDificultad.frame.size.width / 2
                cell.lblDificultad.center.y = cell.viewDificultad.frame.size.height / 2
            } else {
                cell.viewDificultad.backgroundColor = UIColor.clear
                cell.lblDificultad.text = ""
            }
            
            //fin dificultad
            
            //img visto
            frameVar = cell.imgVisto.frame
            frameVar.origin = CGPoint(x: cell.viewDificultad.frame.maxX + 5, y: cell.viewDificultad.frame.minY)
            cell.imgVisto.frame = frameVar
            cell.imgVisto.center.y = cell.viewDificultad.frame.minY + cell.viewDificultad.frame.size.height / 2
            
            if (!Functions().existeIdEnVisto(id_nota: self.noticiasData[indexPath.row].id)){
                cell.imgVisto.isHidden = true
            } else {
                cell.imgVisto.isHidden = false
            }
            //fin img visto
            
            //img play
            frameVar = cell.btnPlay.frame
            frameVar.size = CGSize(width: cell.frame.size.width * 0.11, height: cell.frame.size.width * 0.11)
            cell.btnPlay.frame = frameVar
            let posxbtnplay = cell.frame.size.width - ( cell.btnPlay.frame.size.width + 10 )
            frameVar = cell.btnPlay.frame
            frameVar.origin = CGPoint(x: posxbtnplay, y: 50)
            cell.btnPlay.frame = frameVar
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.center.y = cell.frame.size.height / 2
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            cell.btnPlay.isUserInteractionEnabled = true
            cell.btnPlay.addGestureRecognizer(tap)
            //fin img play
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.celdaSeleccionada = indexPath
        let detalleNoticiaVC = self.storyboard!.instantiateViewController(withIdentifier: "detalleNotaVC") as! detalleNotaVC
        detalleNoticiaVC.dataNota = self.noticiasData[indexPath.row]
        detalleNoticiaVC.offsetAlto = self.offsetAlto + 5
        self.navigationController?.pushViewController(detalleNoticiaVC, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //print("scrollViewWillBeginDragging")
        self.isDataLoading = false
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndDecelerating")
    }
    //paginacion
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((self.tableNoticias.contentOffset.y + self.tableNoticias.frame.size.height) > self.tableNoticias.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo += 1
                self.primeraCarga = true
                self.getData()
                
            }
        }
        
        
    }

}
