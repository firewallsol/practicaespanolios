//
//  NoticiasVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 04/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import Kingfisher
import Jukebox
import PKHUD

class NoticiasVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var noticiasData = [Noticia]()
    var noticiasDataRespaldo = [Noticia]()
    var tableNoticias : UITableView!
    
    var dataLoaded = false
    
    var funciones = Functions()
    
    var offsetAlto : CGFloat = 0
    
    let notifFiltro = Notification.Name("aplicaFiltro")
    
    var isDataLoading = false
    var pageNo : Int = 1
    var catActual : String = "noticias"
    var nivelActual : String = ""
    var filtroVisto = false
    var cat_num = 21
    var nivelArray = [Int]()
    
    var primeraCarga = false
    
    var celdaSeleccionada : IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDataFiltro), name: self.notifFiltro, object: nil)
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.view.tag = 1
        
        self.buildInterface()
        self.getData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if celdaSeleccionada != nil {
            self.tableNoticias.reloadRows(at: [celdaSeleccionada], with: .automatic)
        }
    }
    
    func getData(){
        HUD.show(.labeledProgress(title: "", subtitle: "Cargando..."))
        let stringQuery = "?filter[category_name]=\(self.catActual)" + (self.nivelActual != "" ? "%2B\(self.nivelActual)" : "")
        let urlrequest = Constantes.WSData.urlRestBase + "\(stringQuery)&page=\(self.pageNo)"
        Alamofire.request("\(urlrequest)").responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonNoticias = JSON(value)
                if ((jsonNoticias.array?.count)! > 0){
                    self.noticiasData = self.noticiasDataRespaldo
                    if (self.pageNo == 1){
                        self.noticiasData = self.funciones.JSONtoArrayNoticia(elJson: jsonNoticias)
                    } else {
                        self.noticiasData.append(contentsOf: self.funciones.JSONtoArrayNoticia(elJson: jsonNoticias))
                    }
                    
                    self.noticiasDataRespaldo = self.noticiasData
                    
                    if (!self.dataLoaded){
                        var insets = self.tableNoticias.contentInset
                        insets.bottom += self.offsetAlto
                        self.tableNoticias.contentInset = insets
                    }
                    
                    self.aplicaFiltro()
                }
                
                HUD.hide()
                
            case .failure(let error):
                print(error)
                HUD.hide()
                let alerta = Functions().alertaSimple(titulo: "Error", mensaje: "Ha ocurrido un error al intentar conectarse al servidor", estilo: .alert, showButton: true)
                self.present(alerta, animated: true, completion: nil)
                
            }
        }
    }
    
    func aplicaFiltro(){
        self.noticiasData = self.noticiasDataRespaldo
        
        var newArrayCat = [Noticia]()
        
        //filtro categoria
        for item in self.noticiasData {
            if item.categorias.contains(self.cat_num) {
                newArrayCat.append(item)
            }
        }
        
        //filtro nivel
        var newArrayNivel = [Noticia]()
        if self.nivelArray.count > 0 {
            for cat in self.nivelArray {
                for item in newArrayCat {
                    if item.categorias.contains(cat){
                        newArrayNivel.append(item)
                    }
                }
            }
        } else {
            newArrayNivel = newArrayCat
        }
        
        
        //filtro visto
        var newArrayVisto = [Noticia]()
        if self.filtroVisto {
            for item in newArrayNivel {
                if funciones.existeIdEnVisto(id_nota: item.id){
                    newArrayVisto.append(item)
                }
                
            }
        } else {
            newArrayVisto = newArrayNivel
        }
        
        self.noticiasData = newArrayVisto
        self.dataLoaded = true
        self.tableNoticias.reloadData()
    }
    
    func getDataFiltro(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        //print("entra aqui: \(paramFiltro["categoria"])")
        //self.pageNo = 1
        self.catActual = paramFiltro["categoria"] as! String
        self.nivelActual = paramFiltro["nivel"] as! String
        self.filtroVisto = paramFiltro["visto"] as! Bool
        self.cat_num = paramFiltro["cat_num"] as! Int
        self.nivelArray.removeAll()
        self.nivelArray = paramFiltro["nivel_num"] as! [Int]
        self.aplicaFiltro()
    }
    
    func buildInterface(){
        
        self.tableNoticias = UITableView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        self.view.addSubview(self.tableNoticias)
        self.tableNoticias.dataSource = self
        self.tableNoticias.delegate = self
        self.tableNoticias.register(noticiasCell.self, forCellReuseIdentifier: "noticiasCell")
        self.tableNoticias.rowHeight = 150
        self.tableNoticias.separatorStyle = .none
    }
    
    
    func playItem(sender: UITapGestureRecognizer){
        let navController = self.navigationController as! navControllerPrincipal
        if (navController.toolBarPlayer.isHidden){
            navController.toolBarPlayer.isHidden = false
            navController.view.bringSubview(toFront: navController.toolBarPlayer)
        } else {
            if (navController.jukeBoxPlayer?.currentItem != nil){
                navController.jukeBoxPlayer?.remove(item: (navController.jukeBoxPlayer?.currentItem)!)
            }
            
        }
        
        if navController.jukeBoxPlayer?.state == .playing {
            navController.jukeBoxPlayer?.stop()
        }
        
        let index = sender.view?.tag
        
        if (self.noticiasData[index!].url_audio != ""){
            navController.tituloAudioActual = self.noticiasData[index!].titulo
            navController.jukeBoxPlayer?.append(item: JukeboxItem(URL: URL(string: self.noticiasData[index!].url_audio!)!), loadingAssets: true)
            navController.jukeBoxPlayer?.play()
            
        }
    }
 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataLoaded {
            return self.noticiasData.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableNoticias.dequeueReusableCell(withIdentifier: "noticiasCell")! as! noticiasCell
        
        cell.frame.size.width = self.anchoPantalla
        
        if self.dataLoaded {
            //img
            
            var frameVar = cell.imgFondo.frame
            frameVar.origin = CGPoint(x: 10, y: 10)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.98, height: cell.frame.size.height * 0.97)
            cell.imgFondo.frame = frameVar
            
            cell.viewOpacoImg.frame = cell.imgFondo.frame
            
            cell.imgFondo.center.x = cell.frame.size.width / 2
            cell.imgFondo.center.y = cell.frame.size.height / 2
            cell.viewOpacoImg.center = cell.imgFondo.center
            
            if (self.noticiasData[indexPath.row].url_imagen != ""){
                let imgurl = URL(string: self.noticiasData[indexPath.row].url_imagen!)
                cell.imgFondo.kf.setImage(with: imgurl, placeholder: UIImage(named: "placeHolderImagenGris"))
            } else {
                cell.imgFondo.image = UIImage(named: "placeHolderImagenGris")
            }
            //fin imagen
            
            //titulo
            cell.lblTitulo.text = self.noticiasData[indexPath.row].titulo
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: 10, y: 10)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.9, height: 30)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            var posy = cell.frame.size.height - (cell.lblTitulo.frame.size.height + 10)
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: 10, y: posy)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.9, height: frameVar.size.height)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            // fin titulo
            
            //seccion
            let strSeccion = self.funciones.getSeccionNoticia(arr_ids: self.noticiasData[indexPath.row].categorias)
            cell.lblSeccion.text = strSeccion
            cell.lblSeccion.sizeToFit()
            frameVar = cell.lblSeccion.frame
            posy = cell.lblTitulo.frame.minY - (cell.lblSeccion.frame.size.height + 5)
            frameVar.origin = CGPoint(x:10, y:posy)
            cell.lblSeccion.frame = frameVar
            //fin seccion
            
            //dificultad
            frameVar = cell.viewDificultad.frame
            //temp
            //let posYDif = cell.lblSeccion.frame.minY - (cell.frame.size.width * 0.08) - 10
            
            let posYDif : CGFloat = 5
                
            frameVar.origin = CGPoint(x: cell.lblSeccion.frame.minX, y: posYDif)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.08, height: cell.frame.size.width * 0.08)
            cell.viewDificultad.frame = frameVar
            cell.viewDificultad.layer.cornerRadius = cell.viewDificultad.frame.size.width / 2
            cell.viewDificultad.clipsToBounds = true
            //se comenta, probablemente seria centrado en X respecto a label seccion
            //cell.viewDificultad.center.y = cell.lblSeccion.frame.minY + cell.lblSeccion.frame.size.height / 2
            
            // label dificultad
            let strDif = self.funciones.getNivelDificultad(arr_ids: self.noticiasData[indexPath.row].categorias)
            let elColor = self.funciones.getColorDificultad(tipo: strDif)
            cell.viewDificultad.backgroundColor = elColor
            cell.lblDificultad.text = strDif
            cell.lblDificultad.sizeToFit()
            cell.lblDificultad.center.x = cell.viewDificultad.frame.size.width / 2
            cell.lblDificultad.center.y = cell.viewDificultad.frame.size.height / 2
            
            //fin dificultad
            
            //img visto
            frameVar = cell.imgVisto.frame
            frameVar.origin = CGPoint(x: cell.viewDificultad.frame.maxX + 10, y: cell.viewDificultad.frame.minY)
            cell.imgVisto.frame = frameVar
            cell.imgVisto.center.y = cell.viewDificultad.frame.minY + cell.viewDificultad.frame.size.height / 2
            if (!funciones.existeIdEnVisto(id_nota: self.noticiasData[indexPath.row].id)){
                
                cell.imgVisto.isHidden = true
            } else {
                cell.imgVisto.isHidden = false
            }
            //fin img visto
            
            //img play
            frameVar = cell.btnPlay.frame
            frameVar.size = CGSize(width: cell.frame.size.width * 0.11, height: cell.frame.size.width * 0.11)
            cell.btnPlay.frame = frameVar
            let posxbtnplay = cell.frame.size.width - ( cell.btnPlay.frame.size.width + 10 )
            frameVar = cell.btnPlay.frame
            frameVar.origin = CGPoint(x: posxbtnplay, y: 50)
            cell.btnPlay.frame = frameVar
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.center.y = cell.frame.size.height / 2
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            cell.btnPlay.isUserInteractionEnabled = true
            cell.btnPlay.addGestureRecognizer(tap)
            //fin img play
            
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detalleNoticiaVC = self.storyboard!.instantiateViewController(withIdentifier: "detalleNotaVC") as! detalleNotaVC
        detalleNoticiaVC.dataNota = self.noticiasData[indexPath.row]
        detalleNoticiaVC.offsetAlto = self.offsetAlto / 2
        self.navigationController?.pushViewController(detalleNoticiaVC, animated: true)
        self.celdaSeleccionada = indexPath
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //print("scrollViewWillBeginDragging")
        self.isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndDecelerating")
    }
    //paginacion
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //print("scrollViewDidEndDragging")
        if ((self.tableNoticias.contentOffset.y + self.tableNoticias.frame.size.height) > self.tableNoticias.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo += 1
                self.getData()
                
            }
        }
        
        
    }

}
