//
//  detalleNotaVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 12/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import youtube_ios_player_helper
import Jukebox
import PKHUD
import Firebase

class detalleNotaVC: UIViewController {
    var tituloNota : UILabel!
    var autorFecha : UILabel!
    var imgNota : UIImageView!
    var btnPlay : UIImageView!
    var contenidoNota : UITextView!
    
    var dataNota : Noticia!
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    var altoNavBar : CGFloat = 0
    var altoStatusBar : CGFloat = 0
    var offsetAlto : CGFloat = 0
    
    let xfunciones = Functions()
    
    var scrollView : UIScrollView!
    
    var youtubePlayer : YTPlayerView?
    
    var tituloView = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)!
        self.altoStatusBar = self.xfunciones.statusBarHeight()
        
        self.buildInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = self.tituloView
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        
        switch tipo {
        case "titulo":
            font = UIFont.boldSystemFont(ofSize: 20)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightBold)
            }
            break
        case "boton":
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
            }
            break
        case "fecha":
            font = UIFont.systemFont(ofSize: 14)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            }
            break
        case "cuerpo":
            font = UIFont.systemFont(ofSize: 16)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightMedium)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightLight)
            }
            break
        }
        
        return font
    }
    
    func marcarDesmarcarVisto(){
        let resultado = xfunciones.agregarQuitarVisto(id_nota: self.dataNota.id)
        let alerta = xfunciones.alertaSimple(titulo: "", mensaje: resultado, estilo: .actionSheet, showButton: false)
        self.present(alerta, animated: true, completion: nil)
        xfunciones.dismissAlertDelay(alerta: alerta)
        
    }
    
    func marcarDesmarcarFavorito(){
        
        if let _ = FIRAuth.auth()?.currentUser {
            let resultado = xfunciones.agregarQuitarFavorito(id_nota: self.dataNota.id, xjson: self.dataNota.jsonStr)
            let alerta = xfunciones.alertaSimple(titulo: "", mensaje: resultado, estilo: .actionSheet, showButton: false)
            self.present(alerta, animated: true, completion: nil)
            xfunciones.dismissAlertDelay(alerta: alerta)
        } else { //sino indicalo con una amable alerta
            let index = Functions().getIdiomaIndex()
            let msg = idiomasClass().arrayIdiomas[index].msg_inicio_sesion_favoritos
            HUD.flash(.label(msg), delay: 2.0, completion: nil)
        }
    }
    
    func buildInterface(){
        //scroll view
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        
        //nav bar
        //compartir
        let shareButtonImg = UIImage(named: "button_share_white")
        let shareButton = UIButton.init(type: .custom)
        shareButton.setImage(shareButtonImg, for: .normal)
        shareButton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        shareButton.addTarget(self, action: #selector(self.shareContent(sender:)), for: UIControlEvents.touchUpInside)
        let shareNavButton = UIBarButtonItem.init(customView: shareButton)
        
        //favorito
        let favoritoImg   = UIImage(named: "Estrella_blanca")!
        let favoritoButton   = UIButton.init(type: .custom)
        favoritoButton.setImage(favoritoImg, for: .normal)
        favoritoButton.addTarget(self, action:#selector(self.marcarDesmarcarFavorito), for: UIControlEvents.touchUpInside)
        favoritoButton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        let favBarButton = UIBarButtonItem.init(customView: favoritoButton)
        
        //visto
        let vistoImg   = UIImage(named: "eyeblanco")!
        let vistoButton   = UIButton.init(type: .custom)
        vistoButton.setImage(vistoImg, for: .normal)
        vistoButton.addTarget(self, action:#selector(self.marcarDesmarcarVisto), for: UIControlEvents.touchUpInside)
        vistoButton.frame = CGRect(x: 0, y: 0, width: 27, height: 27)
        let vistoBarButton = UIBarButtonItem.init(customView: vistoButton)
        
        self.navigationItem.rightBarButtonItems = [vistoBarButton, favBarButton, shareNavButton]
        
        //fin navbar
        
        //titulo de view
        let strSeccion = self.xfunciones.getSeccionNoticia(arr_ids: self.dataNota.categorias)
        let strDificultad = self.xfunciones.getNivelDificultad(arr_ids: self.dataNota.categorias)
        if (strSeccion != ""){
            self.title = strSeccion + " / " + strDificultad
            self.tituloView = strSeccion + " / " + strDificultad
        } else {
            self.title = "La voz de la noticia / " + strDificultad
            self.tituloView = "La voz de la noticia / " + strDificultad
        }
        
        //titulo nota
        self.tituloNota = UILabel(frame: CGRect(x: 10, y: 5, width: self.anchoPantalla * 0.95, height: 15))
        self.tituloNota.text = self.dataNota.titulo
        self.tituloNota.font = self.getFont(tipo: "titulo")
        self.tituloNota.numberOfLines = 0
        self.tituloNota.sizeToFit()
        self.scrollView.addSubview(self.tituloNota)
        
        //aca antes autor y fecha
        self.autorFecha = UILabel(frame: CGRect(x: 10, y: self.tituloNota.frame.maxY + 5, width: self.anchoPantalla * 0.98, height: 15))
        self.autorFecha.numberOfLines = 1
        self.autorFecha.text = self.dataNota.fecha
        self.autorFecha.font = self.getFont(tipo: "fecha")
        self.autorFecha.sizeToFit()
        self.scrollView.addSubview(self.autorFecha)
        
        //imagen
        self.imgNota = UIImageView(frame: CGRect(x: 0, y: self.autorFecha.frame.maxY + 10, width: self.anchoPantalla * 0.95, height: self.anchoPantalla * 0.5))
        self.imgNota.contentMode = .scaleAspectFill
        
        if (self.dataNota.url_imagen != ""){
            let imgurl = URL(string: self.dataNota.url_imagen!)
            self.imgNota.kf.setImage(with: imgurl)
            self.imgNota.kf.indicatorType = .activity
            self.imgNota.center.x = self.anchoPantalla / 2
        } else {
            self.imgNota.image = UIImage(named: "placeHolderImagenGris")
        }
        self.scrollView.addSubview(self.imgNota)
        
        //contenido de la nota
        self.contenidoNota = UITextView(frame: CGRect(x: 10, y: self.imgNota.frame.maxY + 10, width: self.anchoPantalla * 0.9, height: 20))
        
        
        let htmlContenido = self.xfunciones.stringFromHtml(string: self.dataNota.contenido, tipo: "texto")
        
        self.contenidoNota.attributedText = htmlContenido
 
        //temp
        //self.contenidoNota.font = self.getFont(tipo: "cuerpo")
        //self.contenidoNota.text = self.dataNota.contenido
        self.contenidoNota.textAlignment = NSTextAlignment.justified
        self.contenidoNota.isScrollEnabled = false
        self.contenidoNota.isEditable = false
        self.contenidoNota.sizeToFit()
        self.scrollView.addSubview(self.contenidoNota)
        self.contenidoNota.center.x = self.anchoPantalla / 2
        
        //si tiene audio se pone el boton de play, sino se oculta
        let posxbtnplay = self.imgNota.frame.maxX - (self.imgNota.frame.size.height * 0.3 + 5)
        let posybtnplay = self.imgNota.frame.maxY - (self.imgNota.frame.size.height * 0.3 + 5)
        
        if (self.dataNota.url_audio != ""){
            self.btnPlay = UIImageView(frame: CGRect(x: posxbtnplay, y: posybtnplay, width: self.imgNota.frame.size.height * 0.3, height: self.imgNota.frame.size.height * 0.3))
            self.btnPlay.contentMode = .scaleAspectFit
            self.btnPlay.image = UIImage(named: "Circle blanco play")
            let tapPlay = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            self.btnPlay.isUserInteractionEnabled = true
            self.btnPlay.addGestureRecognizer(tapPlay)
            self.scrollView.addSubview(self.btnPlay)
        }
        var altoScroll = self.contenidoNota.frame.maxY + 10
        
        var tieneVideo = false
        
        //agregar video de youtube si tiene
        if (self.dataNota.id_video != ""){
            self.youtubePlayer = YTPlayerView(frame: CGRect(x: 0, y: self.contenidoNota.frame.maxY + 1, width: self.anchoPantalla, height: self.anchoPantalla * 0.7))
            self.youtubePlayer?.load(withVideoId: self.dataNota.id_video!)
            self.scrollView.addSubview(self.youtubePlayer!)
            altoScroll += (self.youtubePlayer?.frame.maxY)!
            tieneVideo = true
        }
        
        //agregar botones de ejerciios si tiene
        if (self.dataNota.ejercicios != JSON.null){
            let numEjercicios = self.dataNota.ejercicios?["number_of_exercises"].int
            var posYinicialBtn : CGFloat = 0
            if (tieneVideo){
                posYinicialBtn = (self.youtubePlayer?.frame.maxY)!
            } else {
                posYinicialBtn = self.contenidoNota.frame.maxY
            }
            for index in 1...numEjercicios! {
                posYinicialBtn += 12
                let tituloEjercicio = self.dataNota.ejercicios?["exercise\(index)"]["title"].string
                let anchoBoton = self.anchoPantalla * 0.9
                let anchoLabel = anchoBoton * 0.9
                let labelView = UILabel(frame: CGRect(x: 0, y: 5, width: anchoLabel, height: 10))
                labelView.numberOfLines = 2
                labelView.font = self.getFont(tipo: "boton")
                labelView.text = tituloEjercicio
                labelView.textColor = UIColor.white
                labelView.adjustsFontSizeToFitWidth = true
                labelView.sizeToFit()
                
                let viewBtn = UIView(frame: CGRect(x: 0, y: posYinicialBtn, width: anchoBoton, height: labelView.frame.size.height + 20))
                viewBtn.backgroundColor = UIColor(red: 0, green: 103/255, blue: 174/255, alpha: 1)
                viewBtn.layer.borderColor = UIColor(red: 0, green: 81/255, blue: 163/255, alpha: 1).cgColor
                viewBtn.layer.borderWidth = 0.5
                viewBtn.layer.cornerRadius = 3
                viewBtn.tag = index
                viewBtn.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.gotoEjercicio(sender:)))
                viewBtn.addGestureRecognizer(tap)
                
                viewBtn.addSubview(labelView)
                self.scrollView.addSubview(viewBtn)
                viewBtn.center.x = self.anchoPantalla / 2
                labelView.center.x = viewBtn.frame.size.width / 2
                labelView.center.y = viewBtn.frame.size.height / 2
                
                posYinicialBtn = viewBtn.frame.maxY
                
                altoScroll = viewBtn.frame.maxY
            }
        }
        
        self.view.addSubview(self.scrollView)
        
        self.scrollView.contentSize.height = altoScroll + self.altoPantalla * 0.25
        
    }
    
    func gotoEjercicio(sender: UITapGestureRecognizer){
        
        if let _ = FIRAuth.auth()?.currentUser {
            let tag = sender.view?.tag
            let vcEjercicio = self.storyboard!.instantiateViewController(withIdentifier: "ejercicioVC") as! ejercicioVC
            vcEjercicio.arrayPreguntas = self.dataNota.ejercicios?["exercise\(tag!)"]
            vcEjercicio.offsetAlto = self.offsetAlto
            self.navigationController?.pushViewController(vcEjercicio, animated: true)
        } else { //sino indicalo con una amable alerta
            let index = Functions().getIdiomaIndex()
            let msg = idiomasClass().arrayIdiomas[index].msg_inicio_sesion_ejercicios
            HUD.flash(.label(msg), delay: 2.0, completion: nil)
        }
    }
    
    func playItem(sender: UITapGestureRecognizer){
        let navController = self.navigationController as! navControllerPrincipal
        if (navController.toolBarPlayer.isHidden){
            navController.toolBarPlayer.isHidden = false
            navController.view.bringSubview(toFront: navController.toolBarPlayer)
        } else {
            if (navController.jukeBoxPlayer?.currentItem != nil){
                navController.jukeBoxPlayer?.remove(item: (navController.jukeBoxPlayer?.currentItem)!)
            }
            
        }
        
        if navController.jukeBoxPlayer?.state == .playing {
            navController.jukeBoxPlayer?.stop()
        }
        
        if (self.dataNota.url_audio != ""){
            navController.tituloAudioActual = self.tituloNota.text!
            navController.jukeBoxPlayer?.append(item: JukeboxItem(URL: URL(string: self.dataNota.url_audio!)!), loadingAssets: true)
            navController.jukeBoxPlayer?.play()
        }
    }
    
    
    func shareContent(sender:UIView){
        
        let textToShare = self.dataNota.titulo
        var image : UIImage? = nil
        if (self.dataNota.url_imagen != ""){
            image = self.imgNota.image!
        }
        
        
        if let shareLink = URL(string: "\(self.dataNota.link!)" ) {
            let objectsToShare = [textToShare!, shareLink, image ?? #imageLiteral(resourceName: "app-logo")] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            //Excluded Activities
            activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
            //
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
}
