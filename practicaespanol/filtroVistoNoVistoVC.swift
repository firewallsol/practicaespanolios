//
//  filtroVistoNoVistoVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 17/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class filtroVistoNoVistoVC: UIViewController {
    
    var vistoSwitch : UISwitch!
    var scrollView : UIScrollView!
    
    var altoPantalla : CGFloat = 0
    var anchoPantalla : CGFloat = 0
    
    let notifFiltroVozN = Notification.Name("aplicaFiltroVozNoticia")
    let notifFiltroBoletin = Notification.Name("aplicaFiltroBoletin")
    
    var vistoOnOff = false
    
    var sender = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.setNavBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBar(){
        let aplicarButton = UIBarButtonItem(title: "Aplicar", style: .done, target: self, action: #selector(self.aplicaFiltro(sender:)))
        self.navigationItem.rightBarButtonItem = aplicarButton
    }
    
    func aplicaFiltro(sender: UIBarButtonItem){
        let visto = self.vistoSwitch.isOn
        let paramFiltro = ["visto": visto] as NSDictionary
        if self.sender == "vozNoticia" {
            NotificationCenter.default.post(name: self.notifFiltroVozN, object: paramFiltro)
        } else {
            NotificationCenter.default.post(name: self.notifFiltroBoletin, object: paramFiltro)
        }
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func buildInterface(){
        self.title = "FILTRO"
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //switch
        var xframe = CGRect()
        xframe.origin = CGPoint(x: 20, y: 20)
        xframe.size = CGSize(width: 100, height: 10)
        let lblVisto = UILabel(frame: xframe)
        let index = Functions().getIdiomaIndex()
        
        lblVisto.text = idiomasClass().arrayIdiomas[index].filtro_visto
        
        var font = UIFont.boldSystemFont(ofSize: 17)
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)
        }
        lblVisto.font = font
        lblVisto.sizeToFit()
        self.scrollView.addSubview(lblVisto)
        
        let posy = lblVisto.frame.maxY
        
        xframe.origin = CGPoint(x: lblVisto.frame.maxX + 10, y: posy)
        xframe.size = CGSize(width: self.anchoPantalla * 0.1, height: self.altoPantalla * 0.5)
        self.vistoSwitch = UISwitch(frame: xframe)
        
        self.scrollView.addSubview(self.vistoSwitch)
        self.vistoSwitch.center.y = lblVisto.frame.minY + lblVisto.frame.size.height / 2
        
        self.scrollView.contentSize.height = posy + (self.altoPantalla * 0.25)
        
        self.view.addSubview(self.scrollView)
        
        self.vistoSwitch.isOn = self.vistoOnOff
    }
    
    

}
