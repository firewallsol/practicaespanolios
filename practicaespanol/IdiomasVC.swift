//
//  IdiomasVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 22/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SideMenu

class IdiomasVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var tableIdiomas : UITableView!
    
    var indexIdiomaAnterior : IndexPath!
    
    var arrayIdiomas = ["Español", "English", "Português", "Français", "Deutsch", "中文"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.buildInterface()
        
        self.setNavBar()
    }
    
    func buildInterface(){
        self.view.backgroundColor = UIColor.white
        let index = Functions().getIdiomaIndex()
        self.title = idiomasClass().arrayIdiomas[index].menu_idiomas.uppercased()
        self.tableIdiomas = UITableView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        self.view.addSubview(self.tableIdiomas)
        
        self.tableIdiomas.dataSource = self
        self.tableIdiomas.delegate = self
        self.tableIdiomas.register(IdiomasCell.self, forCellReuseIdentifier: "idiomasCell")
        self.tableIdiomas.separatorStyle = .none
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayIdiomas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Functions().setIndexIdioma(elIndex: indexPath.row)
        self.tableIdiomas.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableIdiomas.dequeueReusableCell(withIdentifier: "idiomasCell")! as! IdiomasCell
        
        cell.frame.size.width = self.anchoPantalla
        var xframe = CGRect()
        xframe.origin = CGPoint(x: 10, y: 0)
        xframe.size = CGSize(width: cell.frame.size.width * 0.7, height: cell.frame.size.height)
        cell.lblIdioma.frame = xframe
        cell.lblIdioma.text = self.arrayIdiomas[indexPath.row]
        cell.lblIdioma.sizeToFit()
        cell.lblIdioma.frame.origin.y = cell.frame.size.height / 2
        
        xframe.origin = CGPoint(x: 10, y: 0)
        let anchoAltoImg = cell.frame.size.height * 0.5
        xframe.size = CGSize(width: anchoAltoImg, height: anchoAltoImg)
        cell.imgCheck.frame = xframe
        cell.imgCheck.frame.origin.y = cell.frame.size.height / 2
        cell.imgCheck.frame.origin.x = cell.frame.size.width - (anchoAltoImg + 5 )
        cell.imgCheck.isHidden = true
        if (indexPath.row == Functions().getIdiomaIndex()){
            cell.imgCheck.isHidden = false
            self.indexIdiomaAnterior = indexPath
        }
        
        return cell
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
