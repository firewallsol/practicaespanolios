//
//  noConfundasDetalleVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 19/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SwiftyJSON
import Jukebox

class noConfundasDetalleVC: UIViewController {
    var scrollView : UIScrollView!
    var lblPalabra1 : UILabel!
    var lblPalabra2 : UILabel!
    var ejemplo1_1 : UILabel!
    var ejemplo1_2 : UILabel!
    var ejemplo2_1 : UILabel!
    var ejemplo2_2 : UILabel!
    var play1 : UIImageView!
    var play2 : UIImageView!
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var palabrasData : NoConfundas!
    
    let xfunciones = Functions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.view.backgroundColor = UIColor.white
        
        self.setNavBar()
        
        self.buildInterface()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        
        switch tipo {
        case "titulo":
            font = UIFont.boldSystemFont(ofSize: 20)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightBold)
            }
            break
        case "ejemplo":
            font = UIFont.systemFont(ofSize: 16)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightRegular)
            }
            break
        case "lblEjemplo":
            font = UIFont.boldSystemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightLight)
            }
            break
        }
        
        return font
    }
    
    func setNavBar(){
        //visto
        let imgVisto = UIImage(named: "eyeblanco")
        
        let vistoButton = UIBarButtonItem()
        vistoButton.style = UIBarButtonItemStyle.plain
        let ojoButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        ojoButton.setImage(imgVisto, for: .normal)
        ojoButton.addTarget(self, action:#selector(self.marcarDesmarcarVisto), for: UIControlEvents.touchUpInside)
        vistoButton.customView = ojoButton
        self.navigationItem.rightBarButtonItem = vistoButton
    }
    
    func marcarDesmarcarVisto(){
        let resultado = xfunciones.agregarQuitarVisto(id_nota: self.palabrasData.id)
        let alerta = xfunciones.alertaSimple(titulo: "", mensaje: resultado, estilo: .actionSheet, showButton: false)
        self.present(alerta, animated: true, completion: nil)
        xfunciones.dismissAlertDelay(alerta: alerta)
    }
    
    func buildInterface(){
        let xpadding : CGFloat = 15
        
        
        self.title = self.palabrasData.palabra1 + " - " + self.palabrasData.palabra2
        
        let anchoAltoBtnPlay = self.anchoPantalla * 0.13
        
        //scroll view
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        
        var posy : CGFloat = 10
        
        //palabra 1
        var xframe = CGRect(x: xpadding, y: 10, width: self.anchoPantalla * 0.8, height: 10)
        self.lblPalabra1 = UILabel(frame: xframe)
        self.lblPalabra1.text = self.palabrasData.palabra1
        self.lblPalabra1.font = self.getFont(tipo: "titulo")
        self.lblPalabra1.sizeToFit()
        
        self.scrollView.addSubview(self.lblPalabra1)
        
        posy = self.lblPalabra1.frame.maxY
        
        //ejemplos palabra 1
        xframe = CGRect(x: xpadding, y: posy + 10, width: 10, height: 10)
        let lblEjemplo1 = UILabel(frame: xframe)
        lblEjemplo1.text = "Ejemplos:"
        lblEjemplo1.font = self.getFont(tipo: "lblEjemplo")
        lblEjemplo1.textColor = UIColor(red: 178/255, green: 29/255, blue: 33/255, alpha: 1)
        lblEjemplo1.sizeToFit()
        
        posy = lblEjemplo1.frame.maxY
        
        self.scrollView.addSubview(lblEjemplo1)
        
        xframe = CGRect(x: xpadding, y: posy + 10, width: 10, height: 10)
        self.ejemplo1_1 = UILabel(frame: xframe)
        self.ejemplo1_1.text = self.palabrasData.ej1_palabra1
        self.ejemplo1_1.font = self.getFont(tipo: "ejemplo")
        self.ejemplo1_1.textColor = UIColor.gray
        self.ejemplo1_1.sizeToFit()
        
        posy = self.ejemplo1_1.frame.maxY
        
        self.scrollView.addSubview(self.ejemplo1_1)
        
        xframe = CGRect(x: xpadding, y: posy + 10, width: 10, height: 10)
        self.ejemplo1_2 = UILabel(frame: xframe)
        self.ejemplo1_2.text = self.palabrasData.ej2_palabra1
        self.ejemplo1_2.font = self.getFont(tipo: "ejemplo")
        self.ejemplo1_2.textColor = UIColor.gray
        self.ejemplo1_2.sizeToFit()
        
        posy = self.ejemplo1_2.frame.maxY
        
        self.scrollView.addSubview(self.ejemplo1_2)
        
        //si tiene audio palabra 1
        if let _ = self.palabrasData.url_audio_palabra1 {
            xframe = CGRect(x: xpadding, y: posy + 15, width: anchoAltoBtnPlay, height: anchoAltoBtnPlay)
            self.play1 = UIImageView(frame: xframe)
            self.play1.contentMode = .scaleAspectFit
            self.play1.image = UIImage(named: "play_negro")
            self.scrollView.addSubview(self.play1)
            self.play1.center.x = self.anchoPantalla / 2
            self.play1.tag = 1
            let tapPlay1 = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            self.play1.isUserInteractionEnabled = true
            self.play1.addGestureRecognizer(tapPlay1)
            posy = self.play1.frame.maxY
            
        }
        
        //palabra 2
        xframe = CGRect(x: xpadding, y: posy + 10, width: self.anchoPantalla * 0.8, height: 10)
        self.lblPalabra2 = UILabel(frame: xframe)
        self.lblPalabra2.text = self.palabrasData.palabra2
        self.lblPalabra2.font = self.getFont(tipo: "titulo")
        self.lblPalabra2.sizeToFit()
        
        self.scrollView.addSubview(self.lblPalabra2)
        
        posy = self.lblPalabra2.frame.maxY
        
        //ejemplos palabra 2
        xframe = CGRect(x: xpadding, y: posy + 10, width: 10, height: 10)
        let lblEjemplo2 = UILabel(frame: xframe)
        lblEjemplo2.text = "Ejemplos:"
        lblEjemplo2.font = self.getFont(tipo: "lblEjemplo")
        lblEjemplo2.textColor = UIColor(red: 178/255, green: 29/255, blue: 33/255, alpha: 1)
        lblEjemplo2.sizeToFit()
        
        posy = lblEjemplo2.frame.maxY
        
        self.scrollView.addSubview(lblEjemplo2)
        
        xframe = CGRect(x: xpadding, y: posy + 10, width: 10, height: 10)
        self.ejemplo1_2 = UILabel(frame: xframe)
        self.ejemplo1_2.text = self.palabrasData.ej1_palabra2
        self.ejemplo1_2.font = self.getFont(tipo: "ejemplo")
        self.ejemplo1_2.textColor = UIColor.gray
        self.ejemplo1_2.sizeToFit()
        
        posy = self.ejemplo1_2.frame.maxY
        
        self.scrollView.addSubview(self.ejemplo1_2)
        
        xframe = CGRect(x: xpadding, y: posy + 10, width: 10, height: 10)
        self.ejemplo2_2 = UILabel(frame: xframe)
        self.ejemplo2_2.text = self.palabrasData.ej2_palabra2
        self.ejemplo2_2.font = self.getFont(tipo: "ejemplo")
        self.ejemplo2_2.textColor = UIColor.gray
        self.ejemplo2_2.sizeToFit()
        
        posy = self.ejemplo2_2.frame.maxY
        
        self.scrollView.addSubview(self.ejemplo2_2)
        
        //si tiene audio palabra 2
        if let _ = self.palabrasData.url_audio_palabra2 {
            xframe = CGRect(x: xpadding, y: posy + 15, width: anchoAltoBtnPlay, height: anchoAltoBtnPlay)
            self.play2 = UIImageView(frame: xframe)
            self.play2.contentMode = .scaleAspectFit
            self.play2.image = UIImage(named: "play_negro")
            self.scrollView.addSubview(self.play2)
            self.play2.center.x = self.anchoPantalla / 2
            self.play2.tag = 2
            let tapPlay2 = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            self.play2.isUserInteractionEnabled = true
            self.play2.addGestureRecognizer(tapPlay2)
            posy = self.play2.frame.maxY
            
        }
        
        self.view.addSubview(self.scrollView)
        
        self.scrollView.contentSize.height = posy + self.altoPantalla * 0.25
    }
    
    func playItem(sender: UITapGestureRecognizer){
        let navController = self.navigationController as! navControllerPrincipal
        if (navController.toolBarPlayer.isHidden){
            navController.toolBarPlayer.isHidden = false
            navController.view.bringSubview(toFront: navController.toolBarPlayer)
        } else {
            if (navController.jukeBoxPlayer?.currentItem != nil){
                navController.jukeBoxPlayer?.remove(item: (navController.jukeBoxPlayer?.currentItem)!)
            }
            
        }
        
        if navController.jukeBoxPlayer?.state == .playing {
            navController.jukeBoxPlayer?.stop()
        }
        
        let index = sender.view?.tag
        var strAudio = ""
        if (index == 1){
            strAudio = self.palabrasData.url_audio_palabra1!
        } else if index == 2 {
            strAudio = self.palabrasData.url_audio_palabra2!
        }
        
        if (strAudio != ""){
            navController.tituloAudioActual = self.title!
            navController.jukeBoxPlayer?.append(item: JukeboxItem(URL: URL(string: strAudio)!), loadingAssets: true)
            navController.jukeBoxPlayer?.play()
        }
        
    }

} //fin de clase
