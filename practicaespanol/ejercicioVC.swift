//
//  ejercicioVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 13/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SwiftyJSON
import DLRadioButton


class ejercicioVC: UIViewController {
    
    var arrayPreguntas : JSON!
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var objsPreguntas = [Pregunta]()
    
    var botonResultados : UIButton!
    
    var numPreguntas = 0
    
    var aciertos = 0
    
    var scrollView : UIScrollView!
    
    var offsetAlto : CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.arrayPreguntas["title"].string!
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.buildInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getFont(tipo:String)->UIFont{
        var font = UIFont()
        
        switch tipo {
        case "pregunta":
            font = UIFont.boldSystemFont(ofSize: 16)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 16, weight: UIFontWeightBold)
            }
            break
        case "respuesta":
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
            }
            break
        case "titulo":
            font = UIFont.boldSystemFont(ofSize: 17)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)
            }
            break
        default:
            font = UIFont.systemFont(ofSize: 15)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 15, weight: UIFontWeightRegular)
            }
            break
        }
        
        return font
    }
    
    func buildInterface(){
        self.scrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        
        self.objsPreguntas.removeAll()
        
        self.numPreguntas = (self.arrayPreguntas.count - 1) / 5 // -1 por el titulo, 5 elementos por pregunta
        
        var posY : CGFloat = 10
        for index in 1...self.numPreguntas {
            let nuevaPregunta = Pregunta()
            var respuestaGenerica = Respuesta()
            var arrayRespuestas = [Respuesta]()
            
            //label num pregunta
            let labelNumPreg = UILabel(frame: CGRect(x: 10, y: posY, width: 20, height: 10))
            labelNumPreg.text = "Pregunta: \(index)"
            labelNumPreg.font = self.getFont(tipo: "titulo")
            labelNumPreg.numberOfLines = 1
            labelNumPreg.sizeToFit()
            
            self.scrollView.addSubview(labelNumPreg)
            nuevaPregunta.label_num_pregunta = labelNumPreg
            
            posY = labelNumPreg.frame.maxY
            
            //la pregunta
            let labelPregunta = UILabel(frame: CGRect(x: 10, y: posY + 10, width: self.anchoPantalla * 0.9, height: 10))
            labelPregunta.numberOfLines = 0
            labelPregunta.font = self.getFont(tipo: "pregunta")
            labelPregunta.text = self.arrayPreguntas["question\(index)"].string ?? "NULL"
            
            labelPregunta.sizeToFit()
            self.scrollView.addSubview(labelPregunta)
            
            //var colorNameToUse = userDefinedColorName ?? defaultColorName
            let textCorrecta = self.arrayPreguntas["correctAnswer\(index)"].string ?? "NULL"
            
            nuevaPregunta.texto_pregunta = self.arrayPreguntas["question\(index)"].string ?? "NULL"
            
            posY = labelPregunta.frame.maxY
            
            let widthHeightRadio : CGFloat = 35
            
            //las respuestas
            var frame = CGRect(x: 20, y: posY + 15, width: widthHeightRadio, height: widthHeightRadio)
            let firstRB = createRadioButton(frame, title: "", color: UIColor.blue)
            firstRB.tag = (index * 10) + 1
            
            //label
            let anchoLabel = self.anchoPantalla - 70
            
            let firstLbl = UILabel(frame: CGRect(x: firstRB.frame.maxX, y: posY + 15, width: anchoLabel, height: 10))
            firstLbl.text = self.arrayPreguntas["answer\(index)_1"].string ?? "NULL"
            firstLbl.font = self.getFont(tipo: "respuesta")
            firstLbl.numberOfLines = 0
            firstLbl.sizeToFit()
            firstLbl.isUserInteractionEnabled = true
            firstLbl.tag = (index * 10) + 1
            let tapFL = UITapGestureRecognizer(target: self, action: #selector(self.selectRadioButton(sender:)))
            firstLbl.addGestureRecognizer(tapFL)
            
            respuestaGenerica.texto_respuesta = firstLbl.text
            if (respuestaGenerica.texto_respuesta == textCorrecta){
                respuestaGenerica.es_correcta = true
                nuevaPregunta.tag_correcto = firstRB.tag
            }
            respuestaGenerica.radio_button = firstRB
            
            arrayRespuestas.append(respuestaGenerica)
            
            posY = firstLbl.frame.maxY
            
            firstRB.center.y = firstLbl.frame.minY + firstLbl.frame.size.height / 2
            
            self.scrollView.addSubview(firstRB)
            self.scrollView.addSubview(firstLbl)
            
            var otherButtons : [DLRadioButton] = []
            
            frame = CGRect(x: 20, y: posY + 15, width: widthHeightRadio, height: widthHeightRadio)
            let secondRB = createRadioButton(frame, title: "", color: UIColor.blue)
            secondRB.tag = (index * 10) + 2
            
            let secondLbl = UILabel(frame: CGRect(x: secondRB.frame.maxX, y: posY + 15, width: anchoLabel, height: 10))
            secondLbl.text = self.arrayPreguntas["answer\(index)_2"].string ?? "NULL"
            secondLbl.font = self.getFont(tipo: "respuesta")
            secondLbl.numberOfLines = 0
            secondLbl.sizeToFit()
            secondLbl.isUserInteractionEnabled = true
            secondLbl.tag = (index * 10) + 2
            let tapSL = UITapGestureRecognizer(target: self, action: #selector(self.selectRadioButton(sender:)))
            secondLbl.addGestureRecognizer(tapSL)
            
            respuestaGenerica = Respuesta()
            respuestaGenerica.texto_respuesta = secondLbl.text
            if (respuestaGenerica.texto_respuesta == textCorrecta){
                respuestaGenerica.es_correcta = true
                nuevaPregunta.tag_correcto = secondRB.tag
            }
            respuestaGenerica.radio_button = secondRB
            
            arrayRespuestas.append(respuestaGenerica)
            
            posY = secondLbl.frame.maxY
            
            secondRB.center.y = secondLbl.frame.minY + secondLbl.frame.size.height / 2
            
            self.scrollView.addSubview(secondRB)
            otherButtons.append(secondRB)
            self.scrollView.addSubview(secondLbl)
            
            frame = CGRect(x: 20, y: posY + 15, width: widthHeightRadio, height: widthHeightRadio)
            let thirdRB = createRadioButton(frame, title: "", color: UIColor.blue)
            thirdRB.tag = (index * 10) + 3
            
            let thirdLbl = UILabel(frame: CGRect(x: thirdRB.frame.maxX, y: posY + 15, width: anchoLabel, height: 10))
            thirdLbl.text = self.arrayPreguntas["answer\(index)_3"].string ?? "NULL"
            thirdLbl.font = self.getFont(tipo: "respuesta")
            thirdLbl.numberOfLines = 0
            thirdLbl.sizeToFit()
            thirdLbl.isUserInteractionEnabled = true
            thirdLbl.tag = (index * 10) + 3
            let tapTL = UITapGestureRecognizer(target: self, action: #selector(self.selectRadioButton(sender:)))
            thirdLbl.addGestureRecognizer(tapTL)
            
            respuestaGenerica = Respuesta()
            respuestaGenerica.texto_respuesta = thirdLbl.text
            if (respuestaGenerica.texto_respuesta == textCorrecta){
                respuestaGenerica.es_correcta = true
                nuevaPregunta.tag_correcto = thirdRB.tag
            }
            respuestaGenerica.radio_button = thirdRB
            
            arrayRespuestas.append(respuestaGenerica)
            
            posY = thirdLbl.frame.maxY + 20
            
            thirdRB.center.y = thirdLbl.frame.minY + thirdLbl.frame.size.height / 2
            
            self.scrollView.addSubview(thirdRB)
            otherButtons.append(thirdRB)
            self.scrollView.addSubview(thirdLbl)
            
            firstRB.otherButtons = otherButtons
            
            nuevaPregunta.respuestas = arrayRespuestas
            
            self.objsPreguntas.append(nuevaPregunta)
            
        }
        
        //boton resultados
        self.botonResultados = UIButton(frame: CGRect(x: 10, y: posY + 5, width: self.anchoPantalla * 0.8, height: self.altoPantalla * 0.1))
        self.botonResultados.backgroundColor = UIColor(red: 0, green: 103/255, blue: 174/255, alpha: 1)
        self.botonResultados.layer.borderWidth = 0.5
        self.botonResultados.layer.cornerRadius = 3
        self.botonResultados.layer.borderColor = UIColor(red: 0, green: 81/255, blue: 163/255, alpha: 1).cgColor
        self.botonResultados.setTitle("GET RESULTS", for: .normal)
        self.botonResultados.setTitleColor(UIColor.white, for: .normal)
        self.botonResultados.titleLabel?.font = self.getFont(tipo: "respuesta")
        self.botonResultados.addTarget(self, action: #selector(self.calificaTest), for: .touchUpInside)
        
        self.botonResultados.center.x = self.anchoPantalla / 2
        
        self.scrollView.addSubview(self.botonResultados)
        
        self.scrollView.contentSize.height = botonResultados.frame.maxY + self.altoPantalla * 0.25
        self.view.addSubview(self.scrollView)
    }
    
    func createRadioButton(_ frame : CGRect, title : String, color : UIColor) -> DLRadioButton {
        let radioButton = DLRadioButton(frame: frame)
        radioButton.iconColor = color
        radioButton.iconSize = frame.size.width / 2
        radioButton.indicatorColor = color
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
        //radioButton.addTarget(self, action: #selector(DemoViewController.logSelectedButton), for: UIControlEvents.touchUpInside)
        return radioButton;
    }
    
    func selectRadioButton(sender: UITapGestureRecognizer){
        if (self.botonResultados.isEnabled) {
            let elTag = sender.view?.tag
            let elTagStr = String(describing: elTag!)
            let characters = elTagStr.characters.map { String($0) }
            let numPregunta = Int(characters[0])! - 1
            let numRespuesta = Int(characters[1])! - 1
            self.objsPreguntas[numPregunta].respuestas[numRespuesta].radio_button.isSelected = true
        }
    }
    
    func calificaTest(){
        
        self.aciertos = 0
        
        self.botonResultados.isEnabled = false
        
        for index in 0...self.numPreguntas - 1 {
            let xpos = self.objsPreguntas[index].label_num_pregunta.frame.maxX
            let ypos = self.objsPreguntas[index].label_num_pregunta.frame.minY
            
            let viewResultado = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
            viewResultado.layer.cornerRadius = 10
            let labelResultado = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 10))
            labelResultado.font = self.getFont(tipo: "pregunta")
            labelResultado.textColor = UIColor.white
            viewResultado.addSubview(labelResultado)
            self.scrollView.addSubview(viewResultado)
            
            let selectedRadio = self.objsPreguntas[index].respuestas[0].radio_button.selected()
            
            if selectedRadio != nil {
                if (selectedRadio?.tag == self.objsPreguntas[index].tag_correcto){
                    self.aciertos += 1
                    labelResultado.text = "Correcto"
                    labelResultado.sizeToFit()
                    var frameVar = viewResultado.frame
                    
                    frameVar.origin = CGPoint(x: xpos + 10, y: ypos)
                    frameVar.size = CGSize(width: labelResultado.frame.size.width + 10, height: labelResultado.frame.size.height + 10)
                    viewResultado.frame = frameVar
                    viewResultado.backgroundColor = Constantes.coloresResultadoTest.verde
                } else {
                    labelResultado.text = "Incorrecto"
                    labelResultado.sizeToFit()
                    var frameVar = viewResultado.frame
                    
                    frameVar.origin = CGPoint(x: xpos + 10, y: ypos)
                    frameVar.size = CGSize(width: labelResultado.frame.size.width + 10, height: labelResultado.frame.size.height + 10)
                    viewResultado.frame = frameVar
                    viewResultado.backgroundColor = Constantes.coloresResultadoTest.rojo
                }
            } else {
                labelResultado.text = "Incorrecto"
                labelResultado.sizeToFit()
                var frameVar = viewResultado.frame
                
                frameVar.origin = CGPoint(x: xpos + 10, y: ypos)
                frameVar.size = CGSize(width: labelResultado.frame.size.width + 10, height: labelResultado.frame.size.height + 10)
                viewResultado.frame = frameVar
                viewResultado.backgroundColor = Constantes.coloresResultadoTest.rojo
            }
            viewResultado.center.y = ypos + self.objsPreguntas[index].label_num_pregunta.frame.size.height / 2
            labelResultado.center.y = viewResultado.frame.size.height / 2
            labelResultado.center.x = viewResultado.frame.size.width / 2
            self.objsPreguntas[index].respuestas[0].radio_button.isUserInteractionEnabled = false
            self.objsPreguntas[index].respuestas[1].radio_button.isUserInteractionEnabled = false
            self.objsPreguntas[index].respuestas[2].radio_button.isUserInteractionEnabled = false
        }
        let alert = UIAlertController(title: "Felicidades", message: "Has completado el test. Tu resultado es \(self.aciertos) de \(self.numPreguntas)", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
}

class Pregunta {
    var label_num_pregunta : UILabel!
    var texto_pregunta : String!
    var tag_correcto : Int!
    var respuestas : [Respuesta]!
}

class Respuesta {
    var texto_respuesta : String!
    var label_respuesta : UILabel!
    var radio_button : DLRadioButton!
    var es_correcta : Bool = false
}
