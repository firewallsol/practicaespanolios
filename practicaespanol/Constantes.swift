//
//  Constantes.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 05/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import Foundation
import UIKit

struct Constantes {
    static let IDNavPrincipal = "navPrincipal"
    static let facebookAddr = "https://www.facebook.com/practicaespanol"
    static let quienesSomosYTVideoID = "ybLOSqrauK4"
    static let firebaseStorageRef = "gs://practica-espanol-7c3f1.appspot.com/profile_images"
    static let facebookUrlAuth = "https://practica-espanol-7c3f1.firebaseapp.com/__/auth/handler"
    
    
    struct WSData {
        //produccion
        static let urlRestBase = "http://www.practicaespanol.com/wp-json/wp/v2/posts"
        static let metodoHTTP = "GET"
        static let urlNoConfundas = "http://www.practicaespanol.com/wp-json/wp/v2/no_confundas/"
    }
    
    struct coloresDificultad {
        static let verde = UIColor(red: 124/255, green: 162/255, blue: 45/255, alpha: 1)
        static let rojo = UIColor(red: 229/255, green: 26/255, blue: 4/255, alpha: 1)
        static let amarillo = UIColor(red: 1, green: 164/255, blue: 0, alpha: 1)
        
    }
    
    struct coloresResultadoTest {
        static let verde = UIColor(red: 0, green: 100/255, blue: 50/255, alpha: 1)
        static let rojo = UIColor(red: 181/255, green: 40/255, blue: 30/255, alpha: 1)
    }
    
    struct paletaColores {
        static let grisFondo = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1)
        static let fondoSideMenu = UIColor(red: 17/255, green: 17/255, blue: 17/255, alpha: 1)
        static let rojoNavBar = UIColor(red: 203/255, green: 7/255, blue: 11/255, alpha: 1)
        static let azulBtnFacebook = UIColor(red: 0, green: 101/255, blue: 179/255, alpha: 1)
        static let rojoBtnLoginEmail = UIColor(red: 176/255, green: 25/255, blue: 30/255, alpha: 1)
        static let bordeAzulBotonesLogin = UIColor(red: 18/255, green: 68/255, blue: 158/255, alpha: 1)
        static let bordeRojoBotonesLogin = UIColor(red: 176/255, green: 25/255, blue: 30/255, alpha: 1)
    }
    
    struct topics {
        static let topicURLA1 = "nivel_A1"
        static let topicURLA2 = "nivel_A2"
        static let topicURLB1 = "nivel_B1"
        static let topicURLB2 = "nivel_B2"
        static let topicURLC1 = "nivel_C1"
        static let topicURLC2 = "nivel_C2"
    }
}
