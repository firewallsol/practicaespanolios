//
//  FiltroNoConfundasVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 03/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class FiltroNoConfundasVC: UIViewController, UIPickerViewDelegate {
    
    var txtLetra : UITextField!
    var vistoSwitch : UISwitch!
    var scrollView : UIScrollView!
    
    var pickerLetra = UIPickerView()
    
    var altoPantalla : CGFloat = 0
    var anchoPantalla : CGFloat = 0
    
    let notifFiltro = Notification.Name("aplicaFiltroNoConf")
    
    let arrLetras = ["TODOS","A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","T","U","V","W","X","Y","Z",]
    
    var letraActual = ""
    var vistoOnOff = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.white
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.setNavBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBar(){
        let aplicarButton = UIBarButtonItem(title: "Aplicar", style: .done, target: self, action: #selector(self.aplicaFiltro(sender:)))
        self.navigationItem.rightBarButtonItem = aplicarButton
    }
    
    func aplicaFiltro(sender: UIBarButtonItem){
        let letraString = self.arrLetras[self.arrLetras.index(of: self.txtLetra.text!)!]
        let visto = self.vistoSwitch.isOn
        let paramFiltro = ["letra": letraString, "visto": visto] as NSDictionary
        NotificationCenter.default.post(name: self.notifFiltro, object: paramFiltro)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func getFont(tipo: tipoFuente)->UIFont{
        
        var font = UIFont()
        
        switch tipo {
        case .Etiqueta:
            font = UIFont.boldSystemFont(ofSize: 17)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)
            }
            break
        case .Texto:
            font = UIFont.systemFont(ofSize: 14)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            }
            break
        }
        
        return font
    }
    
    func buildInterface(){
        self.title = "FILTRO"
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        //label letra
        var xframe = CGRect()
        xframe.origin = CGPoint(x: 20, y: 10)
        xframe.size = CGSize(width: 200, height: 10)
        let lblLetra = UILabel(frame: xframe)
        lblLetra.text = "LETRA"
        lblLetra.font = self.getFont(tipo: .Etiqueta)
        lblLetra.sizeToFit()
        self.scrollView.addSubview(lblLetra)
        
        var posy = lblLetra.frame.maxY
        
        //picker letra
        xframe = self.pickerLetra.frame
        xframe.size.height = 180
        self.pickerLetra.frame = xframe
        self.pickerLetra.delegate = self
        let toolBarLetra = UIToolbar()
        toolBarLetra.barStyle = UIBarStyle.default
        toolBarLetra.isTranslucent = true
        toolBarLetra.sizeToFit()
        
        let okBtnNivel = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerLetra(sender:)))
        let spaceBtnNivel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelBtnNivel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerLetra(sender:)))
        
        toolBarLetra.setItems([cancelBtnNivel, spaceBtnNivel, okBtnNivel], animated: false)
        toolBarLetra.isUserInteractionEnabled = true
        
        xframe.origin = CGPoint(x: 0, y: posy + 10)
        xframe.size = CGSize(width: self.anchoPantalla, height: 40)
        self.txtLetra = UITextField(frame: xframe)
        self.txtLetra.text = "TODOS"
        self.txtLetra.font = self.getFont(tipo: .Texto)
        self.txtLetra.textAlignment = .center
        self.txtLetra.layer.borderColor = UIColor.lightGray.cgColor
        self.txtLetra.layer.borderWidth = 0.5
        self.txtLetra.inputView = self.pickerLetra
        self.txtLetra.inputAccessoryView = toolBarLetra
        self.txtLetra.tintColor = UIColor.clear
        self.scrollView.addSubview(self.txtLetra)
        
        posy = self.txtLetra.frame.maxY
        
        //switch
        xframe.origin = CGPoint(x: 20, y: posy + 25)
        xframe.size = CGSize(width: 100, height: 10)
        let lblVisto = UILabel(frame: xframe)
        
        let index = Functions().getIdiomaIndex()
        
        lblVisto.text = idiomasClass().arrayIdiomas[index].filtro_visto
        lblVisto.font = self.getFont(tipo: .Etiqueta)
        lblVisto.sizeToFit()
        self.scrollView.addSubview(lblVisto)
        
        posy = lblVisto.frame.maxY
        
        xframe.origin = CGPoint(x: lblVisto.frame.maxX + 10, y: posy)
        xframe.size = CGSize(width: self.anchoPantalla * 0.1, height: self.altoPantalla * 0.5)
        self.vistoSwitch = UISwitch(frame: xframe)
        
        self.scrollView.addSubview(self.vistoSwitch)
        self.vistoSwitch.center.y = lblVisto.frame.minY + lblVisto.frame.size.height / 2
        
        self.scrollView.contentSize.height = posy + (self.altoPantalla * 0.25)
        
        self.view.addSubview(self.scrollView)
        
        self.txtLetra.text = self.arrLetras[self.arrLetras.index(of: self.letraActual)!]
        self.vistoSwitch.isOn = self.vistoOnOff
    }
    
    func donePickerLetra(sender: UIBarButtonItem){
        if (sender.title == "OK"){
            self.txtLetra.text = self.arrLetras[self.pickerLetra.selectedRow(inComponent: 0)]
        }
        
        self.view.endEditing(true)
    }
    
    //picker view delegate
    func numberOfComponentsInPickerView(pickerView : UIPickerView!) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.arrLetras.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.arrLetras[row]
        
    }
    
    //picker view delegate - fin
    


}
