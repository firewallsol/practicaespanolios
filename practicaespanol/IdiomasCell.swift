//
//  IdiomasCell.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 22/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class IdiomasCell: UITableViewCell {

    var lblIdioma : UILabel!
    var imgCheck : UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        self.buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func buildInterface(){
        self.lblIdioma = UILabel()
        self.contentView.addSubview(self.lblIdioma)
        
        self.imgCheck = UIImageView()
        self.imgCheck.contentMode = .scaleAspectFill
        self.imgCheck.image = UIImage(named: "checkmark_rojo_64")
        self.contentView.addSubview(self.imgCheck)
    }

}
