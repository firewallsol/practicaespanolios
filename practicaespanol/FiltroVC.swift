//
//  FiltroVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 20/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class FiltroVC: UIViewController, UIPickerViewDelegate {
    
    var txtNivel : UITextField!
    var txtCategoria : UITextField!
    var vistoSwitch : UISwitch!
    var scrollView : UIScrollView!
    
    var pickerNivel = UIPickerView()
    var pickerCategoria = UIPickerView()
    
    var altoPantalla : CGFloat = 0
    var anchoPantalla : CGFloat = 0
    
    var strCategoriaActual = ""
    var strNivelActual = ""
    var vistoOnOff = false
    
    let notifFiltro = Notification.Name("aplicaFiltro")
    let notifSetNumCat = Notification.Name("setNumCat")
    
    let arrNiveles = ["TODOS LOS NIVELES","A","B", "C"]
    //let numNiveles = []
    let arrCategorias = ["TODAS LAS CATEGORIAS","CIENCIA","CULTURA","DEPORTES","ECONOMIA","ENTRETENIMIENTO","ESPAÑA","GASTRONOMIA","MODA","MUNDO","NATURALEZA","SALUD","VIAJES"]
    let numCategorias = [21,44,45,1825,53,48,49,50,348,52,250,2145,35]
    let arrCatToQuery = ["noticias", "150-ciencia", "140-cultura","deportes","160-economia","190-entretenimiento","110-espana","210-gastronomia","200-moda","120-mundo","naturaleza","180-salud","130-viajes"]
    let arrNivelToQuey = ["","nivel-a","nivel-b","nivel-c"]
    let arrNIvelA = [1262, 55]
    let arrNIvelB = [56, 57]
    let arrNIvelC = [209, 58]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        
        self.altoPantalla = self.view.frame.size.height
        self.anchoPantalla = self.view.frame.size.width
        
        self.setNavBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBar(){
        //aplicar
        let aplicarButton  = UIBarButtonItem(title: "Aplicar", style: .done, target: self, action: #selector(self.aplicaFiltro(sender:)))
        self.navigationItem.rightBarButtonItem = aplicarButton
    }
    
    func aplicaFiltro(sender: UIBarButtonItem){
        //se manda el texto en string, se comenta el codigo por clave numerica
        let categoriaNum = self.numCategorias[self.arrCategorias.index(of: self.txtCategoria.text!)!]
        let categoriaString = self.arrCatToQuery[self.arrCategorias.index(of: self.txtCategoria.text!)!]
        let nivelString = self.arrNivelToQuey[self.arrNiveles.index(of: self.txtNivel.text!)!]
        let visto = self.vistoSwitch.isOn
        
        var nivelArray = [Int]()
        
        switch nivelString {
            //"nivel-a","nivel-b","nivel-c"
        case "nivel-a": nivelArray = self.arrNIvelA
            break
        case "nivel-b": nivelArray = self.arrNIvelB
            break
        case "nivel-c": nivelArray = self.arrNIvelC
            break
        default:
            break
        }
        
        let paramFiltro = [ "categoria": categoriaString, "nivel": nivelString, "visto": visto, "cat_num": categoriaNum,
                            "nivel_num": nivelArray] as NSDictionary
        NotificationCenter.default.post(name: self.notifFiltro, object: paramFiltro)
        NotificationCenter.default.post(name: self.notifSetNumCat, object: paramFiltro)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func buildInterface(){
        self.title = "FILTRO"
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        let index = Functions().getIdiomaIndex()
        
        //label nivel
        var xframe = CGRect()
        xframe.origin = CGPoint(x: 20, y: 10)
        xframe.size = CGSize(width: 200, height: 10)
        let lblNivel = UILabel(frame: xframe)
        lblNivel.text = idiomasClass().arrayIdiomas[index].filtro_nivel
        lblNivel.font = self.getFont(tipo: .Etiqueta)
        lblNivel.sizeToFit()
        self.scrollView.addSubview(lblNivel)
        
        var posy = lblNivel.frame.maxY
        
        //picker nivel
        xframe = self.pickerNivel.frame
        xframe.size.height = 180
        self.pickerNivel.frame = xframe
        self.pickerNivel.delegate = self
        let toolBarNivel = UIToolbar()
        toolBarNivel.barStyle = UIBarStyle.default
        toolBarNivel.isTranslucent = true
        toolBarNivel.sizeToFit()
        
        let okBtnNivel = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerNivel(sender:)))
        let spaceBtnNivel = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelBtnNivel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerNivel(sender:)))
        
        toolBarNivel.setItems([cancelBtnNivel, spaceBtnNivel, okBtnNivel], animated: false)
        toolBarNivel.isUserInteractionEnabled = true

        //text nivel
        xframe.origin = CGPoint(x: 0, y: posy + 10)
        xframe.size = CGSize(width: self.anchoPantalla, height: 40)
        self.txtNivel = UITextField(frame: xframe)
        self.txtNivel.text = "TODOS LOS NIVELES"
        self.txtNivel.font = self.getFont(tipo: .Texto)
        self.txtNivel.textAlignment = .center
        self.txtNivel.layer.borderColor = UIColor.lightGray.cgColor
        self.txtNivel.layer.borderWidth = 0.5
        self.txtNivel.inputView = self.pickerNivel
        self.txtNivel.inputAccessoryView = toolBarNivel
        self.txtNivel.tintColor = UIColor.clear
        self.scrollView.addSubview(self.txtNivel)
        
        posy = self.txtNivel.frame.maxY
        
        //label catgoria
        xframe.origin = CGPoint(x: 20, y: posy + 25)
        xframe.size = CGSize(width: 200, height: 10)
        let lblCategoria = UILabel(frame: xframe)
        lblCategoria.text = idiomasClass().arrayIdiomas[index].filtro_categoria
        lblCategoria.font = self.getFont(tipo: .Etiqueta)
        lblCategoria.sizeToFit()
        self.scrollView.addSubview(lblCategoria)
        
        posy = lblCategoria .frame.maxY
        
        //picker categoria
        xframe = self.pickerCategoria.frame
        xframe.size.height = 180
        self.pickerCategoria.frame = xframe
        self.pickerCategoria.delegate = self
        let toolBarCategoria = UIToolbar()
        toolBarCategoria.barStyle = UIBarStyle.default
        toolBarCategoria.isTranslucent = true
        toolBarCategoria.sizeToFit()
        
        let okBtnCategoria = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerCategoria(sender:)))
        let spaceBtnCategoria = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelBtnCategoria = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePickerCategoria(sender:)))
        
        toolBarCategoria.setItems([cancelBtnCategoria, spaceBtnCategoria, okBtnCategoria], animated: false)
        toolBarCategoria.isUserInteractionEnabled = true
        
        //text categoria
        xframe.origin = CGPoint(x: 0, y: posy + 10)
        xframe.size = CGSize(width: self.anchoPantalla, height: 40)
        self.txtCategoria = UITextField(frame: xframe)
        self.txtCategoria.text = "TODAS LAS CATEGORIAS"
        self.txtCategoria.font = self.getFont(tipo: .Texto)
        self.txtCategoria.textAlignment = .center
        self.txtCategoria.layer.borderColor = UIColor.lightGray.cgColor
        self.txtCategoria.layer.borderWidth = 0.5
        self.txtCategoria.inputView = self.pickerCategoria
        self.txtCategoria.inputAccessoryView = toolBarCategoria
        self.txtCategoria.tintColor = UIColor.clear
        self.scrollView.addSubview(self.txtCategoria)
        
        posy = self.txtCategoria.frame.maxY
        
        //switch
        xframe.origin = CGPoint(x: 20, y: posy + 25)
        xframe.size = CGSize(width: 100, height: 10)
        let lblVisto = UILabel(frame: xframe)
        lblVisto.text = idiomasClass().arrayIdiomas[index].filtro_visto
        lblVisto.font = self.getFont(tipo: .Etiqueta)
        lblVisto.sizeToFit()
        self.scrollView.addSubview(lblVisto)
        
        posy = lblVisto.frame.maxY
        
        xframe.origin = CGPoint(x: lblVisto.frame.maxX + 10, y: posy)
        xframe.size = CGSize(width: self.anchoPantalla * 0.1, height: self.altoPantalla * 0.5)
        self.vistoSwitch = UISwitch(frame: xframe)
        self.scrollView.addSubview(self.vistoSwitch)
        self.vistoSwitch.center.y = lblVisto.frame.minY + lblVisto.frame.size.height / 2

        self.scrollView.contentSize.height = posy + (self.altoPantalla * 0.25)
        
        self.view.addSubview(self.scrollView)
        
        self.txtCategoria.text = self.arrCategorias[self.arrCatToQuery.index(of: self.strCategoriaActual)!]
        self.txtNivel.text = self.arrNiveles[self.arrNivelToQuey.index(of: self.strNivelActual)!]
        self.vistoSwitch.isOn = self.vistoOnOff
        
    }
    
    func donePickerNivel(sender: UIBarButtonItem){
        if (sender.title == "OK"){
            self.txtNivel.text = self.arrNiveles[self.pickerNivel.selectedRow(inComponent: 0)]
        }
        
        self.view.endEditing(true)
    }
    
    func donePickerCategoria(sender: UIBarButtonItem){
        if (sender.title == "OK"){
            self.txtCategoria.text = self.arrCategorias[self.pickerCategoria.selectedRow(inComponent: 0)]
        }
        
        self.view.endEditing(true)
    }
    
    //picker view delegate
    func numberOfComponentsInPickerView(pickerView : UIPickerView!) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        var items = 0
        if self.pickerNivel == pickerView {
            items = self.arrNiveles.count
        } else if self.pickerCategoria == pickerView {
            items = self.arrCategorias.count
        }
        return items
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var item = ""
        if self.pickerNivel == pickerView {
            item = self.arrNiveles[row]
        } else if self.pickerCategoria == pickerView {
            item = self.arrCategorias[row]
        }
        
        return item
        
    }

    //picker view delegate - fin
    
    
    func getFont(tipo: tipoFuente)->UIFont{
        
        var font = UIFont()
        
        switch tipo {
        case .Etiqueta:
            font = UIFont.boldSystemFont(ofSize: 17)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 17, weight: UIFontWeightBold)
            }
            break
        case .Texto:
            font = UIFont.systemFont(ofSize: 14)
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
            }
            break
        }
        
        return font
    }

    
} // fin de clase

enum tipoFuente: String {
    case Etiqueta = "etiqueta"
    case Texto = "texto"
}
