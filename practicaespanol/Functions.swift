//
//  Functions.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 06/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase

class Functions {
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    let userDefaults = UserDefaults.standard
    
    func startActivityIndicator(sender: UIViewController) {
        let screenSize: CGRect = UIScreen.main.bounds
        self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        self.activityIndicator.frame = CGRect(x:0, y:0, width: screenSize.width, height: screenSize.height)
        self.activityIndicator.center = sender.view.center
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        
        // Change background color and alpha channel here
        self.activityIndicator.backgroundColor = UIColor.black
        self.activityIndicator.clipsToBounds = true
        self.activityIndicator.alpha = 0.5
        
        sender.view.addSubview(self.activityIndicator)
        sender.view.bringSubview(toFront: self.activityIndicator)
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    
    func stringFromHtml(string: String, tipo: String) -> NSAttributedString? {
        do {
            var newHTML = ""
            switch tipo {
            case "titulo":
                newHTML = "<html><head>" +
                    "   <style type='text/css'>" +
                    " @font-face { " +
                    " font-family: \"San Francisco\"; " +
                    " font-weight: bold; " +
                    " src: url(\"https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-bold-webfont.woff\"); " +
                    "}" +
                    "       body {font-size: 15px; font-family: 'San Francisco'; font-weight: bold; color:white; }" +
                    "   </style>" +
                    "</head><body>" + string + "</body></html>"
                break
            case "texto":
                newHTML = "<html><head>" +
                    "   <style type='text/css'>" +
                    " @font-face { " +
                    " font-family: \"San Francisco\"; " +
                    " font-weight: bold; " +
                    " src: url(\"https://applesocial.s3.amazonaws.com/assets/styles/fonts/sanfrancisco/sanfranciscodisplay-bold-webfont.woff\"); " +
                    "}" +
                    "       body {font-size: 13px; font-family: 'San Francisco'; font-weight: normal; }" +
                    "   </style>" +
                    "</head><body>" + string + "</body></html>"
                break
            default: newHTML = string
                break
            }
            
            let data = newHTML.data(using: String.Encoding.unicode, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                
                return str
            }
        } catch {
        }
        return nil
    }
    
    func getNivelDificultad(arr_ids: [Int])->String{
        
        var categoria = ""
        
        for id in arr_ids {
            if categoria != "" {
                break
            }
            switch id {
            case 1262: categoria = "A1"
            case 55: categoria = "A2"
            case 56: categoria = "B1"
            case 57: categoria = "B2"
            case 209: categoria = "C1"
            case 58: categoria = "C2"
            default:
                break
            }
        }
        
        if categoria == "" {
            for id in arr_ids {
                if categoria != "" {
                    break
                }
                switch id {
                case 200: categoria = "A"
                case 201: categoria = "B"
                case 202: categoria = "C"
                default:
                    break
                }
            }
            
        }
        
        return categoria
    }
    
    func getSeccionNoticia(arr_ids: [Int])-> String{
        var categoria = ""
        
        for id in arr_ids {
            if categoria != "" {
                break
            }
            switch id {
            case 44: categoria = "Ciencia"
            case 45: categoria = "Cultura"
            case 1825: categoria = "Deportes"
            case 53: categoria = "Economía"
            case 48: categoria = "Entretenimiento"
            case 49: categoria = "España"
            case 50: categoria = "Gastronomía"
            case 348: categoria = "Moda"
            case 52: categoria = "Mundo"
            case 250: categoria = "Naturaleza"
            case 2145: categoria = "Salud"
            case 35: categoria = "Viajes"
            default:
                break
            }
        }
        
        return categoria
    }
    
    func getColorDificultad(tipo: String)->UIColor{
        let firstChar = tipo[tipo.startIndex]
        var elColor = UIColor()
        switch firstChar {
        case "A": elColor = Constantes.coloresDificultad.amarillo;  break;
        case "B": elColor = Constantes.coloresDificultad.rojo;  break;
        case "C": elColor = Constantes.coloresDificultad.verde;  break;
        default: elColor = UIColor.clear; break;
        }
        return elColor
    }
    
    func getFileNameFromUrl(laurl: String)->String{
        var fileName = ""
        let http_rest = laurl.components(separatedBy: "//")
        let url_parts = http_rest[1].components(separatedBy: "/")
        fileName = url_parts[url_parts.count - 1]
        return fileName
    }
    
    func getUrlWithoutFileName(laurl: String)->String{
        var urlStr = ""
        let http_rest = laurl.components(separatedBy: "//")
        let url_parts = http_rest[1].components(separatedBy: "/")
        var tempStr = ""
        for index in 0...url_parts.count - 2 {
            tempStr += url_parts[index] + "/"
        }
        urlStr = http_rest[0] + "//" + tempStr
        return urlStr
    }
    
    func statusBarHeight() -> CGFloat {
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        return Swift.min(statusBarSize.width, statusBarSize.height)
    }
    
    func getLongStrFechaFromISO8601(fechaStr : String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //2017-02-01T13:12:19
        let lafecha = dateFormatter.date(from: fechaStr)
        
        dateFormatter.dateFormat = "MM"
        let elMes = self.getLongMonthString(mes: dateFormatter.string(from: lafecha!))
        
        dateFormatter.dateFormat = "d 'de \(elMes) de' yyyy"
        return dateFormatter.string(from: lafecha!)
    }
    
    func getLongMonthString(mes: String)->String{
        var str = ""
        switch mes {
        case "01": str = "Enero"; break;
        case "02": str = "Febrero"; break;
        case "03": str = "Marzo"; break;
        case "04": str = "Abril"; break;
        case "05": str = "Mayo"; break;
        case "06": str = "Junio"; break;
        case "07": str = "Julio"; break;
        case "08": str = "Agosto"; break;
        case "09": str = "Septiembre"; break;
        case "10": str = "Octubre"; break;
        case "11": str = "Noviembre"; break;
        case "12": str = "Diciembre"; break;
        default: str = "NAM"; break;
        }
        return str
    }
    
    func getYouTubeID(urlvideo: String)-> String{
        let http_parts = urlvideo.components(separatedBy: "//")
        let url_parts = http_parts[1].components(separatedBy: "/")
        return url_parts[url_parts.count - 1]
    }
    
    func JSONtoArrayNoticia(elJson: JSON)-> [Noticia]{
        var arrNoticias = [Noticia]()
        for element in elJson.arrayValue {
            let id = element["id"].int
            let fecha = self.getLongStrFechaFromISO8601(fechaStr: element["date"].string!)
            let link = element["link"].string
            var titulo = ""
            if let str = element["title"]["rendered"].string {
                titulo = str
            } else {
                titulo = element["title"].string!
            }
            
            var strContenido = ""
            if let str = element["content"]["rendered"].string {
                strContenido = str
            } else {
                strContenido = element["content"].string!
            }
            
            let categorias = element["categories"].arrayObject as! [Int]
            
            var url_imagen = ""
            if (element["imageApp"].exists()){
                if (element["imageApp"].string! != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: element["imageApp"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: element["imageApp"].string!)
                    url_imagen = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            var ejercicios = JSON.null
            var num_ejericios = 0
            if (element["exercises"].exists()){
                num_ejericios = element["exercises"]["number_of_exercises"].int!
                ejercicios = element["exercises"]
            }
            
            var url_audio = ""
            if (element["audio"].exists()){
                if (element["audio"].string != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: element["audio"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: element["audio"].string!)
                    url_audio = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            
            var url_video = ""
            var id_video = ""
            if (element["video"].exists()){
                if (element["video"].string != ""){
                    url_video = element["video"].string!
                    id_video = self.getYouTubeID(urlvideo: url_video)
                }
            }
            
            let unaNoticia = Noticia(id: id!, fecha: fecha, link: link!, titulo: titulo, contenido: strContenido, categorias: categorias, url_imagen: url_imagen, ejercicios: ejercicios, num_ejercicios: num_ejericios, url_audio: url_audio, url_video: url_video, id_video: id_video, jsonString: element.rawString())
            arrNoticias.append(unaNoticia)
        }
        return arrNoticias
    }
    
    func JSONtoNoticia(elJson: JSON)-> Noticia{
        
            let id = elJson["id"].int
            let fecha = self.getLongStrFechaFromISO8601(fechaStr: elJson["date"].string!)
            let link = elJson["link"].string
            var titulo = ""
            if let str = elJson["title"]["rendered"].string {
                titulo = str
            } else {
                titulo = elJson["title"].string!
            }
            
            var strContenido = ""
            if let str = elJson["content"]["rendered"].string {
                strContenido = str
            } else {
                strContenido = elJson["content"].string!
            }
            
            let categorias = elJson["categories"].arrayObject as! [Int]
            
            var url_imagen = ""
            if (elJson["imageApp"].exists()){
                if (elJson["imageApp"].string! != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: elJson["imageApp"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: elJson["imageApp"].string!)
                    url_imagen = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            var ejercicios = JSON.null
            var num_ejericios = 0
            if (elJson["exercises"].exists()){
                num_ejericios = elJson["exercises"]["number_of_exercises"].int!
                ejercicios = elJson["exercises"]
            }
            
            var url_audio = ""
            if (elJson["audio"].exists()){
                if (elJson["audio"].string != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: elJson["audio"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: elJson["audio"].string!)
                    url_audio = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            
            var url_video = ""
            var id_video = ""
            if (elJson["video"].exists()){
                if (elJson["video"].string != ""){
                    url_video = elJson["video"].string!
                    id_video = self.getYouTubeID(urlvideo: url_video)
                }
            }
            
            let unaNoticia = Noticia(id: id!, fecha: fecha, link: link!, titulo: titulo, contenido: strContenido, categorias: categorias, url_imagen: url_imagen, ejercicios: ejercicios, num_ejercicios: num_ejericios, url_audio: url_audio, url_video: url_video, id_video: id_video, jsonString: elJson.rawString())
        
        
        return unaNoticia
    }
    
    func JSONtoArrayBoletin(elJson: JSON)-> [Boletin]{
        var arrBoletines = [Boletin]()
        for element in elJson.arrayValue {
            let id = element["id"].int
            let fecha = self.getLongStrFechaFromISO8601(fechaStr: element["date"].string!)
            let link = element["link"].string
            var titulo = ""
            if let str = element["title"]["rendered"].string {
                titulo = str
            } else {
                titulo = element["title"].string!
            }
            
            var strContenido = ""
            if let str = element["content"]["rendered"].string {
                strContenido = str
            } else {
                strContenido = element["content"].string!
            }
            
            let categorias = element["categories"].arrayObject as! [Int]
            
            var ejercicios = JSON.null
            var num_ejericios = 0
            if (element["exercises"].exists()){
                num_ejericios = element["exercises"]["number_of_exercises"].int!
                ejercicios = element["exercises"]
            }
            
            var url_audio = ""
            if (element["audio"].exists()){
                if (element["audio"].string != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: element["audio"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: element["audio"].string!)
                    url_audio = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            
            var url_video = ""
            var id_video = ""
            if (element["video"].exists()){
                if (element["video"].string != ""){
                    url_video = element["video"].string!
                    id_video = self.getYouTubeID(urlvideo: url_video)
                }
            }
            
            let unBoletin = Boletin(id: id!, fecha: fecha, link: link!, titulo: titulo, contenido: strContenido, categorias: categorias, ejercicios: ejercicios, num_ejercicios: num_ejericios, url_audio: url_audio, url_video: url_video, id_video: id_video, jsonString: element.rawString()!)
            arrBoletines.append(unBoletin)
        }
        return arrBoletines
    }
    
    func JSONtoArrayNoConfundas(elJson: JSON)-> [NoConfundas]{
        var arrNoConfundas = [NoConfundas]()
        for element in elJson.arrayValue {
            let unObjeto = NoConfundas()
            
            unObjeto.id = element["id"].int
            unObjeto.fecha = self.getLongStrFechaFromISO8601(fechaStr: element["date"].string!)
            unObjeto.link = element["link"].string
            
            if let str = element["title"]["rendered"].string {
                unObjeto.titulo = str
            } else {
                unObjeto.titulo = element["title"].string!
            }
            
            if let str = element["content"]["rendered"].string {
                unObjeto.contenido = str
            } else {
                unObjeto.contenido = element["content"].string!
            }
            
            unObjeto.palabra1 = element["palabra_1"].string
            unObjeto.ej1_palabra1 = element["ejemplo_1_palabra_1"].string
            unObjeto.ej2_palabra1 = element["ejemplo_2_palabra_1"].string
            
            if (element["audio_palabra_1"].exists()){
                if (element["audio_palabra_1"].string != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: element["audio_palabra_1"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: element["audio_palabra_1"].string!)
                    unObjeto.url_audio_palabra1 = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            
            unObjeto.palabra2 = element["palabra_2"].string
            unObjeto.ej1_palabra2 = element["ejemplo_1_palabra_2"].string
            unObjeto.ej2_palabra2 = element["ejemplo_2_palabra_2"].string
            
            if (element["audio_palabra_2"].exists()){
                if (element["audio_palabra_2"].string != ""){
                    let urlString = self.getUrlWithoutFileName(laurl: element["audio_palabra_2"].string!)
                    let fileName = self.getFileNameFromUrl(laurl: element["audio_palabra_2"].string!)
                    unObjeto.url_audio_palabra2 = urlString + fileName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                }
            }
            let palabra1 = element["palabra_2"].string!
            let primeraLetra = palabra1.characters[(palabra1.characters.startIndex)]
            let primeraLetraStr = String(describing: primeraLetra)
            unObjeto.primera_letra = primeraLetraStr.uppercased()
                
            arrNoConfundas.append(unObjeto)
        }
        return arrNoConfundas
    }
    
    func agregarQuitarVisto(id_nota: Int)-> String{
        var mensaje = "Ocurrio un problema al intentar marcar/desmarcar la noticia como vista"
        
        var arrayVisto = self.getArrayVisto()
        
        let index = self.getIdiomaIndex()
        
        if let index = arrayVisto.index(of: id_nota) { //si se encuentra el id de la nota, se remueve del arreglo
            arrayVisto.remove(at: index)
            mensaje = "Noticia desmarcada como vista"
        } else { // si no se encuentra se agrega al arreglo
            arrayVisto.append(id_nota)
            mensaje = idiomasClass().arrayIdiomas[index].msg_guardado_visto
        }
        
        self.userDefaults.set(arrayVisto, forKey: "arrayVisto")
        return mensaje
    }
    
    func existeIdEnVisto(id_nota: Int)->Bool{
        let arrayVisto = self.getArrayVisto()
        if let _ = arrayVisto.index(of: id_nota) {
            return true
        } else {
            return false
        }
    }
    
    func getArrayVisto()->[Int]{
        var arrayV = [Int]()
        if let arrayVisto = self.userDefaults.array(forKey: "arrayVisto") as? [Int] { //si existe el arreglo
            arrayV = arrayVisto
        }
        return arrayV
    }
    
    func alertaSimple(titulo: String, mensaje: String, estilo: UIAlertControllerStyle, showButton: Bool = true)->UIAlertController{
        let alertController = UIAlertController(title: titulo, message: mensaje, preferredStyle: estilo)
        if showButton {
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
        }
        return alertController
    }
    
    func dismissAlertDelay(alerta: UIAlertController){
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            
            alerta.dismiss(animated: true, completion: nil)
        }
    }
    
    func agregarQuitarFavorito(id_nota: Int, xjson: String)->String{
        var mensaje = "Ocurrio un problema al intentar marcar/desmarcar la noticia como favorito"
        var dicFavoritos = self.getDicFavoritos()
        
        let index = self.getIdiomaIndex()
        
        if let _ = dicFavoritos[id_nota] { //si existe el json se elimina
            dicFavoritos.removeValue(forKey: id_nota)
            mensaje = "Noticia desmarcada como favorito"
        } else { //si no existe se agrega
            dicFavoritos[id_nota] = xjson
            
            mensaje = idiomasClass().arrayIdiomas[index].msg_guardado_favoritos
        }
        let data = NSKeyedArchiver.archivedData(withRootObject: dicFavoritos)
        self.userDefaults.set(data, forKey: "dicFavoritos")
        return mensaje
    }
    
    func existeIdEnFavoritos(id_nota: Int)->Bool{
        let dicFavoritos = self.getDicFavoritos()
        if let _ = dicFavoritos[id_nota] {
            return true
        } else {
            return false
        }
    }
    
    func getDicFavoritos()->[Int:String]{
        
        var dicFavoritos = [Int: String]()
        if let dicF = self.userDefaults.value(forKey: "dicFavoritos") as? NSData {
            let newDic = NSKeyedUnarchiver.unarchiveObject(with: dicF as Data) as! [Int:String]
            dicFavoritos = newDic
        }
        
        return dicFavoritos
    }
    
    //para las suscripciones de alertas
    func getDicAlertas()->[String:Bool]{
        var dicAlertas = [String:Bool]()
        if let dica = self.userDefaults.value(forKey: "dicAlertas") as? NSData {
            let newdic = NSKeyedUnarchiver.unarchiveObject(with: dica as Data) as! [String:Bool]
            dicAlertas = newdic
        }
        return dicAlertas
    }
    
    func agregarQuitarAlerta(nivel: String, estado: Bool) {
        var dicAlertas = self.getDicAlertas()
        dicAlertas[nivel] = estado
        let data = NSKeyedArchiver.archivedData(withRootObject: dicAlertas)
        self.userDefaults.setValue(data, forKey: "dicAlertas")
    }

    
    //PAra resize de la imagen
    func scaleAndCropImage(image:UIImage, toSize size: CGSize) -> UIImage {
        // Sanity check; make sure the image isn't already sized.
        if image.size.equalTo(size) {
            return image
        }
        
        let widthFactor = size.width / image.size.width
        let heightFactor = size.height / image.size.height
        var scaleFactor: CGFloat = 0.0
        
        scaleFactor = heightFactor
        
        if widthFactor > heightFactor {
            scaleFactor = widthFactor
        }
        
        var thumbnailOrigin = CGPoint.zero
        let scaledWidth  = image.size.width * scaleFactor
        let scaledHeight = image.size.height * scaleFactor
        
        if widthFactor > heightFactor {
            thumbnailOrigin.y = (size.height - scaledHeight) / 2.0
        }
            
        else if widthFactor < heightFactor {
            thumbnailOrigin.x = (size.width - scaledWidth) / 2.0
        }
        
        var thumbnailRect = CGRect.zero
        thumbnailRect.origin = thumbnailOrigin
        thumbnailRect.size.width  = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.draw(in: thumbnailRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func getFirAuthErrorMessage(elError : Error)->String{
        var mensaje = "Ocurrio un error al intentar crear el usuario"
        if let errCode = FIRAuthErrorCode(rawValue: elError._code) {
            
            switch errCode {
            case .errorCodeEmailAlreadyInUse:
                mensaje = "Este correo ya se encuentra en uso"
                break
            case .errorCodeUserDisabled:
                mensaje = "Esta cuenta se encuentra deshabilitada"
                break
            case .errorCodeInvalidEmail:
                mensaje = "Dirección de email no válida"
                break
            case .errorCodeWrongPassword:
                mensaje = "La contraseña es incorrecta"
                break
            case .errorCodeUserNotFound:
                mensaje = "No existe este usuario registrado"
                break
            case .errorCodeNetworkError:
                mensaje = "Ha ocurrido un problema con la red, intente de nuevo por favor"
                break
            case .errorCodeWeakPassword:
                mensaje = "Su contraseña es muy débil, por favor ingrese una nueva"
                break
            default: mensaje = "Ocurrio un error al intentar crear el usuario"
                
            }
        }
        return mensaje
    }
    
    func removeTodosFavoritos(){
        self.userDefaults.removeObject(forKey: "dicFavoritos")
    }
    
    func getIdiomaIndex()->Int{
        return self.userDefaults.integer(forKey: "indexIdioma")
    }
    
    func setIndexIdioma(elIndex: Int){
        self.userDefaults.set(elIndex, forKey: "indexIdioma")
    }
    
} //fin de clase
