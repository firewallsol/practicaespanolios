//
//  PrincipalPageC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 04/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class PrincipalPageC: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    let titlesViews : [String] = ["Noticias", "La voz","Boletín"]
    var currentIndex : Int = 0
    private var pages: [UIViewController]!
    var offsetAlto : CGFloat  = 0
    
    let notifCambiaMarcador = Notification.Name("setMarcador")
    let notifCambiaPagina = Notification.Name("cambiaPagina")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.cambiarPagina(notification:)), name: self.notifCambiaPagina, object: nil)
        
        dataSource = self
        delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadViews(){
        let vcnoticias = self.storyboard!.instantiateViewController(withIdentifier: "NoticiasVC") as! NoticiasVC
        vcnoticias.offsetAlto = self.offsetAlto
        var frameVar = vcnoticias.view.frame
        frameVar.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        vcnoticias.view.frame = frameVar
        
        let vcVozNoticia = self.storyboard!.instantiateViewController(withIdentifier: "vozNoticiaVC") as! vozNoticiaVC
        vcVozNoticia.offsetAlto = self.offsetAlto
        frameVar = vcVozNoticia.view.frame
        frameVar.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        vcVozNoticia.view.frame = frameVar
        
        let vcboletin = self.storyboard!.instantiateViewController(withIdentifier: "BoletinVC") as! BoletinVC
        vcboletin.offsetAlto = self.offsetAlto
        frameVar = vcboletin.view.frame
        frameVar.size = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        vcboletin.view.frame = frameVar
        
        self.pages = [vcnoticias, vcVozNoticia,vcboletin]
        
        let startingViewController = self.pages.first! as UIViewController
        self.setViewControllers([startingViewController], direction: .forward, animated: false, completion: nil)

    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else { return }
        
        let elTag = pageViewController.viewControllers?.first?.view.tag
        
        let paramPagina = [ "pagina": elTag!] as NSDictionary
        NotificationCenter.default.post(name: self.notifCambiaMarcador, object: paramPagina)
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return self.titlesViews.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return 0
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        self.currentIndex = (self.pages as NSArray).index(of: viewController)
        return (self.currentIndex == 0 ? nil : self.pages[self.currentIndex - 1])
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        self.currentIndex = (self.pages as NSArray).index(of: viewController)
        return (self.currentIndex == self.pages.count - 1 ? nil : self.pages[self.currentIndex + 1])
    }
    
    //cambio de pagina por botones de cabecera
    func cambiarPagina (notification: NSNotification){
        let paramMarcador = notification.object as! NSDictionary
        let pagActual = paramMarcador["pagina"] as! Int
        self.slideToPage(index: pagActual, completion: nil)
    }
    
    func slideToPage(index: Int, completion: (() -> Void)?){
        let tempIndex = self.currentIndex
        
        if self.currentIndex < index {
            
            for tempInt in tempIndex...index {
                self.setViewControllers([self.pages[tempInt]], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: {[weak self] (complete: Bool) -> Void in
                    if (complete) {
                        self?.currentIndex = tempInt
                        completion?()
                    }
                })
            }
            
        }
        else if self.currentIndex > index {
            
            for tempInt in stride(from: tempIndex, through: index, by: -1){
                self.setViewControllers([self.pages[tempInt]], direction: UIPageViewControllerNavigationDirection.reverse, animated: true, completion: {[weak self] (complete: Bool) -> Void in
                    if complete {
                        self?.currentIndex = tempInt
                        completion?()
                    }
                })
            }
            
        }
    }
}
