//
//  boletinCell.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 10/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class boletinCell: UITableViewCell {
    var viewContenedor : UIView!
    var lblTitulo : UILabel!
    var lblFecha : UILabel!
    var imgVisto : UIImageView!
    var imgEstrella : UIImageView!
    var btnPlay : UIImageView!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        self.frame.size.height = 70
        self.selectionStyle = .none
        self.buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildInterface(){
        //content view
        self.viewContenedor = UIView(frame: CGRect(x: 0, y: 0, width: self.anchoCelda * 0.97, height: self.altoCelda * 0.97))
        
        //boton play
        self.btnPlay = UIImageView(frame: CGRect(x: 10, y: 10, width: 40, height: 40))
        self.btnPlay.contentMode = .scaleAspectFit
        self.btnPlay.image = UIImage(named: "Play_rojo")
        self.viewContenedor.addSubview(self.btnPlay)
        
        //titulo
        self.lblTitulo = UILabel(frame: CGRect(x: 20, y: 10, width: 40, height: 15))
        var fontTitulo = UIFont.boldSystemFont(ofSize: 15)
        if #available(iOS 8.2, *) {
            fontTitulo = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
        }
        self.lblTitulo.font = fontTitulo
        self.lblTitulo.numberOfLines = 2
        self.lblTitulo.minimumScaleFactor = 0.5
        self.lblTitulo.adjustsFontSizeToFitWidth = true
        self.viewContenedor.addSubview(self.lblTitulo)
        
        //fecha
        self.lblFecha = UILabel(frame: CGRect(x: 20, y: 20, width: 40, height: 15))
        var fontFecha = UIFont.systemFont(ofSize: 14)
        if #available(iOS 8.2, *) {
            fontFecha = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        }
        self.lblFecha.font = fontFecha
        self.lblFecha.numberOfLines = 1
        self.viewContenedor.addSubview(self.lblFecha)
        
        //estrella
        self.imgEstrella = UIImageView(frame: CGRect(x: 50, y: 20, width: 30, height: 30))
        self.imgEstrella.contentMode = .scaleAspectFit
        self.imgEstrella.image = UIImage(named: "Estrella_amarillo")
        self.viewContenedor.addSubview(self.imgEstrella)
        
        //img ojo
        self.imgVisto = UIImageView(frame: CGRect(x: 60, y: 20, width: 30, height: 30))
        self.imgVisto.contentMode = .scaleAspectFit
        self.imgVisto.image = UIImage(named: "eye_negro")
        self.viewContenedor.addSubview(self.imgVisto)
        
        self.contentView.addSubview(self.viewContenedor)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
