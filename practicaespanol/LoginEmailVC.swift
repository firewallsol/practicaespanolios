//
//  LoginEmailVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 05/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Firebase
import PKHUD

class LoginEmailVC: UIViewController, UITextFieldDelegate {
    
    var scrollView : UIScrollView!
    var txtUsuario : UITextField!
    var txtContra  : UITextField!
    var btnEnviar : UIView!
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        //SI EXISTIERA alguna sesion de firebase iniciada la cerramos
        try! FIRAuth.auth()?.signOut()
        
        self.buildInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func buildInterface(){
        self.view.backgroundColor = UIColor.white
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        let index = Functions().getIdiomaIndex()
        self.title = idiomasClass().arrayIdiomas[index].btn_email.uppercased()
        
        var xframe = CGRect.zero
        var posy : CGFloat = 0
        
        let anchoTextField = self.anchoPantalla * 0.9
        let altoTextField = self.anchoPantalla * 0.13
        
        //usuario
        xframe.origin = CGPoint(x: 10, y: 20)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtUsuario = UITextField(frame: xframe)
        self.txtUsuario.placeholder = "Correo electrónico"
        self.txtUsuario.layer.cornerRadius = 3
        self.txtUsuario.layer.borderColor = UIColor.gray.cgColor
        self.txtUsuario.layer.borderWidth = 1
        self.txtUsuario.delegate = self
        self.txtUsuario.keyboardType = .emailAddress
        self.txtUsuario.autocapitalizationType = .none
        self.scrollView.addSubview(self.txtUsuario)
        self.txtUsuario.center.x = self.anchoPantalla / 2
        
        posy = self.txtUsuario.frame.maxY
        
        let paddingView = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtUsuario.frame.height))
        
        self.txtUsuario.leftView = paddingView
        
        self.txtUsuario.leftViewMode = UITextFieldViewMode.always
        
        //contra
        xframe.origin = CGPoint(x: 10, y: posy + 20)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtContra = UITextField(frame: xframe)
        self.txtContra.placeholder = "Correo electrónico"
        self.txtContra.layer.cornerRadius = 3
        self.txtContra.delegate = self
        self.txtContra.layer.borderColor = UIColor.gray.cgColor
        self.txtContra.layer.borderWidth = 1
        self.txtContra.isSecureTextEntry = true
        self.scrollView.addSubview(self.txtContra)
        self.txtContra.center.x = self.anchoPantalla / 2
        
        posy = self.txtContra.frame.maxY
        
        let paddingView2 = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtContra.frame.height))
        
        self.txtContra.leftView = paddingView2
        
        self.txtContra.leftViewMode = UITextFieldViewMode.always
        
        
        //btn enviar
        xframe.origin = CGPoint(x: 10, y: posy + 25)
        xframe.size = CGSize(width: anchoTextField * 0.6, height: altoTextField * 1.2)
        self.btnEnviar = UIView(frame: xframe)
        self.btnEnviar.backgroundColor = UIColor.white
        self.btnEnviar.layer.borderWidth = 0.8
        self.btnEnviar.layer.cornerRadius = 5
        self.btnEnviar.layer.borderColor = Constantes.paletaColores.bordeRojoBotonesLogin.cgColor
        self.scrollView.addSubview(self.btnEnviar)
        self.btnEnviar.center.x = self.anchoPantalla / 2
        let tapEnviar = UITapGestureRecognizer(target: self, action: #selector(self.loginUsuario(sender:)))
        self.btnEnviar.isUserInteractionEnabled = true
        self.btnEnviar.addGestureRecognizer(tapEnviar)
        
        //label
        let lblEnviar = UILabel(frame: xframe)
        lblEnviar.text = "Enviar"
        lblEnviar.textColor = Constantes.paletaColores.bordeRojoBotonesLogin
        lblEnviar.sizeToFit()
        self.btnEnviar.addSubview(lblEnviar)
        lblEnviar.center.x = self.btnEnviar.frame.size.width / 2
        lblEnviar.center.y = self.btnEnviar.frame.size.height / 2
        
        posy = self.btnEnviar.frame.maxY
        
        
        self.scrollView.contentSize.height = posy + self.altoPantalla * 0.25
        
        self.view.addSubview(self.scrollView)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func loginUsuario(sender: UITapGestureRecognizer){
        //antes de enviar nada, validar todos los datos
        if (!self.validaForma()){
            return
        }
        HUD.show(.labeledProgress(title: "", subtitle: "Enviando datos"))
        let correo = self.txtUsuario.text!.lowercased()
        let passwd = self.txtContra.text!
        
        FIRAuth.auth()?.signIn(withEmail: correo, password: passwd, completion: {(user, error) in
            
            if error != nil {
                print(" JSON Error: \(error!) ")
                
                let mensaje = Functions().getFirAuthErrorMessage(elError: error!)
                
                HUD.flash(.labeledError(title: "Error", subtitle: mensaje), delay: 2.0)
                return
            }else{
                print("Login Successful: \(user)")
                HUD.hide()
                //actualizar el perfil con  los datos y la imagen si es que agrego una
                _ = self.navigationController?.popViewController(animated: true)
            }
            
        })

    }
    
    func validaForma()->Bool{
        
        var mensaje = ""
        var donde : CGFloat = 0
        //correo
        if (self.txtUsuario.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""){
            mensaje = "El campo \"Correo electrónico\", es obligatorio"
            donde = self.txtUsuario.frame.origin.y
        }
        
        //contra
        if (mensaje == "" && (self.txtContra.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Contraseña\", es obligatorio"
            donde = self.txtContra.frame.origin.y
        }
        
        if mensaje != "" {
            //self.scrollView.setContentOffset(CGPoint(x: 0, y: donde - 80), animated: true)
            
            HUD.flash(.labeledError(title: "Error", subtitle: mensaje),delay: 2.0)
            
            return false
        }
        
        
        return true
    }

}
