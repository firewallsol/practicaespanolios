//
//  Usuario.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 08/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

class Usuario {
    var nombre: String!
    var apellidos : String!
    var email : String!
    var isLogged : Bool = false
    var loggedBy : String!
}
