//
//  NoConfundas.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 28/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//
class NoConfundas {
    var id : Int!
    var fecha : String!
    var link : String!
    var titulo : String!
    var contenido : String!
    var palabra1 : String!
    var ej1_palabra1 : String!
    var ej2_palabra1 : String!
    var url_audio_palabra1 : String?
    var palabra2 : String!
    var ej1_palabra2 : String!
    var ej2_palabra2 : String!
    var url_audio_palabra2 : String?
    var primera_letra : String!
}
