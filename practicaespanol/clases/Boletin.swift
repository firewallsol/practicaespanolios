//
//  Boletin.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 26/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SwiftyJSON

class Boletin : Noticia {
    
    init(id: Int, fecha: String, link: String, titulo: String, contenido: String, categorias: [Int], ejercicios: JSON = JSON.null, num_ejercicios: Int = 0, url_audio: String = "", url_video: String = "", id_video: String = "", jsonString: String) {
        super.init(id: id, fecha: fecha, link: link, titulo: titulo, contenido: contenido, categorias: categorias, url_imagen: "", ejercicios: ejercicios, num_ejercicios: num_ejercicios, url_audio: url_audio, url_video: url_video, id_video: id_video, jsonString: jsonString)
        
    }
}
