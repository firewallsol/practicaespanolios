//
//  Noticia.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 26/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SwiftyJSON

class Noticia: NSObject {
    var id : Int!
    var fecha : String!
    var link : String!
    var titulo : String!
    var contenido : String!
    var categorias : [Int]!
    var url_imagen : String?
    var ejercicios : JSON?
    var num_ejercicios : Int?
    var url_audio : String?
    var url_video : String?
    var id_video : String?
    var jsonStr : String!
    
    init(id: Int, fecha: String, link: String, titulo: String, contenido: String, categorias: [Int], url_imagen: String = "", ejercicios: JSON = JSON.null, num_ejercicios: Int = 0, url_audio: String = "", url_video: String = "", id_video : String = "", jsonString : String!) {
        self.id = id
        self.fecha = fecha
        self.link = link
        self.titulo = titulo
        self.contenido = contenido
        self.categorias = categorias
        self.url_imagen = url_imagen
        self.ejercicios = ejercicios
        self.num_ejercicios = num_ejercicios
        self.url_audio = url_audio
        self.url_video = url_video
        self.id_video = id_video
        self.jsonStr = jsonString
    }
}
