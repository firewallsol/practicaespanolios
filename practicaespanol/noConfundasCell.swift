//
//  noConfundasCell.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 18/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class noConfundasCell: UITableViewCell {
    
    var viewContenedor : UIView!
    var lblTexto : UILabel!
    var imgVisto : UIImageView!
    
    var anchoCelda : CGFloat = 0
    var altoCelda : CGFloat = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.anchoCelda = self.contentView.frame.size.width
        self.altoCelda = self.contentView.frame.size.height
        self.frame.size.height = 60
        self.selectionStyle = .none
        self.buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildInterface(){
        self.contentView.backgroundColor = UIColor.clear
        
        //content view
        self.viewContenedor = UIView(frame: CGRect(x: 0, y: 0, width: self.anchoCelda * 0.98, height: self.altoCelda * 0.98))
        
        //titulo
        self.lblTexto = UILabel(frame: CGRect(x: 20, y: 10, width: 40, height: 15))
        var fontTitulo = UIFont.boldSystemFont(ofSize: 15)
        if #available(iOS 8.2, *) {
            fontTitulo = UIFont.systemFont(ofSize: 15, weight: UIFontWeightBold)
        }
        self.lblTexto.font = fontTitulo
        self.viewContenedor.addSubview(self.lblTexto)
        
        //img ojo
        self.imgVisto = UIImageView(frame: CGRect(x: 60, y: 20, width: 30, height: 30))
        self.imgVisto.contentMode = .scaleAspectFit
        self.imgVisto.image = UIImage(named: "eye_negro")
        self.viewContenedor.addSubview(self.imgVisto)
        
        self.contentView.addSubview(self.viewContenedor)
    }

}
