//
//  FavoritosVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 10/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
import SideMenu
import PKHUD

class FavoritosVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    
    var favoritosData = [Noticia]()
    
    var tableFavoritos : UITableView!
    
    var celdaSeleccionada : IndexPath!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        
        self.buildInterface()
        
        self.setNavBar()
        
        self.getData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let index = Functions().getIdiomaIndex()
        
        self.title = idiomasClass().arrayIdiomas[index].menu_favoritos.uppercased()
        if self.celdaSeleccionada != nil {
            self.getData()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func buildInterface(){
        
        self.tableFavoritos = UITableView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        self.view.addSubview(self.tableFavoritos)
        self.tableFavoritos.dataSource = self
        self.tableFavoritos.delegate = self
        self.tableFavoritos.register(noticiasCell.self, forCellReuseIdentifier: "noticiasCell")
        self.tableFavoritos.rowHeight = 150
        self.tableFavoritos.separatorStyle = .none
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
        
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func getData(){
        let favData = Functions().getDicFavoritos()
        
        if favData.count > 0 {
            for (_, strJson) in favData {
                let jsonNoticia = JSON(parseJSON: strJson)
                self.favoritosData.append(Functions().JSONtoNoticia(elJson: jsonNoticia))
            }
            
            var insets = self.tableFavoritos.contentInset
            insets.bottom += self.altoPantalla * 0.25
            self.tableFavoritos.contentInset = insets
            
            self.tableFavoritos.reloadData()
        } else {
            HUD.flash(.label("No hay noticias en Favoritos"), delay: 2.0, completion: nil)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.favoritosData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableFavoritos.dequeueReusableCell(withIdentifier: "noticiasCell")! as! noticiasCell
        
        cell.frame.size.width = self.anchoPantalla
        
        //if self.dataLoaded {
            //img
            
            var frameVar = cell.imgFondo.frame
            frameVar.origin = CGPoint(x: 10, y: 10)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.98, height: cell.frame.size.height * 0.97)
            cell.imgFondo.frame = frameVar
            
            cell.viewOpacoImg.frame = cell.imgFondo.frame
            
            cell.imgFondo.center.x = cell.frame.size.width / 2
            cell.imgFondo.center.y = cell.frame.size.height / 2
            cell.viewOpacoImg.center = cell.imgFondo.center
            
            if (self.favoritosData[indexPath.row].url_imagen != ""){
                let imgurl = URL(string: self.favoritosData[indexPath.row].url_imagen!)
                cell.imgFondo.kf.setImage(with: imgurl, placeholder: UIImage(named: "placeHolderImagenGris"))
            } else {
                cell.imgFondo.image = UIImage(named: "placeHolderImagenGris")
            }
            //fin imagen
            
            //titulo
            cell.lblTitulo.text = self.favoritosData[indexPath.row].titulo
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: 10, y: 10)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.9, height: 30)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            var posy = cell.frame.size.height - (cell.lblTitulo.frame.size.height + 10)
            frameVar = cell.lblTitulo.frame
            frameVar.origin = CGPoint(x: 10, y: posy)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.9, height: frameVar.size.height)
            cell.lblTitulo.frame = frameVar
            cell.lblTitulo.sizeToFit()
            // fin titulo
            
            //seccion
            let strSeccion = Functions().getSeccionNoticia(arr_ids: self.favoritosData[indexPath.row].categorias)
            cell.lblSeccion.text = strSeccion
            cell.lblSeccion.sizeToFit()
            frameVar = cell.lblSeccion.frame
            posy = cell.lblTitulo.frame.minY - (cell.lblSeccion.frame.size.height + 5)
            frameVar.origin = CGPoint(x:10, y:posy)
            cell.lblSeccion.frame = frameVar
            //fin seccion
            
            //dificultad
            frameVar = cell.viewDificultad.frame
            //temp
            //let posYDif = cell.lblSeccion.frame.minY - (cell.frame.size.width * 0.08) - 10
            
            let posYDif : CGFloat = 5
            
            frameVar.origin = CGPoint(x: cell.lblSeccion.frame.minX, y: posYDif)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.08, height: cell.frame.size.width * 0.08)
            cell.viewDificultad.frame = frameVar
            cell.viewDificultad.layer.cornerRadius = cell.viewDificultad.frame.size.width / 2
            cell.viewDificultad.clipsToBounds = true
            //se comenta, probablemente seria centrado en X respecto a label seccion
            //cell.viewDificultad.center.y = cell.lblSeccion.frame.minY + cell.lblSeccion.frame.size.height / 2
            
            // label dificultad
            let strDif = Functions().getNivelDificultad(arr_ids: self.favoritosData[indexPath.row].categorias)
            let elColor = Functions().getColorDificultad(tipo: strDif)
            cell.viewDificultad.backgroundColor = elColor
            cell.lblDificultad.text = strDif
            cell.lblDificultad.sizeToFit()
            cell.lblDificultad.center.x = cell.viewDificultad.frame.size.width / 2
            cell.lblDificultad.center.y = cell.viewDificultad.frame.size.height / 2
            
            //fin dificultad
            
            //img visto
            frameVar = cell.imgVisto.frame
            frameVar.origin = CGPoint(x: cell.viewDificultad.frame.maxX + 10, y: cell.viewDificultad.frame.minY)
            cell.imgVisto.frame = frameVar
            cell.imgVisto.center.y = cell.viewDificultad.frame.minY + cell.viewDificultad.frame.size.height / 2
            if (!Functions().existeIdEnVisto(id_nota: self.favoritosData[indexPath.row].id)){
                
                cell.imgVisto.isHidden = true
            } else {
                cell.imgVisto.isHidden = false
            }
            //fin img visto
            
            //img play
            frameVar = cell.btnPlay.frame
            frameVar.size = CGSize(width: cell.frame.size.width * 0.11, height: cell.frame.size.width * 0.11)
            cell.btnPlay.frame = frameVar
            let posxbtnplay = cell.frame.size.width - ( cell.btnPlay.frame.size.width + 10 )
            frameVar = cell.btnPlay.frame
            frameVar.origin = CGPoint(x: posxbtnplay, y: 50)
            cell.btnPlay.frame = frameVar
            cell.btnPlay.tag = indexPath.row
            cell.btnPlay.center.y = cell.frame.size.height / 2
            //let tap = UITapGestureRecognizer(target: self, action: #selector(self.playItem(sender:)))
            //cell.btnPlay.isUserInteractionEnabled = true
            //cell.btnPlay.addGestureRecognizer(tap)
            //fin img play
            
            
        //}
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detalleNoticiaVC = storyboard.instantiateViewController(withIdentifier: "detalleNotaVC") as! detalleNotaVC
        detalleNoticiaVC.dataNota = self.favoritosData[indexPath.row]
        detalleNoticiaVC.offsetAlto = (self.altoPantalla * 0.1)
        self.navigationController?.pushViewController(detalleNoticiaVC, animated: true)
        self.celdaSeleccionada = indexPath
    }

}
