//
//  ContainerPageView.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 04/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import SideMenu

class ContainerPageView: UIViewController {
    
    //interfaz
    var viewNoticias : UIView!
    var viewBoletin : UIView!
    var viewLaVoz : UIView!
    var labelNoticias : UILabel!
    var labelBoletin : UILabel!
    var labelLaVoz : UILabel!
    var containerView : UIView!
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    var altoNavBar : CGFloat = 0
    var altoStatusBar : CGFloat = 0
    
    var toolBarHeight : CGFloat = 0
    
    var xfunciones = Functions()
    
    let notifSetNumCat = Notification.Name("setNumCat")
    let notifCambiaMarcador = Notification.Name("setMarcador")
    let notifCambiaPagina = Notification.Name("cambiaPagina")
    let notifFiltroVozN = Notification.Name("aplicaFiltroVozNoticia")
    let notifFiltroBoletin = Notification.Name("aplicaFiltroBoletin")
    
    var strCategoriaActual = "noticias"
    var nivelActual = ""
    var vistoOnOff = false
    var vistoOnOffLaVoz = false
    var vistoOnOffBoletin = false
    var screenActual = 1 //el tag del view en el page flip 1- noticias 2 la voz, 3 boletin

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.setNumCategoria(notification:)), name: self.notifSetNumCat, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.obsCambiaMarcador(notification:)), name: self.notifCambiaMarcador, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDatosFiltroLaVoz), name: self.notifFiltroVozN, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDatosFiltroBoletin), name: self.notifFiltroBoletin, object: nil)
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        self.altoNavBar = (self.navigationController?.navigationBar.frame.size.height)!
        self.altoStatusBar = self.xfunciones.statusBarHeight()

        self.setNavBar()
        self.buildInterface()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.navigationItem.title = " "
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setLabelsText()
    }
    
    func setLabelsText(){
        let index = Functions().getIdiomaIndex()
        self.labelNoticias.text = idiomasClass().arrayIdiomas[index].menu_noticias
        self.labelLaVoz.text = idiomasClass().arrayIdiomas[index].menu_la_voz
        self.labelBoletin.text = idiomasClass().arrayIdiomas[index].menu_boletin
    }
    
    func setNumCategoria(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        self.strCategoriaActual = paramFiltro["categoria"] as! String
        self.nivelActual = paramFiltro["nivel"] as! String
        self.vistoOnOff = paramFiltro["visto"] as! Bool
    }
    
    func getDatosFiltroLaVoz(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        self.vistoOnOffLaVoz = paramFiltro["visto"] as! Bool
    }
    
    func getDatosFiltroBoletin(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        self.vistoOnOffBoletin = paramFiltro["visto"] as! Bool
    }
    
    func setNavBar(){
        
        self.toolBarHeight = self.view.frame.size.height * 0.1
        
        let logo = UIImage(named: "cabecera-app")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 80, height: 40))
        imageView.image = logo
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        //filtro
        let imgFiltro = UIImage(named: "filter2")
        let filterButton = UIBarButtonItem(image: imgFiltro, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.filterButtonTap))
        
        self.navigationItem.rightBarButtonItem = filterButton
        
        self.navigationController?.navigationBar.barStyle = UIBarStyle.default
        navigationController?.navigationBar.barTintColor = Constantes.paletaColores.rojoNavBar
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.toolbar.barTintColor = UIColor.black
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
        
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func filterButtonTap(){
        switch self.screenActual {
        case 1:
            let vcFiltro = FiltroVC()
            vcFiltro.strCategoriaActual = self.strCategoriaActual
            vcFiltro.strNivelActual = self.nivelActual
            vcFiltro.vistoOnOff = self.vistoOnOff
            vcFiltro.buildInterface()
            self.navigationController?.pushViewController(vcFiltro, animated: true)
            break
        case 2:
            let vcFiltro = filtroVistoNoVistoVC()
            vcFiltro.vistoOnOff = self.vistoOnOffLaVoz
            vcFiltro.sender = "vozNoticia"
            vcFiltro.buildInterface()
            self.navigationController?.pushViewController(vcFiltro, animated: true)
            break
        case 3:
            let vcFiltro = filtroVistoNoVistoVC()
            vcFiltro.vistoOnOff = self.vistoOnOffBoletin
            vcFiltro.sender = "boletin"
            vcFiltro.buildInterface()
            self.navigationController?.pushViewController(vcFiltro, animated: true)
            break
        default:
            break
        }
        
    }
    
    func buildInterface(){
        
        let widthBoton = self.anchoPantalla / 3
        let altoBoton = self.altoPantalla * 0.09
        
        //noticias
        self.viewNoticias = UIView(frame: CGRect(x: 0, y: self.altoNavBar + self.altoStatusBar, width: widthBoton, height: altoBoton))
        self.viewNoticias.backgroundColor = UIColor.white
        self.viewNoticias.layer.borderColor = UIColor.lightGray.cgColor
        self.viewNoticias.layer.borderWidth = 0.5
        self.viewNoticias.tag = 1
        self.viewNoticias.isUserInteractionEnabled = true
        let tapNoticias = UITapGestureRecognizer(target: self, action: #selector(self.tapCabecera(sender:)))
        self.viewNoticias.addGestureRecognizer(tapNoticias)
        self.view.addSubview(self.viewNoticias)
        //label
        self.labelNoticias = UILabel(frame: CGRect(x: 10, y: 10, width: widthBoton, height: altoBoton))
        self.labelNoticias.text = "Noticias"
        self.labelNoticias.textAlignment = .center
        var lightFont = UIFont.systemFont(ofSize: labelNoticias.font.pointSize - 2)
        if #available(iOS 8.2, *) {
            lightFont = UIFont.systemFont(ofSize: labelNoticias.font.pointSize - 2, weight: UIFontWeightLight)
        }
        self.labelNoticias.font = lightFont
        //self.labelNoticias.sizeToFit()
        self.labelNoticias.textColor = UIColor.black
        self.labelNoticias.center.x = self.viewNoticias.frame.size.width / 2
        self.labelNoticias.center.y = self.viewNoticias.frame.size.height / 2
        self.viewNoticias.addSubview(self.labelNoticias)
        
        //la voz
        self.viewLaVoz = UIView(frame: CGRect(x: self.viewNoticias.frame.maxX, y: self.altoNavBar + self.altoStatusBar, width: widthBoton, height: altoBoton))
        self.viewLaVoz.backgroundColor = Constantes.paletaColores.grisFondo
        self.viewLaVoz.layer.borderColor = UIColor.darkGray.cgColor
        self.viewLaVoz.layer.borderWidth = 0.5
        self.viewLaVoz.tag = 2
        self.viewLaVoz.isUserInteractionEnabled = true
        let tapLaVoz = UITapGestureRecognizer(target: self, action: #selector(self.tapCabecera(sender:)))
        self.viewLaVoz.addGestureRecognizer(tapLaVoz)
        self.view.addSubview(self.viewLaVoz)
        
        //label
        self.labelLaVoz = UILabel(frame: CGRect(x: 10, y: 10, width: widthBoton, height: altoBoton))
        self.labelLaVoz.text = "La voz"
        //self.labelLaVoz.sizeToFit()
        self.labelLaVoz.textAlignment = .center
        self.labelLaVoz.textColor = UIColor.black
        self.labelLaVoz.font = lightFont
        self.labelLaVoz.center.x = self.viewLaVoz.frame.size.width / 2
        self.labelLaVoz.center.y = self.viewLaVoz.frame.size.height / 2
        self.viewLaVoz.addSubview(self.labelLaVoz)
        
        //boletin
        self.viewBoletin = UIView(frame: CGRect(x: self.viewLaVoz.frame.maxX, y: self.altoNavBar + self.altoStatusBar, width: widthBoton, height: altoBoton))
        self.viewBoletin.backgroundColor = Constantes.paletaColores.grisFondo
        self.viewBoletin.layer.borderColor = UIColor.darkGray.cgColor
        self.viewBoletin.layer.borderWidth = 0.5
        self.viewBoletin.tag = 3
        self.viewBoletin.isUserInteractionEnabled = true
        let tapBoletin = UITapGestureRecognizer(target: self, action: #selector(self.tapCabecera(sender:)))
        self.viewBoletin.addGestureRecognizer(tapBoletin)
        self.view.addSubview(self.viewBoletin)
        
        //label
        self.labelBoletin = UILabel(frame: CGRect(x: 10, y: 10, width: widthBoton, height: altoBoton))
        self.labelBoletin.text = "Boletín"
        self.labelBoletin.textAlignment = .center
        //self.labelBoletin.sizeToFit()
        self.labelBoletin.textColor = UIColor.black
        self.labelBoletin.font = lightFont
        self.labelBoletin.center.x = self.viewBoletin.frame.size.width / 2
        self.labelBoletin.center.y = self.viewBoletin.frame.size.height / 2
        self.viewBoletin.addSubview(self.labelBoletin)
        
        let altoContainer  = self.altoPantalla - (self.altoNavBar + self.altoStatusBar + self.viewNoticias.frame.size.height)
        
        self.containerView = UIView(frame: CGRect(x: 0, y: self.viewNoticias.frame.maxY, width: self.anchoPantalla, height: altoContainer))
        self.containerView.backgroundColor = UIColor.white
        self.view.addSubview(self.containerView)
        
        let pageController = storyboard!.instantiateViewController(withIdentifier: "PrincipalPageC") as! PrincipalPageC
        pageController.offsetAlto = (self.altoNavBar + self.altoStatusBar + self.viewNoticias.frame.size.height + self.toolBarHeight)
        addChildViewController(pageController)
        var frameVar = pageController.view.frame
        frameVar.size = CGSize(width: self.containerView.frame.size.width, height: altoContainer)
        
        pageController.view.frame = frameVar
        pageController.loadViews()
        self.containerView.addSubview(pageController.view)
    }
    
    func tapCabecera(sender: UITapGestureRecognizer){
        self.screenActual = (sender.view?.tag)!
        self.cambiaMarcador()
        
        let paramPagina = [ "pagina": self.screenActual - 1] as NSDictionary
        NotificationCenter.default.post(name: self.notifCambiaPagina, object: paramPagina)
    }
    
    func obsCambiaMarcador(notification: NSNotification){
        let paramMarcador = notification.object as! NSDictionary
        self.screenActual = paramMarcador["pagina"] as! Int
        
        self.cambiaMarcador()
    }
    
    func cambiaMarcador(){
        
        switch self.screenActual {
        case 1: //noticias
            UIView.animate(withDuration: 0.5, animations: {
                
                self.viewNoticias.backgroundColor = UIColor.white
                self.viewNoticias.layer.borderColor = Constantes.paletaColores.grisFondo.cgColor
                self.viewNoticias.layer.borderWidth = 0.5
                
                self.viewLaVoz.backgroundColor = Constantes.paletaColores.grisFondo
                self.viewLaVoz.layer.borderColor = UIColor.darkGray.cgColor
                self.viewLaVoz.layer.borderWidth = 0.5
                
                self.viewBoletin.backgroundColor = Constantes.paletaColores.grisFondo
                self.viewBoletin.layer.borderColor = UIColor.darkGray.cgColor
                self.viewBoletin.layer.borderWidth = 0.5
                
            }, completion: nil)
            break
        case 2: //la voz
            
            self.viewNoticias.backgroundColor = Constantes.paletaColores.grisFondo
            self.viewNoticias.layer.borderColor = UIColor.darkGray.cgColor
            self.viewNoticias.layer.borderWidth = 0.5
            
            self.viewLaVoz.backgroundColor = UIColor.white
            self.viewLaVoz.layer.borderColor = Constantes.paletaColores.grisFondo.cgColor
            self.viewLaVoz.layer.borderWidth = 0.5
            
            self.viewBoletin.backgroundColor = Constantes.paletaColores.grisFondo
            self.viewBoletin.layer.borderColor = UIColor.darkGray.cgColor
            self.viewBoletin.layer.borderWidth = 0.5
            break
        case 3: //boletin
            UIView.animate(withDuration: 0.5, animations: {
                
                self.viewNoticias.backgroundColor = Constantes.paletaColores.grisFondo
                self.viewNoticias.layer.borderColor = UIColor.darkGray.cgColor
                self.viewNoticias.layer.borderWidth = 0.5
                
                self.viewLaVoz.backgroundColor = Constantes.paletaColores.grisFondo
                self.viewLaVoz.layer.borderColor = UIColor.darkGray.cgColor
                self.viewLaVoz.layer.borderWidth = 0.5
                
                self.viewBoletin.backgroundColor = UIColor.white
                self.viewBoletin.layer.borderColor = Constantes.paletaColores.grisFondo.cgColor
                self.viewBoletin.layer.borderWidth = 0.5
                
            }, completion: nil)
            break
        default:
            break
        }
        
    }

}
