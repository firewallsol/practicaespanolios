//
//  IdiomasStrings.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 22/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import Foundation

class idiomasClass {
    var arrayIdiomas = [idiomasStrings]()
    
    //strings español
    let stringsEspanol = idiomasStrings(
        menu_noticias: "Noticias",
        menu_no_confundas: "No confundas",
        menu_la_voz: "La voz",
        menu_boletin: "Boletín",
        menu_perfil_notif: "Perfil/Notificaciones",
        menu_favoritos: "Favoritos",
        menu_idiomas: "Idiomas",
        menu_quienes_somos: "Quiénes somos",
        menu_privacidad: "Privacidad",
        filtro_nivel: "Nivel",
        filtro_categoria: "Categoría",
        filtro_visto: "Visto",
        msg_guardado_favoritos: "Noticia guardada en favoritos",
        msg_guardado_visto: "Noticia guardada como vista",
        msg_inicio_sesion_ejercicios: "Para realizar los ejercicios debes iniciar sesión",
        msg_inicio_sesion_favoritos: "Para acceder a esta sección debes iniciar sesión",
        btn_registrarse: "Registrarse",
        btn_facebook: "Entrar con Facebook",
        btn_email: "Entrar con mail"
    )
    
    //strings ingles
    let stringsIngles = idiomasStrings(
        menu_noticias : "News",
        menu_no_confundas : "Don't confuse",
        menu_la_voz : "The voice",
        menu_boletin : "Newsletter",
        menu_perfil_notif : "Profile/ Notifications",
        menu_favoritos : "Favourites",
        menu_idiomas : "Languages",
        menu_quienes_somos : "About us",
        menu_privacidad : "Privacy",
        filtro_nivel : "Level",
        filtro_categoria : "Categories",
        filtro_visto : "Viewed",
        msg_guardado_favoritos : "News saved in favourites",
        msg_guardado_visto : "News saved as viewed",
        msg_inicio_sesion_ejercicios : "To do the exercises you must log in",
        msg_inicio_sesion_favoritos : "To access this section you must log in",
        btn_registrarse : "To log in",
        btn_facebook : "Login with Facebook",
        btn_email : "Login with email"
    )
    
    //strings portugues
    let stringsPortugues = idiomasStrings(
        menu_noticias : "Notícias",
        menu_no_confundas : "Não confundas",
        menu_la_voz : "A voz",
        menu_boletin : "Boletim",
        menu_perfil_notif : "Perfil/Notificações",
        menu_favoritos : "Favoritos",
        menu_idiomas : "Idiomas",
        menu_quienes_somos : "Quem somos",
        menu_privacidad : "Privacidade",
        filtro_nivel : "Nível",
        filtro_categoria : "Categorias",
        filtro_visto : "Visto",
        msg_guardado_favoritos : "News saved in favourites",
        msg_guardado_visto : "News saved as viewed",
        msg_inicio_sesion_ejercicios : "Para realizar os exercícios deves iniciar sessão",
        msg_inicio_sesion_favoritos : "Para aceder a esta secção deves iniciar sessão",
        btn_registrarse : "Registar-se",
        btn_facebook : "Entrar com o Facebook",
        btn_email : "Entrar com o mail"
    )
    
    //strings frances
    let stringsFrances = idiomasStrings(
        menu_noticias : "Nouvelles",
        menu_no_confundas : "Pas confondre",
        menu_la_voz : "La voix",
        menu_boletin : "Journal",
        menu_perfil_notif : "Profil/Notifications",
        menu_favoritos : "Favoris",
        menu_idiomas : "Langues",
        menu_quienes_somos : "Qui sommes-nous",
        menu_privacidad : "Confidentialité",
        filtro_nivel : "Niveau",
        filtro_categoria : "Catégories",
        filtro_visto : "Vu",
        msg_guardado_favoritos : "Nouvelle enregistré​e dans ​favoris",
        msg_guardado_visto : "Information enregistrée",
        msg_inicio_sesion_ejercicios : "Pour effectuer les exercices vous devez connecteur",
        msg_inicio_sesion_favoritos : "Pour accéder à cette section, vous devez connecter",
        btn_registrarse : "Inscription",
        btn_facebook : "Entrez Facebook",
        btn_email : "Entrez email"
    )
    
    //strings aleman
    let stringsAleman = idiomasStrings(
        menu_noticias : "Nachrichten",
        menu_no_confundas : "Nicht verwechseln",
        menu_la_voz : "Die Stimme",
        menu_boletin : "Newsletter",
        menu_perfil_notif : "Profil / Mitteilungen",
        menu_favoritos : "Favoriten",
        menu_idiomas : "Sprachen",
        menu_quienes_somos : "Über uns",
        menu_privacidad : "Datenschutz-Bestimmungen",
        filtro_nivel : "Niveau",
        filtro_categoria : "Kategorien",
        filtro_visto : "Gesehen",
        msg_guardado_favoritos : "Nachricht unter Favoriten gespeichert",
        msg_guardado_visto : "Nachricht als gelesen gespeichert",
        msg_inicio_sesion_ejercicios : "Um die Übungen durchzuführen, musst du dich anmelden",
        msg_inicio_sesion_favoritos : "Um Zugang zu diesem Bereich zu haben, musst du dich anmelden",
        btn_registrarse : "Registrierung",
        btn_facebook : "Über Facebook anmelden",
        btn_email : "Über Mail anmelden"
    )
    
    //strings chino
    let stringsChino = idiomasStrings(
        menu_noticias : "新闻",
        menu_no_confundas : "注意别混淆",
        menu_la_voz : "声音",
        menu_boletin : "Newsletter",
        menu_perfil_notif : "头像/通知",
        menu_favoritos : "最爱",
        menu_idiomas : "语言",
        menu_quienes_somos : "关于我们",
        menu_privacidad : "隐私",
        filtro_nivel : "Level",
        filtro_categoria : "Categories",
        filtro_visto : "Viewed",
        msg_guardado_favoritos : "News saved in favourites",
        msg_guardado_visto : "News saved as viewed",
        msg_inicio_sesion_ejercicios : "To do the exercises you must log in",
        msg_inicio_sesion_favoritos : "To access this section you must log in",
        btn_registrarse : "To log in",
        btn_facebook : "Login with Facebook",
        btn_email : "Login with email"
    )
    
    init() {
        self.arrayIdiomas.append(self.stringsEspanol)
        self.arrayIdiomas.append(self.stringsIngles)
        self.arrayIdiomas.append(self.stringsPortugues)
        self.arrayIdiomas.append(self.stringsFrances)
        self.arrayIdiomas.append(self.stringsAleman)
        self.arrayIdiomas.append(self.stringsChino)
        
    }
}

struct idiomasStrings {
        var menu_noticias = ""
        var menu_no_confundas = ""
        var menu_la_voz = ""
        var menu_boletin = ""
        var menu_perfil_notif = ""
        var menu_favoritos = ""
        var menu_idiomas = ""
        var menu_quienes_somos = ""
        var menu_privacidad = ""
        var filtro_nivel = ""
        var filtro_categoria = ""
        var filtro_visto = ""
        var msg_guardado_favoritos = ""
        var msg_guardado_visto = ""
        var msg_inicio_sesion_ejercicios = ""
        var msg_inicio_sesion_favoritos = ""
        var btn_registrarse = ""
        var btn_facebook = ""
        var btn_email = ""
}
