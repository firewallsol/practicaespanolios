//
//  RegistroUsuarioVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 05/05/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import DLRadioButton
import Firebase
import PKHUD

class RegistroUsuarioVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var scrollView : UIScrollView!
    var imageProfile : UIImageView!
    var txtNombreUsr : UITextField!
    var txtApellidosUsr : UITextField!
    var txtEmailUsr : UITextField!
    var txtContraUsr : UITextField!
    var txtContraConfUsr : UITextField!
    var btnEnviar : UIView!
    var checkBtn : DLRadioButton!
    
    let imagePicker = UIImagePickerController()
    
    var altoPantalla : CGFloat = 0
    var anchoPantalla : CGFloat = 0
    
    var xfunciones = Functions()
    
    var imagenPerfil = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.altoPantalla = self.view.frame.size.height
        self.anchoPantalla = self.view.frame.size.width

        self.buildInterface()
        
        self.imagePicker.delegate = self
        
        //SI EXISTIERA alguna sesion de firebase iniciada la cerramos
        try! FIRAuth.auth()?.signOut()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func keyboardWillShow(sender: NSNotification) {
        var userInfo = sender.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        self.scrollView.contentInset = contentInset
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInset
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func buildInterface(){
        let index = Functions().getIdiomaIndex()
        self.title = idiomasClass().arrayIdiomas[index].btn_registrarse.uppercased()
        self.view.backgroundColor = UIColor.white
        
        self.scrollView = UIScrollView(frame: self.view.frame)
        
        var xframe = CGRect.zero
        var posy : CGFloat = 0
        
        //imagen perfil
        xframe.origin = CGPoint(x: 10, y: 10)
        xframe.size = CGSize(width: self.anchoPantalla * 0.3, height: self.anchoPantalla * 0.3)
        self.imageProfile = UIImageView(frame: xframe)
        self.imageProfile.image = UIImage(named: "profile01")
        self.imageProfile.contentMode = .scaleAspectFill
        self.imageProfile.layer.cornerRadius = self.imageProfile.frame.size.width / 2
        self.imageProfile.layer.borderWidth = 1
        self.imageProfile.layer.borderColor = UIColor.black.cgColor
        self.imageProfile.clipsToBounds = true
        self.scrollView.addSubview(self.imageProfile)
        self.imageProfile.center.x = self.anchoPantalla / 2
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(self.abreGaleria(sender:)))
        self.imageProfile.isUserInteractionEnabled = true
        self.imageProfile.addGestureRecognizer(tapImage)
        
        posy = self.imageProfile.frame.maxY
        
        let anchoTextField = self.anchoPantalla * 0.9
        let altoTextField = self.anchoPantalla * 0.13
        
        //nombre
        xframe.origin = CGPoint(x: 10, y: posy + 20)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtNombreUsr = UITextField(frame: xframe)
        self.txtNombreUsr.placeholder = "Nombre"
        self.txtNombreUsr.delegate = self
        self.txtNombreUsr.layer.cornerRadius = 3
        self.txtNombreUsr.layer.borderColor = UIColor.gray.cgColor
        self.txtNombreUsr.layer.borderWidth = 1
        self.txtNombreUsr.keyboardType = .alphabet
        self.txtNombreUsr.returnKeyType = .next
        self.scrollView.addSubview(self.txtNombreUsr)
        self.txtNombreUsr.center.x = self.anchoPantalla / 2
        self.txtNombreUsr.tag = 1
        
        let paddingView = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtNombreUsr.frame.height))
        
        self.txtNombreUsr.leftView = paddingView
        
        self.txtNombreUsr.leftViewMode = UITextFieldViewMode.always
        
        posy = self.txtNombreUsr.frame.maxY
        
        //apellidos
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtApellidosUsr = UITextField(frame: xframe)
        self.txtApellidosUsr.placeholder = "Apellidos"
        self.txtApellidosUsr.delegate = self
        self.txtApellidosUsr.layer.cornerRadius = 3
        self.txtApellidosUsr.layer.borderColor = UIColor.gray.cgColor
        self.txtApellidosUsr.layer.borderWidth = 1
        self.scrollView.addSubview(self.txtApellidosUsr)
        self.txtApellidosUsr.center.x = self.anchoPantalla / 2
        let paddingView2 = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtApellidosUsr.frame.height))
        self.txtApellidosUsr.leftView = paddingView2
        self.txtApellidosUsr.leftViewMode = UITextFieldViewMode.always
        self.txtApellidosUsr.keyboardType = .alphabet
        self.txtApellidosUsr.returnKeyType = .next
        self.txtApellidosUsr.tag = 2
        
        posy = self.txtApellidosUsr.frame.maxY
        
        //correo electronico
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtEmailUsr = UITextField(frame: xframe)
        self.txtEmailUsr.placeholder = "Correo electrónico"
        self.txtEmailUsr.layer.cornerRadius = 3
        self.txtEmailUsr.delegate = self
        self.txtEmailUsr.layer.borderColor = UIColor.gray.cgColor
        self.txtEmailUsr.layer.borderWidth = 1
        self.txtEmailUsr.keyboardType = .emailAddress
        self.txtEmailUsr.returnKeyType = .next
        self.txtEmailUsr.autocapitalizationType = .none
        self.scrollView.addSubview(self.txtEmailUsr)
        self.txtEmailUsr.center.x = self.anchoPantalla / 2
        let paddingView3 = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtEmailUsr.frame.height))
        self.txtEmailUsr.leftView = paddingView3
        self.txtEmailUsr.leftViewMode = UITextFieldViewMode.always
        
        posy = self.txtEmailUsr.frame.maxY
        
        //contraseña
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtContraUsr = UITextField(frame: xframe)
        self.txtContraUsr.placeholder = "Contraseña"
        self.txtContraUsr.delegate = self
        self.txtContraUsr.layer.cornerRadius = 3
        self.txtContraUsr.layer.borderColor = UIColor.gray.cgColor
        self.txtContraUsr.layer.borderWidth = 1
        self.txtContraUsr.isSecureTextEntry = true
        self.scrollView.addSubview(self.txtContraUsr)
        self.txtContraUsr.center.x = self.anchoPantalla / 2
        let paddingView4 = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtContraUsr.frame.height))
        self.txtContraUsr.leftView = paddingView4
        self.txtContraUsr.leftViewMode = UITextFieldViewMode.always
        self.txtContraUsr.returnKeyType = .next
        
        posy = self.txtContraUsr.frame.maxY
        
        //confirmar contraseña
        xframe.origin = CGPoint(x: 10, y: posy + 10)
        xframe.size = CGSize(width: anchoTextField, height: altoTextField)
        self.txtContraConfUsr = UITextField(frame: xframe)
        self.txtContraConfUsr.placeholder = "Repetir contraseña"
        self.txtContraConfUsr.delegate = self
        self.txtContraConfUsr.layer.cornerRadius = 3
        self.txtContraConfUsr.layer.borderColor = UIColor.gray.cgColor
        self.txtContraConfUsr.layer.borderWidth = 1
        self.txtContraConfUsr.isSecureTextEntry = true
        self.scrollView.addSubview(self.txtContraConfUsr)
        self.txtContraConfUsr.center.x = self.anchoPantalla / 2
        let paddingView5 = UIView(frame: CGRect(x:0, y:0, width:15, height:self.txtContraConfUsr.frame.height))
        self.txtContraConfUsr.leftView = paddingView5
        self.txtContraConfUsr.leftViewMode = UITextFieldViewMode.always
        self.txtContraConfUsr.returnKeyType = .done
        
        posy = self.txtContraConfUsr.frame.maxY
        let posx = self.txtContraConfUsr.frame.minX
        //privacidad
        //check
        xframe.origin = CGPoint(x: posx, y: posy + 20)
        xframe.size = CGSize(width: self.anchoPantalla * 0.9, height: 35)
        self.checkBtn = DLRadioButton(frame: xframe)
        self.checkBtn.iconColor = UIColor.red
        self.checkBtn.indicatorColor = UIColor.red
        self.checkBtn.setTitle(" Acepto la política de privacidad de la agencia EFE", for: .normal)
        self.checkBtn.titleLabel?.numberOfLines = 2
        self.checkBtn.setTitleColor(UIColor.gray, for: .normal)
        self.checkBtn.contentHorizontalAlignment = .left
        self.checkBtn.isIconSquare = true
        
        self.scrollView.addSubview(self.checkBtn)
        
        posy = self.checkBtn.frame.maxY
        
        //btn enviar
        xframe.origin = CGPoint(x: 10, y: posy + 25)
        xframe.size = CGSize(width: anchoTextField * 0.6, height: altoTextField * 1.2)
        self.btnEnviar = UIView(frame: xframe)
        self.btnEnviar.backgroundColor = UIColor.white
        self.btnEnviar.layer.borderWidth = 0.8
        self.btnEnviar.layer.cornerRadius = 5
        self.btnEnviar.layer.borderColor = Constantes.paletaColores.bordeRojoBotonesLogin.cgColor
        self.scrollView.addSubview(self.btnEnviar)
        self.btnEnviar.center.x = self.anchoPantalla / 2
        let tapEnviar = UITapGestureRecognizer(target: self, action: #selector(self.enviarDatos))
        self.btnEnviar.isUserInteractionEnabled = true
        self.btnEnviar.addGestureRecognizer(tapEnviar)
        
        //label
        let lblEnviar = UILabel(frame: xframe)
        lblEnviar.text = "Enviar"
        lblEnviar.textColor = Constantes.paletaColores.bordeRojoBotonesLogin
        lblEnviar.sizeToFit()
        self.btnEnviar.addSubview(lblEnviar)
        lblEnviar.center.x = self.btnEnviar.frame.size.width / 2
        lblEnviar.center.y = self.btnEnviar.frame.size.height / 2
        
        posy = self.btnEnviar.frame.maxY
        
        self.scrollView.contentSize.height = posy + 5 + self.altoPantalla * 0.25
        
        self.view.addSubview(self.scrollView)
    }
    
    func validaForma()->Bool{
        
        var mensaje = ""
        var donde : CGFloat = 0
        //nombre
        if (self.txtNombreUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""){
            mensaje = "El campo \"Nombre\", es obligatorio"
            donde = self.txtNombreUsr.frame.origin.y
        }
        //apellidos
        if (mensaje == "" && (self.txtApellidosUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Apellidos\", es obligatorio"
            donde = self.txtApellidosUsr.frame.origin.y
        }
        //correo
        if (mensaje == "" && (self.txtEmailUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Correo electrónico\", es obligatorio"
            donde = self.txtEmailUsr.frame.origin.y
        }
        //contra
        if (mensaje == "" && (self.txtContraUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "")){
            mensaje = "El campo \"Contraseña\", es obligatorio"
            donde = self.txtContraUsr.frame.origin.y
        }
        
        //contra
        if (mensaje == "" && ((self.txtContraUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count)! < 5 )){
            mensaje = "El campo \"Contraseña\", requiere al menos 5 caracteres"
            donde = self.txtContraUsr.frame.origin.y
        }
        
        //conf contraseña
        if (mensaje == "" && (self.txtContraUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) != self.txtContraConfUsr.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))){
            mensaje = "Las contraseñas no coinciden"
            donde = self.txtContraUsr.frame.origin.y
        }
        //privacidad
        
        if (mensaje == "" && (!self.checkBtn.isSelected)){
            mensaje = "Debe aceptar la política de privacidad"
            donde = self.checkBtn.frame.origin.y
        }
        
        if mensaje != "" {
            self.scrollView.setContentOffset(CGPoint(x: 0, y: donde - 80), animated: true)
            
            HUD.flash(.labeledError(title: "Error", subtitle: mensaje),delay: 2.0)
            
            return false
        }
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.tag == 1 || textField.tag == 2){
            let characterSet = CharacterSet.letters
            
            if ((string.rangeOfCharacter(from: characterSet.inverted) != nil) && (string != "") && (string != " ")) {
                return false
            }
        }
        
        return true
    }
    
    func enviarDatos(){
        //antes de enviar nada, validar todos los datos
        if (!self.validaForma()){
            return
        }
        HUD.show(.labeledProgress(title: "", subtitle: "Enviando datos"))
        let correo = self.txtEmailUsr.text?.lowercased()
        let passwd = self.txtContraUsr.text
        
        //se intenta insertar el usuario a firebase login
        FIRAuth.auth()?.createUser(withEmail: correo!, password: passwd!, completion: {(user, error) in
            
            if error != nil {
                print(" JSON Error: \(error!) ")
                
                let mensaje = self.xfunciones.getFirAuthErrorMessage(elError: error!)
                
                HUD.flash(.labeledError(title: "Error", subtitle: mensaje), delay: 2.0)
                return
            }else{
                print("Login Successful: \(user)")
                
                //actualizar el perfil con  los datos y la imagen si es que agrego una
                self.uploadImagen(nameImg: correo!)
            }
        })
        
       
        
    }
    
    func uploadImagen(nameImg: String){
        var urlImagen = URL(string: "")
        //si selecciono imagen, subirla a firebase
        if (self.imagenPerfil) {
            let storage = FIRStorage.storage().reference(forURL: Constantes.firebaseStorageRef)
            let fileName = storage.child("\(nameImg).jpg")
            let newImage = self.xfunciones.scaleAndCropImage(image: self.imageProfile.image!, toSize: CGSize(width: 200, height: 200))
            let imgData = UIImagePNGRepresentation(newImage) as Data?
            let xmetadata = FIRStorageMetadata()
            xmetadata.contentType = "image/jpeg"
            
            _ = fileName.put(imgData!, metadata: xmetadata, completion: { Rmetadata, error in
                if (error != nil) {
                    HUD.flash(.labeledError(title: "Error", subtitle: "Ocurrio un error al intentar crear el usuario"), delay: 2.0)
                    return
                } else {
                    // Metadata contains file metadata such as size, content-type, and download URL.
                    urlImagen = Rmetadata!.downloadURL()
                    self.updateProfile(urlImg: urlImagen)
                }
            })
        } else {
            self.updateProfile(urlImg: urlImagen)
        }
        
    }
    
    func updateProfile(urlImg: URL?){
        // actualizar datos de usuario
        let nombre_completo = self.txtNombreUsr.text! + " " + self.txtApellidosUsr.text!
        
        let user = FIRAuth.auth()?.currentUser
        if let user = user {
            let changeRequest = user.profileChangeRequest()
            
            changeRequest.displayName = nombre_completo
            if self.imagenPerfil {
                changeRequest.photoURL = urlImg!
            }
            changeRequest.commitChanges { error in
                if error != nil {
                    let mensaje = self.xfunciones.getFirAuthErrorMessage(elError: error!)
                    
                    HUD.flash(.labeledError(title: "Error", subtitle: mensaje), delay: 2.0)
                    return
                } else {
                    HUD.flash(.labeledSuccess(title: "Éxito", subtitle: "Usuario creado correctamente"), delay: 2.5){ finished in
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        switch textField {
        case self.txtNombreUsr:
            self.txtApellidosUsr.becomeFirstResponder()
            break
        case self.txtApellidosUsr:
            self.txtEmailUsr.becomeFirstResponder()
            break
        case self.txtEmailUsr:
            self.txtContraUsr.becomeFirstResponder()
            break
        case self.txtContraUsr:
            self.txtContraConfUsr.becomeFirstResponder()
            break
        default:
            break
        }
        
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let point = textField.frame.origin
        self.scrollView.setContentOffset(CGPoint(x:0, y:point.y - 80), animated: true)
        return true
    }
    
    //MARK: Para image picker
    func abreGaleria(sender: UITapGestureRecognizer){
        self.imagePicker.allowsEditing = false
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let nuevaImg = self.xfunciones.scaleAndCropImage(image: chosenImage, toSize: CGSize(width: 300, height: 300))
            self.imageProfile.image = nuevaImg
            self.imageProfile.setNeedsDisplay()
            self.imagenPerfil = true
        }
        dismiss(animated: true, completion: nil)
    }
    
    //para image picker e imagen
    
    
    
    
}
