//
//  sideMenuRC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 12/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit

class sideMenuRC: UIViewController {
    
    @IBOutlet weak var lblPortada: UILabel!
    @IBOutlet weak var lblLaVoz: UILabel!
    @IBOutlet weak var lblNoConfundas: UILabel!
    @IBOutlet weak var lblPerfil: UILabel!
    @IBOutlet weak var lblFavoritos: UILabel!
    @IBOutlet weak var lblIdiomas: UILabel!
    @IBOutlet weak var lblQuienesSomos: UILabel!
    @IBOutlet weak var lblPrivacidad: UILabel!
    @IBOutlet weak var lblFacebook: UILabel!
    @IBOutlet weak var lblBoletin: UILabel!
    
    let notifPortada = Notification.Name("gotoPortada")
    let notifQuienesSomos = Notification.Name("gotoQuienesSomos")
    let notifPrivacidad = Notification.Name("gotoPrivacidad")
    let notifVozNoticia = Notification.Name("gotoVozNoticia")
    let notifNoConfundas = Notification.Name("gotoNoConfudas")
    let notifFacebook = Notification.Name("gotoFacebook")
    let notifPerfil = Notification.Name("gotoPerfil")
    let notifFavoritos = Notification.Name("gotoFavoritos")
    let notifBoletin = Notification.Name("gotoBoletin")
    let notifIdiomas = Notification.Name("gotoIdiomas")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = Constantes.paletaColores.fondoSideMenu
        self.view.backgroundColor = Constantes.paletaColores.fondoSideMenu
        
        self.configTaps()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.changeLabels()
    }
    
    func changeLabels(){
        let index = Functions().getIdiomaIndex()
        //self.lblPortada.text = idiomasClass().arrayIdiomas[index].menu_noticias
        self.lblNoConfundas.text = idiomasClass().arrayIdiomas[index].menu_no_confundas
        self.lblPerfil.text = idiomasClass().arrayIdiomas[index].menu_perfil_notif
        self.lblFavoritos.text = idiomasClass().arrayIdiomas[index].menu_favoritos
        self.lblIdiomas.text = idiomasClass().arrayIdiomas[index].menu_idiomas
        self.lblQuienesSomos.text = idiomasClass().arrayIdiomas[index].menu_quienes_somos
        self.lblPrivacidad.text = idiomasClass().arrayIdiomas[index].menu_privacidad
    }
    
    func configTaps(){
        let tapPortada = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblPortada.tag = 1
        self.lblPortada.isUserInteractionEnabled = true
        self.lblPortada.addGestureRecognizer(tapPortada)
        
        let tapVozNoticia = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblLaVoz.tag = 2
        self.lblLaVoz.isUserInteractionEnabled = true
        self.lblLaVoz.addGestureRecognizer(tapVozNoticia)
        
        let tapNoConfundas = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblNoConfundas.tag = 3
        self.lblNoConfundas.isUserInteractionEnabled = true
        self.lblNoConfundas.addGestureRecognizer(tapNoConfundas)
        
        let tapPerfil = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblPerfil.tag = 4
        self.lblPerfil.isUserInteractionEnabled = true
        self.lblPerfil.addGestureRecognizer(tapPerfil)
        
        let tapFavoritos = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblFavoritos.tag = 5
        self.lblFavoritos.isUserInteractionEnabled = true
        self.lblFavoritos.addGestureRecognizer(tapFavoritos)
        
        let tapIdiomas = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblIdiomas.tag = 6
        self.lblIdiomas.isUserInteractionEnabled = true
        self.lblIdiomas.addGestureRecognizer(tapIdiomas)
        
        let tapQuienesSomos = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblQuienesSomos.tag = 7
        self.lblQuienesSomos.isUserInteractionEnabled = true
        self.lblQuienesSomos.addGestureRecognizer(tapQuienesSomos)
        
        let tapPrivacidad = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblPrivacidad.tag = 8
        self.lblPrivacidad.isUserInteractionEnabled = true
        self.lblPrivacidad.addGestureRecognizer(tapPrivacidad)
        
        let tapFacebook = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblFacebook.tag = 9
        self.lblFacebook.isUserInteractionEnabled = true
        self.lblFacebook.addGestureRecognizer(tapFacebook)
        
        let tapBoletin = UITapGestureRecognizer(target: self, action: #selector(self.manageTaps(sender:)))
        self.lblBoletin.tag = 10
        self.lblBoletin.isUserInteractionEnabled = true
        self.lblBoletin.addGestureRecognizer(tapBoletin)
        
    }
    
    func manageTaps(sender: UITapGestureRecognizer){
        let tag = sender.view?.tag
        
        switch tag! {
        case 1:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifPortada, object: nil)
            break
        case 2:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifVozNoticia, object: nil)
            break
        case 3:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifNoConfundas, object: nil)
            break
        case 4:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifPerfil, object: nil)
            break
        case 5:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifFavoritos, object: nil)
            break
        case 6:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifIdiomas, object: nil)
            break
        case 7:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifQuienesSomos, object: nil)
            break
        case 8:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifPrivacidad, object: nil)
            break
        case 9:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifFacebook, object: nil)
            break
        case 10:
            self.navigationController?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: self.notifBoletin, object: nil)
            break
        default:
            break
        }

    }
    
    
    
}
