//
//  noConfundasVC.swift
//  practicaespanol
//
//  Created by Leonel Sanchez on 18/04/17.
//  Copyright © 2017 firewallsoluciones. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenu
import PKHUD
import Foundation


class noConfundasVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var anchoPantalla : CGFloat = 0
    var altoPantalla : CGFloat = 0
    var altoStatusBar : CGFloat = 0
    
    var noConfundasData = [NoConfundas]()
    var noConfundasDataRespaldo = [NoConfundas]()
    
    var tableNoConfundas : UITableView!
    
    var xfunciones = Functions()
    
    var dataLoaded = false
    
    var offsetAlto : CGFloat = 0
    
    var pageNo = 1
    
    var isDataLoading = false
    
    var strLetraActual = "TODOS"
    var vistoOnOff = false
    
    let notifFiltroNC = Notification.Name("aplicaFiltroNoConf")
    
    var celdaSeleccionada : IndexPath!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getDatosFiltro), name: self.notifFiltroNC, object: nil)
        
        self.anchoPantalla = self.view.frame.size.width
        self.altoPantalla = self.view.frame.size.height
        self.altoStatusBar = self.xfunciones.statusBarHeight()
        
        self.offsetAlto = (self.view.frame.size.height * 0.1) + (self.navigationController?.navigationBar.frame.size.height)!
        
        self.setNavBar()
        
        self.buildInterface()
        self.pageNo = 1
        self.getData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setNavBar(){
        //top bar
        let topBtn1 = UIButton(type: .custom)
        topBtn1.setImage(UIImage(named: "ic_menu_hamburger"), for: .normal)
        topBtn1.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
        topBtn1.addTarget(self, action: #selector(self.showMenu(_:)), for: .touchUpInside)
        let hbMenu = UIBarButtonItem(customView: topBtn1)
        
        self.navigationItem.leftBarButtonItem = hbMenu
        
        //filtro -- 
        
        let imgFiltro = UIImage(named: "filter2")
        let filterButton = UIBarButtonItem(image: imgFiltro, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.filterButtonTap))
        
        self.navigationItem.rightBarButtonItem = filterButton
    }
    
    func showMenu(_ sender: UIBarButtonItem){
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (!self.dataLoaded){
            //self.xfunciones.startActivityIndicator(sender: self)
            HUD.show(.labeledProgress(title: "", subtitle: "Cargando..."))
        }
        
        if self.celdaSeleccionada != nil {
            self.tableNoConfundas.reloadRows(at: [self.celdaSeleccionada], with: .automatic)
        }
        
        let index = Functions().getIdiomaIndex()
        
        self.title = idiomasClass().arrayIdiomas[index].menu_no_confundas.uppercased()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func getDatosFiltro(notification: NSNotification){
        let paramFiltro = notification.object as! NSDictionary
        self.strLetraActual = paramFiltro["letra"] as! String
        self.vistoOnOff = paramFiltro["visto"] as! Bool
        self.aplicafiltro()
    }
    
    func aplicafiltro(){
        self.noConfundasData = self.noConfundasDataRespaldo
        self.pageNo = 1
        var newArray = [NoConfundas]()
        if self.strLetraActual != "TODOS" {
            for item in self.noConfundasData {
                if let pletra = item.primera_letra {
                    if pletra.contains(self.strLetraActual){
                        newArray.append(item)
                    }
                }
                
            }
            self.noConfundasData = newArray
        }
        
        if self.vistoOnOff {
            for item in self.noConfundasData {
                if xfunciones.existeIdEnVisto(id_nota: item.id){
                    newArray.append(item)
                }
            }
            self.noConfundasData = newArray
        }
        self.dataLoaded = true
        self.tableNoConfundas.reloadData()
    }
    
    func buildInterface(){
        
        self.tableNoConfundas = UITableView(frame: CGRect(x: 0, y: 0, width: self.anchoPantalla, height: self.altoPantalla))
        self.view.addSubview(self.tableNoConfundas)
        self.tableNoConfundas.dataSource = self
        self.tableNoConfundas.delegate = self
        self.tableNoConfundas.register(noConfundasCell.self, forCellReuseIdentifier: "noConfundasCell")
        self.tableNoConfundas.rowHeight = 60
        self.tableNoConfundas.separatorStyle = .none
        self.tableNoConfundas.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1)
    }
    
    func getData(){
        
        let urlrequest = Constantes.WSData.urlNoConfundas+"?page=\(self.pageNo)"
        Alamofire.request("\(urlrequest)").responseJSON { response in
            switch response.result {
            case .success(let value):
                let jsonData = JSON(value)
                if ((jsonData.array?.count)! > 0){
                    self.noConfundasData = self.noConfundasDataRespaldo
                    if (self.pageNo == 1){
                        self.noConfundasData = self.xfunciones.JSONtoArrayNoConfundas(elJson: jsonData)
                    } else {
                        self.noConfundasData.append(contentsOf: self.xfunciones.JSONtoArrayNoConfundas(elJson: jsonData))
                    }
                    self.noConfundasDataRespaldo = self.noConfundasData
                    
                    if (!self.dataLoaded){
                        var insets = self.tableNoConfundas.contentInset
                        insets.bottom += self.offsetAlto
                        self.tableNoConfundas.contentInset = insets
                    }
                    
                    self.aplicafiltro()
                }
                HUD.hide()
            case .failure(let error):
                print(error)
                HUD.hide()
                let alerta = Functions().alertaSimple(titulo: "Error", mensaje: "Ha ocurrido un error al intentar conectarse al servidor", estilo: .alert, showButton: true)
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    
    func filterButtonTap(){
        let vcFiltro = FiltroNoConfundasVC()
        vcFiltro.letraActual = self.strLetraActual
        vcFiltro.vistoOnOff = self.vistoOnOff
        vcFiltro.buildInterface()
        self.navigationController?.pushViewController(vcFiltro, animated: true)
    }
    
    //table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.dataLoaded {
            return self.noConfundasData.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableNoConfundas.dequeueReusableCell(withIdentifier: "noConfundasCell")! as! noConfundasCell
        
        cell.frame.size.width = self.anchoPantalla
        
        if self.dataLoaded {
            
            //contenedor back
            var frameVar = cell.viewContenedor.frame
            frameVar.size = CGSize(width: cell.frame.width * 0.98, height: cell.frame.height * 0.98)
            cell.viewContenedor.frame = frameVar
            
            
            //titulo
            cell.lblTexto.text = self.noConfundasData[indexPath.row].palabra1 + " - " + self.noConfundasData[indexPath.row].palabra2
            frameVar = cell.lblTexto.frame
            frameVar.origin = CGPoint(x: 10, y: 5)
            frameVar.size = CGSize(width: cell.frame.size.width * 0.7, height: 10)
            cell.lblTexto.frame = frameVar
            cell.lblTexto.sizeToFit()
            cell.lblTexto.center.y = cell.viewContenedor.frame.size.height / 2
            
            //visto
            cell.imgVisto.frame.size = CGSize(width: cell.frame.size.height * 0.47, height: cell.frame.size.height * 0.47)
            frameVar = cell.imgVisto.frame
            let posVisto = cell.viewContenedor.frame.size.width - (cell.imgVisto.frame.size.width + 5)
            frameVar.origin = CGPoint(x: posVisto, y: 30)
            cell.imgVisto.frame = frameVar
            cell.imgVisto.center.y = cell.viewContenedor.frame.size.height / 2
            if (!xfunciones.existeIdEnVisto(id_nota: self.noConfundasData[indexPath.row].id)){
                cell.imgVisto.isHidden = true
            } else {
                cell.imgVisto.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detalleNoConfundasVC = noConfundasDetalleVC()
        detalleNoConfundasVC.palabrasData = self.noConfundasData[indexPath.row]
        self.celdaSeleccionada = indexPath
        self.navigationController?.pushViewController(detalleNoConfundasVC, animated: true)
        
    }
    
    
    //table view
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        //print("scrollViewWillBeginDragging")
        self.isDataLoading = false
    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //print("scrollViewDidEndDecelerating")
    }
    //paginacion
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //print("scrollViewDidEndDragging")
        if ((self.tableNoConfundas.contentOffset.y + self.tableNoConfundas.frame.size.height) > self.tableNoConfundas.contentSize.height)
        {
            if !isDataLoading{
                isDataLoading = true
                self.pageNo += 1
                self.getData()
                
            }
        }
        
        
    }
    
    
} //fin de clase
